use super::*;
use snui::cache::{font::FontCache, source::SourceCache, Cache};
use snui::environment::{Env, Environment, ThemeManager, STYLESHEET_SIGNAL};
use snui::{scene::*, widgets::Rectangle, *};

use smithay_client_toolkit::reexports::calloop::timer::{TimeoutAction, Timer};
use smithay_client_toolkit::reexports::client::Proxy as WlProxy;

use snui::tiny_skia::ClipMask;
use snui::tiny_skia::Transform;

use std::cell::RefCell;
use std::mem::take;
use std::ops::{Deref, DerefMut};
use std::rc::{Rc, Weak};
use std::sync::Arc;
use std::time::Duration;

use smithay_client_toolkit::event_loop::WaylandSource;
use smithay_client_toolkit::reexports::client::backend::WaylandError;
use smithay_client_toolkit::reexports::client::{
    backend, event_created_child,
    protocol::wl_buffer,
    protocol::wl_callback,
    protocol::wl_compositor,
    protocol::wl_data_device,
    protocol::wl_data_device_manager,
    protocol::wl_data_offer,
    protocol::wl_keyboard::{self, KeyState},
    protocol::wl_output::{self, EVT_NAME_SINCE},
    protocol::wl_pointer::{self, Axis, ButtonState},
    protocol::wl_region,
    protocol::wl_registry,
    protocol::wl_seat::{self, Capability},
    protocol::wl_shm::{self, Format},
    protocol::wl_shm_pool,
    protocol::wl_subcompositor,
    protocol::wl_subsurface,
    protocol::wl_surface,
    Connection, Dispatch, QueueHandle, WEnum,
};
use smithay_client_toolkit::reexports::{
    protocols::xdg::shell::client::{
        xdg_popup::{self, EVT_REPOSITIONED_SINCE},
        xdg_positioner, xdg_surface, xdg_toplevel, xdg_wm_base,
    },
    protocols_wlr::layer_shell::v1::client::{zwlr_layer_shell_v1, zwlr_layer_surface_v1},
};
use smithay_client_toolkit::shm::multi::{MultiPool, PoolError};
use smithay_client_toolkit::{error::GlobalError, globals::ProvidesBoundGlobal};
use wayland_cursor::CursorTheme;

use smithay_client_toolkit::reexports::calloop::{EventLoop, LoopHandle, LoopSignal};

use xkbcommon::xkb::{Context, Keymap};

const GEOMETRY_PADDING: f32 = 10.;
const DEFAULT_KEYBOARD_DELAY: i32 = 500;
const DEFAULT_KEYBOARD_REPEAT_RATE: i32 = 100;

/// The core of the wayland client.
///
/// It implements [`Dispatch`] for all the [`Proxy`] used by [`WaylandClient`].
///
/// This means you can use it you can implement your own [`Dispatch`] type over it.
pub struct WaylandClientInner<T, D>
where
    T: 'static,
{
    font: FontCache,
    source: SourceCache,
    id: Option<SurfaceId>,
    data: T,
    /// Used by views to insert new ones to the client.
    globals: Rc<RefCell<GlobalManager>>,
    views: HashMap<SurfaceId, View<T>>,
    context: Context,
    pool: Option<MultiPool<SurfaceId>>,
    conn: Connection,
    env: Arc<dyn Environment>,
    loop_handle: LoopHandle<'static, D>,
}

impl<T: 'static, D> WaylandClientInner<T, D> {
    /// Create a [`WaylandClientInner`].
    ///
    /// This type can be use to delegate dispatch implementations.
    pub fn new(
        data: T,
        registry: wl_registry::WlRegistry,
        conn: Connection,
        loop_handle: LoopHandle<'static, D>,
    ) -> Self {
        WaylandClientInner {
            data,
            id: None,
            pool: None,
            font: FontCache::default(),
            source: SourceCache::default(),
            context: Context::new(0),
            conn,
            loop_handle,
            env: Arc::new(ThemeManager),
            globals: Rc::new(RefCell::new(GlobalManager::new(registry))),
            views: HashMap::new(),
        }
    }
}

/// A wayland client.
pub struct WaylandClient<T>
where
    T: 'static,
{
    pub inner: WaylandClientInner<T, Self>,
    loop_signal: LoopSignal,
    pub qhandle: QueueHandle<Self>,
}

impl<T> Deref for WaylandClient<T> {
    type Target = WaylandClientInner<T, Self>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> AsMut<WaylandClientInner<T, Self>> for WaylandClient<T> {
    fn as_mut(&mut self) -> &mut WaylandClientInner<T, Self> {
        &mut self.inner
    }
}

impl<T> DerefMut for WaylandClient<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

pub trait Application {
    /// Update the id view.
    fn update(&mut self) -> Result<(), WaylandError>;
    /// Send an event to the id view.
    fn event(&mut self, event: Event) -> Result<(), WaylandError>;
}

/// A mega trait for [`View`].
pub trait ViewDispatch:
    Application
    + Dispatch<wl_region::WlRegion, ()>
    + Dispatch<xdg_popup::XdgPopup, SurfaceId>
    + Dispatch<wl_surface::WlSurface, ()>
    + Dispatch<wl_callback::WlCallback, SurfaceId>
    + Dispatch<xdg_surface::XdgSurface, SurfaceId>
    + Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>
    + Dispatch<wl_compositor::WlCompositor, ()>
    + Dispatch<xdg_positioner::XdgPositioner, ()>
{
}

impl<T> ViewDispatch for T
where
    T: Application,
    T: Dispatch<wl_region::WlRegion, ()>,
    T: Dispatch<xdg_popup::XdgPopup, SurfaceId>,
    T: Dispatch<wl_surface::WlSurface, ()>,
    T: Dispatch<wl_callback::WlCallback, SurfaceId>,
    T: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
    T: Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>,
    T: Dispatch<wl_compositor::WlCompositor, ()>,
    T: Dispatch<xdg_positioner::XdgPositioner, ()>,
{
}

impl<T> WaylandClient<T>
where
    T: 'static,
{
    pub fn new(data: T) -> Option<(Self, EventLoop<'static, Self>)> {
        let conn = Connection::connect_to_env().ok()?;

        let mut event_queue = conn.new_event_queue();
        let qh = event_queue.handle();

        let event_loop: EventLoop<Self> =
            EventLoop::try_new().expect("Failed to initialize the event loop!");

        let display = conn.display();
        let registry = display.get_registry(&qh, ());

        conn.flush().unwrap();

        let mut wl_client = WaylandClient {
            inner: WaylandClientInner::new(data, registry, conn, event_loop.handle()),
            loop_signal: event_loop.get_signal(),
            qhandle: qh,
        };

        for _ in 0..2 {
            event_queue.blocking_dispatch(&mut wl_client).unwrap();
            event_queue.flush().unwrap();
        }

        event_loop
            .handle()
            .insert_source(WaylandSource::new(event_queue).ok()?, |_, queue, client| {
                if !client.has_view() {
                    client.loop_signal.stop();
                    client.loop_signal.wakeup();
                }
                queue.dispatch_pending(client)
            })
            .expect("Failed to insert the wayland client in the event loop!");

        if !wl_client
            .globals
            .borrow()
            .formats()
            .iter()
            .any(|format| Format::Abgr8888.eq(format))
        {
            eprintln!("warn: backend: ABGR8888 format not supported by the compositor.");
        }

        Some((wl_client, event_loop))
    }
    pub fn new_with_cb<F>(data: T, f: F) -> Option<(Self, EventLoop<'static, Self>)>
    where
        F: FnOnce(&mut WaylandClientInner<T, Self>, &QueueHandle<Self>),
    {
        let (mut client, event_loop) = Self::new(data)?;
        f(&mut client.inner, &client.qhandle);
        Some((client, event_loop))
    }
    pub fn init<F>(data: T, f: F)
    where
        F: FnOnce(&mut WaylandClientInner<T, Self>, &QueueHandle<WaylandClient<T>>),
    {
        if let Some((mut client, mut event_loop)) = Self::new_with_cb(data, f) {
            event_loop
                .run(None, &mut client, |_| {})
                .expect("Error during event loop!")
        }
    }
    pub fn view_handle(&mut self, id: &SurfaceId) -> Option<ViewHandle<T, Self>> {
        self.inner
            .views
            .get_mut(id)
            .map(|view| view.handle(&self.inner.conn, &self.qhandle))
    }
}

impl<T> Application for WaylandClient<T> {
    fn update(&mut self) -> Result<(), WaylandError> {
        self.inner.update(&self.qhandle);
        self.conn.flush()
    }
    fn event(&mut self, event: Event) -> Result<(), WaylandError> {
        self.inner.dispatch(event, &self.qhandle);
        self.conn.flush()
    }
}

impl<T, D> WaylandClientInner<T, D>
where
    T: 'static,
{
    /// Focus the [`View`] associated with a [`SurfaceId`].
    pub fn focus(&mut self, id: SurfaceId) {
        if self.views.contains_key(&id) {
            self.id = Some(id);
        }
    }
    /// Set the app_id of the client.
    pub fn set_app_id(&mut self, app_id: impl AsRef<str>) {
        self.globals
            .borrow_mut()
            .app_id
            .replace_range(0.., app_id.as_ref());
    }
    /// Replace the [`Environment`] on the view.
    pub fn set_environment(&mut self, env: impl Environment + 'static) {
        self.env = Arc::new(env);
        for view in self.views.values_mut() {
            view.env = self.env.clone();
        }
    }
    pub fn environment(&self) -> &dyn Environment {
        self.env.as_ref()
    }
    /// Destroy all views.
    pub fn clear(&mut self) {
        for (_, mut view) in self.views.drain() {
            view.surface.destroy();
            self.pool
                .as_mut()
                .unwrap()
                .remove(&SurfaceId(view.surface.id()));
        }
    }
    /// Return the application's data.
    pub fn data(&mut self) -> &mut T {
        &mut self.data
    }
    /// Return the current view.
    pub fn view(&mut self) -> Option<&mut View<T>> {
        self.id.as_ref().and_then(|id| self.views.get_mut(id))
    }
    /// Returns an iterator across all views.
    pub fn views(&mut self) -> impl Iterator<Item = &mut View<T>> {
        self.views.values_mut()
    }
    /// Returns the view associated to the [`SurfaceId`].
    pub fn get_mut_view(&mut self, id: &SurfaceId) -> Option<&mut View<T>> {
        self.views.get_mut(id)
    }
    /// Returns the view associated to the [`SurfaceId`].
    pub fn get_view(&mut self, id: &SurfaceId) -> Option<&View<T>> {
        self.views.get(id)
    }
    /// Dispatch an event to the id view.
    pub fn dispatch(&mut self, event: Event, qh: &QueueHandle<D>)
    where
        D: 'static,
        D: ViewDispatch,
    {
        if let Some(view) = self.id.as_ref().and_then(|id| self.views.get_mut(id)) {
            let slot = view.dispatch(
                &mut self.data,
                self.pool.as_mut().unwrap(),
                Cache::new(&mut self.font, &mut self.source),
                event,
                &self.conn,
                qh,
            );

            if !view.state.configured {
                let parent = view
                    .surface
                    .shell
                    .parent()
                    .map(|surface| SurfaceId(surface.id()));
                let id = std::mem::replace(&mut self.id, parent);
                if let Some((mut view, id)) =
                    id.as_ref().and_then(|id| self.views.remove(id)).zip(id)
                {
                    view.surface.destroy();
                    self.pool.as_mut().unwrap().remove(&id);
                    self.destroy_children(id);
                    self.update(qh);
                }
            }

            if let Some(view) = slot {
                self.views.insert(SurfaceId(view.surface.id()), view);
            }
        }
    }
    /// Update to the id view.
    pub fn update(&mut self, qh: &QueueHandle<D>)
    where
        D: 'static,
        D: ViewDispatch,
    {
        if let Some(view) = self.id.as_ref().and_then(|id| self.views.get_mut(id)) {
            let slot = view.update(
                &mut self.data,
                self.pool.as_mut().unwrap(),
                Cache::new(&mut self.font, &mut self.source),
                &self.conn,
                qh,
            );
            if !view.state.configured {
                let parent = view
                    .surface
                    .shell
                    .parent()
                    .map(|surface| SurfaceId(surface.id()));
                let id = std::mem::replace(&mut self.id, parent);
                if let Some((mut view, id)) =
                    id.as_ref().and_then(|id| self.views.remove(id)).zip(id)
                {
                    view.surface.destroy();
                    self.pool.as_mut().unwrap().remove(&id);
                    self.destroy_children(id);
                }
            }
            if let Some(view) = slot {
                self.views.insert(SurfaceId(view.surface.id()), view);
            }
        }
    }
    /// Destroys views from children surfaces.
    fn destroy_children(&mut self, parent: SurfaceId) {
        let mut children = Vec::new();
        self.views.retain(|id, view| {
            if view
                .surface
                .shell
                .parent()
                .map(|surface| SurfaceId(surface.id()) == parent)
                .unwrap_or_default()
            {
                children.push(id.clone());
                self.pool.as_mut().unwrap().remove(id);
                false
            } else {
                true
            }
        });
        for id in children {
            self.destroy_children(id);
        }
    }
    fn destroy_children_popup(&mut self, parent: SurfaceId) {
        let mut children = Vec::new();
        self.views.retain(|id, view| {
            let shell = &view.surface.shell;
            if shell.popup().is_some()
                && shell
                    .parent()
                    .map(|surface| SurfaceId(surface.id()) == parent)
                    .unwrap_or_default()
            {
                children.push(id.clone());
                self.pool.as_mut().unwrap().remove(id);
                false
            } else {
                true
            }
        });
        for id in children {
            self.destroy_children(id);
        }
    }
    /// Returns the [`SurfaceId`] of the root surface.
    pub fn root(&self, id: SurfaceId) -> Option<SurfaceId> {
        let surface = &self.views.get(&id)?.surface;
        if let Some(id) = surface.shell.parent().map(|s| SurfaceId(s.id())) {
            self.root(id)
        } else {
            Some(id)
        }
    }
    /// Create an xdg-shell window.
    pub fn create_window<'s>(
        &'s mut self,
        widget: impl Widget<T> + 'static,
        qh: &'s QueueHandle<D>,
    ) -> ViewHandle<'s, T, D>
    where
        D: 'static,
        D: Dispatch<wl_compositor::WlCompositor, ()>,
        D: Dispatch<wl_surface::WlSurface, ()>,
        D: Dispatch<wl_region::WlRegion, ()>,
        D: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
        D: Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>,
    {
        let surface = self.globals.borrow().create_xdg_surface(qh);
        let view = View {
            env: self.env.clone(),
            state: State::default(),
            cursor: Default::default(),
            globals: self.globals.clone(),
            widget: Proxy::new(Box::new(widget.padding(GEOMETRY_PADDING))),
            clipmask: Some(ClipMask::new()),
            surface: surface.expect("Failed to create a XdgSurface!"),
        };

        let id = SurfaceId(view.surface.id());
        self.views.insert(id.clone(), view);
        self.views.get_mut(&id).unwrap().handle(&self.conn, qh)
    }
    /// Create a layer-shell widget.
    pub fn create_widget<'s>(
        &'s mut self,
        widget: impl Widget<T> + 'static,
        config: LayerSurfaceBuilder,
        qh: &'s QueueHandle<D>,
    ) -> ViewHandle<'s, T, D>
    where
        D: 'static,
        D: Dispatch<wl_region::WlRegion, ()>,
        D: Dispatch<wl_compositor::WlCompositor, ()>,
        D: Dispatch<wl_surface::WlSurface, ()>,
        D: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
        D: Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>,
        D: Dispatch<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1, SurfaceId>,
    {
        let globals = self.globals.borrow();
        if let Some(surface) = globals.create_layer_surface(config, qh) {
            let view = View {
                surface,
                env: self.env.clone(),
                state: State::default(),
                cursor: Default::default(),
                globals: self.globals.clone(),
                clipmask: Some(ClipMask::new()),
                widget: Proxy::new(Box::new(widget)),
            };

            let id = SurfaceId(view.surface.id());
            self.views.insert(id.clone(), view);
            self.views.get_mut(&id).unwrap().handle(&self.conn, qh)
        } else {
            std::mem::drop(globals);
            eprintln!("Failed to initialize a LayerSurface!");
            self.create_window(widget, qh)
        }
    }
    pub fn has_view(&self) -> bool {
        !self.views.is_empty()
    }
    pub fn cache(&mut self) -> Cache {
        Cache::new(&mut self.font, &mut self.source)
    }
    pub fn globals(&self) -> Weak<RefCell<GlobalManager>> {
        Rc::downgrade(&self.globals)
    }
}

impl<T> Drop for WaylandClient<T> {
    fn drop(&mut self) {
        self.inner.globals.borrow().destroy();
    }
}

/// A view is weston's terminoly for any graphical client that's a component of the desktop shell.
///
/// A view here can be a window, a popup, a widget, a subsurface, basically anything you can attach a buffer to.
/// It holds your widgets and the "window" state.
pub struct View<T> {
    state: State,
    surface: Surface,
    cursor: MouseEvent,
    clipmask: Option<ClipMask>,
    widget: Proxy<T, Box<dyn Widget<T>>>,
    env: Arc<dyn Environment>,
    globals: Rc<RefCell<GlobalManager>>,
}

#[derive(Default)]
struct State {
    time: u32,
    redraw: bool,
    offset: usize,
    configured: bool,
    pending_cb: bool,
    enter_serial: u32,
    last_serial: u32,
    modifiers: Modifiers,
    constraints: BoxConstraints,
    window_state: Vec<WindowState>,
}

impl<T: 'static> View<T> {
    /// Create a [`ViewHandle`]
    pub fn handle<'s, D>(
        &'s mut self,
        conn: &'s Connection,
        qh: &'s QueueHandle<D>,
    ) -> ViewHandle<'s, T, D> {
        ViewHandle {
            slot: None,
            conn,
            env: self.env.clone(),
            globals: self.globals.clone(),
            state: &mut self.state,
            qh,
            surface: &mut self.surface,
        }
    }
}

/// A handle to a view.
pub struct ViewHandle<'s, T, D> {
    qh: &'s QueueHandle<D>,
    conn: &'s Connection,
    env: Arc<dyn Environment>,
    globals: Rc<RefCell<GlobalManager>>,
    slot: Option<View<T>>,
    state: &'s mut State,
    surface: &'s mut Surface,
}

impl<'s, T, D> ViewHandle<'s, T, D> {
    pub fn set_min_size(&mut self, width: i32, height: i32) {
        match &self.surface.shell {
            Shell::Toplevel { toplevel, .. } => {
                toplevel.set_min_size(width, height);
            }
            Shell::LayerSurface { layer_surface, .. } => {
                layer_surface.set_size(width as u32, height as u32);
            }
            _ => {
                self.state.constraints =
                    self.state.constraints.with_min(width as f32, height as f32);
            }
        }
    }
    pub fn set_max_size(&mut self, width: i32, height: i32) {
        match &self.surface.shell {
            Shell::Toplevel { toplevel, .. } => {
                toplevel.set_max_size(width, height);
            }
            Shell::LayerSurface { layer_surface, .. } => {
                layer_surface.set_size(width as u32, height as u32);
            }
            _ => {
                self.state.constraints =
                    self.state.constraints.with_max(width as f32, height as f32);
            }
        }
    }
    /// Returns the [`SurfaceId`] associated to the [`View`].
    pub fn id(&self) -> SurfaceId {
        SurfaceId(self.surface.id())
    }
}

impl<'s, T, D> WindowHandle<T> for ViewHandle<'s, T, D>
where
    T: 'static,
    D: 'static,
    D: ViewDispatch,
{
    fn close(&mut self) {
        // The WaylandClient will destroy the surface and remove the view.
        self.state.configured = false;
    }
    fn _move(&mut self, serial: u32) {
        match &self.surface.shell {
            Shell::Toplevel { toplevel, .. } => {
                for seat in &self.globals.borrow().seats {
                    toplevel._move(&seat.seat, serial);
                }
            }
            _ => {}
        }
    }
    fn maximize(&mut self) {
        match &self.surface.shell {
            Shell::Toplevel { toplevel, .. } => {
                if self.state.window_state.contains(&WindowState::Maximized) {
                    toplevel.unset_maximized();
                } else {
                    toplevel.set_maximized();
                }
            }
            Shell::LayerSurface { layer_surface, .. } => {
                // The following Configure event should be adjusted to
                // the size of the output
                layer_surface.set_anchor(Anchor::all());
            }
            _ => {}
        }
    }
    fn children(&self) -> usize {
        self.surface
            .object_data()
            .map(|udata| Arc::strong_count(udata).max(2) - 2)
            .unwrap_or_default()
    }
    fn resize(&mut self, serial: u32, anchor: (widgets::Alignment, widgets::Alignment)) {
        match &self.surface.shell {
            Shell::Toplevel { toplevel, .. } => {
                let (anchor_x, anchor_y) = anchor;
                let edge = match anchor_x {
                    widgets::Alignment::Start => match anchor_y {
                        widgets::Alignment::Start => xdg_toplevel::ResizeEdge::TopLeft,
                        widgets::Alignment::Center => xdg_toplevel::ResizeEdge::Left,
                        widgets::Alignment::End => xdg_toplevel::ResizeEdge::BottomLeft,
                    },
                    widgets::Alignment::Center => match anchor_y {
                        widgets::Alignment::Start => xdg_toplevel::ResizeEdge::Top,
                        widgets::Alignment::Center => xdg_toplevel::ResizeEdge::None,
                        widgets::Alignment::End => xdg_toplevel::ResizeEdge::Bottom,
                    },
                    widgets::Alignment::End => match anchor_y {
                        widgets::Alignment::Start => xdg_toplevel::ResizeEdge::TopRight,
                        widgets::Alignment::Center => xdg_toplevel::ResizeEdge::Right,
                        widgets::Alignment::End => xdg_toplevel::ResizeEdge::BottomRight,
                    },
                };
                for seat in &self.globals.borrow().seats {
                    toplevel.resize(seat, serial, edge);
                }
            }
            _ => {}
        }
    }
    fn show_menu(&mut self, menu: Menu<T>) {
        match menu {
            Menu::System { position, serial } => {
                if let Shell::Toplevel { toplevel, .. } = &self.surface.shell {
                    for seat in &self.globals.borrow().seats {
                        toplevel.show_window_menu(
                            &seat.seat,
                            serial,
                            position.x as i32,
                            position.y as i32,
                        );
                    }
                }
            }
            Menu::Toplevel {
                widget,
                title,
                size,
            } => {
                if let Some(mut surface) = self.globals.borrow().create_xdg_surface(self.qh) {
                    if let Some(toplevel) = surface.shell.toplevel() {
                        toplevel.set_title(title);
                        toplevel.set_parent(self.surface.shell.toplevel());
                        toplevel.set_min_size(
                            size.minimum_width() as i32,
                            size.minimum_height() as i32,
                        );
                        toplevel.set_max_size(
                            size.maximum_width() as i32,
                            size.maximum_height() as i32,
                        );
                    }
                    surface.shell.set_parent(self.surface.wl_surface.clone());
                    let view = View {
                        widget: Proxy::new(Box::new(widget.padding(GEOMETRY_PADDING))),
                        env: self.env.clone(),
                        cursor: Default::default(),
                        clipmask: Some(ClipMask::new()),
                        globals: self.globals.clone(),
                        state: State::default(),
                        surface,
                    };
                    view.surface.deref().commit();
                    self.slot = Some(view);
                }
            }
            Menu::Popup {
                size,
                offset,
                anchor,
                widget,
                serial,
            } => {
                if let Some(positioner) = self.globals.borrow().create_positioner(self.qh) {
                    let (anchor_x, anchor_y) = anchor;
                    let anchor = match anchor_x {
                        widgets::Alignment::Start => match anchor_y {
                            widgets::Alignment::Start => xdg_positioner::Anchor::TopLeft,
                            widgets::Alignment::Center => xdg_positioner::Anchor::Left,
                            widgets::Alignment::End => xdg_positioner::Anchor::BottomLeft,
                        },
                        widgets::Alignment::Center => match anchor_y {
                            widgets::Alignment::Start => xdg_positioner::Anchor::Top,
                            widgets::Alignment::Center => xdg_positioner::Anchor::None,
                            widgets::Alignment::End => xdg_positioner::Anchor::Bottom,
                        },
                        widgets::Alignment::End => match anchor_y {
                            widgets::Alignment::Start => xdg_positioner::Anchor::TopRight,
                            widgets::Alignment::Center => xdg_positioner::Anchor::Right,
                            widgets::Alignment::End => xdg_positioner::Anchor::BottomRight,
                        },
                    };
                    positioner.set_anchor(anchor);
                    positioner.set_offset(offset.x as i32, offset.y as i32);
                    positioner.set_size(size.width as i32, size.height as i32);
                    positioner.set_anchor_rect(
                        0,
                        0,
                        self.state.constraints.maximum_width() as i32,
                        self.state.constraints.maximum_height() as i32,
                    );
                    let view = match &self.surface.shell {
                        Shell::Toplevel { xdg_surface, .. } | Shell::Popup { xdg_surface, .. } => {
                            let mut popup = self
                                .globals
                                .borrow()
                                .create_popup(
                                    Some(self.surface.deref()),
                                    positioner,
                                    Some(xdg_surface),
                                    self.qh,
                                )
                                .unwrap();
                            popup.output = self.surface.output.clone();
                            View {
                                widget: Proxy::new(widget),
                                env: self.env.clone(),
                                cursor: Default::default(),
                                clipmask: Some(ClipMask::new()),
                                globals: self.globals.clone(),
                                state: State::default(),
                                surface: popup,
                            }
                        }
                        Shell::LayerSurface { layer_surface, .. } => {
                            let mut popup = self
                                .globals
                                .borrow()
                                .create_popup(Some(self.surface.deref()), positioner, None, self.qh)
                                .unwrap();
                            popup.output = self.surface.output.clone();
                            layer_surface.get_popup(popup.shell.popup().unwrap());
                            View {
                                widget: Proxy::new(widget),
                                env: self.env.clone(),
                                cursor: Default::default(),
                                clipmask: Some(ClipMask::new()),
                                globals: self.globals.clone(),
                                state: State::default(),
                                surface: popup,
                            }
                        }
                    };

                    if let Some(serial) = serial {
                        if view
                            .surface
                            .object_data()
                            .map(|udata| Arc::strong_count(udata) == 2)
                            .unwrap_or_default()
                        {
                            if let Some(popup) = view.surface.shell.popup() {
                                for seat in self.globals.borrow().seats.iter() {
                                    popup.grab(seat, serial);
                                }
                            }
                        }
                    }

                    view.surface.deref().commit();
                    self.slot = Some(view);
                }
            }
        }
    }
    fn minimize(&mut self) {
        match &self.surface.shell {
            Shell::Toplevel { toplevel, .. } => {
                toplevel.set_minimized();
            }
            _ => {
                self.surface.commit(None);
            }
        }
    }
    fn set_title(&mut self, title: String) {
        match self.surface.shell {
            Shell::Toplevel { ref toplevel, .. } => {
                toplevel.set_title(title);
            }
            _ => {}
        }
    }
    fn set_cursor(&mut self, cursor: Cursor) {
        let scale = self
            .surface
            .output
            .as_ref()
            .and_then(|id| {
                self.globals
                    .borrow()
                    .outputs
                    .get(id)
                    .map(|output| output.scale)
            })
            .unwrap_or(1);
        let mut ref_global = self.globals.borrow_mut();
        let globals = &mut *ref_global;
        let surface = if let Some(surface) = globals.pointer_surface.as_ref() {
            surface
        } else {
            globals.pointer_surface = Some(
                globals
                    .create_surface(self.qh)
                    .expect("Failed to create cursor surface"),
            );
            globals.pointer_surface.as_ref().unwrap()
        };
        let seats = &globals.seats;
        let cursor_size = 24 * scale as u32;
        let cursor_theme = globals.cursor_theme.get_mut(&cursor_size);
        if let Some(cursor_theme) = cursor_theme {
            for seat in seats {
                surface.set_buffer_scale(scale);
                if let Cursor::None = cursor {
                    surface.attach(None, 0, 0);
                    surface.commit();
                } else if let Some(cursor) = cursor_theme.get_cursor(cursor.as_str()) {
                    let buffer = &cursor[0];
                    let (hotspot_x, hotspot_y) = buffer.hotspot();
                    surface.attach(Some(buffer), 0, 0);
                    surface.commit();
                    seat.pointer
                        .as_ref()
                        .expect("Failed to retreive the pointer.")
                        .set_cursor(
                            self.state.enter_serial,
                            Some(surface),
                            hotspot_x as i32 / scale,
                            hotspot_y as i32 / scale,
                        );
                }
            }
            return;
        }
        if let Ok(cursor_theme) =
            CursorTheme::load(self.conn, globals.shm.clone().unwrap(), cursor_size)
        {
            globals.cursor_theme.insert(cursor_size, cursor_theme);
            std::mem::drop(ref_global);
            self.set_cursor(cursor);
        } else {
            surface.destroy();
        }
    }
    fn get_state(&self) -> &[WindowState] {
        self.state.window_state.as_slice()
    }
}

impl<T: 'static> View<T> {
    fn event<D>(
        &mut self,
        data: &mut T,
        cache: Cache,
        event: Event,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) -> (Damage, Option<Self>)
    where
        D: 'static,
        D: ViewDispatch,
    {
        let mut damage = (matches!(event, Event::Configure | Event::Callback(_))
            || self.state.redraw)
            .then_some(Damage::Partial)
            .unwrap_or_default();
        let mut handle = ViewHandle {
            slot: None,
            env: self.env.clone(),
            globals: self.globals.clone(),
            state: &mut self.state,
            conn,
            qh,
            surface: &mut self.surface,
        };
        let env = Env::new(self.env.as_ref());
        let mut ctx = UpdateContext::new(data, cache, &mut handle);
        damage = damage.max(self.widget.event(&mut ctx, event, &env));

        if ctx.reset() {
            damage = damage.max(self.widget.update(&mut ctx, &env));
        }

        std::mem::drop(ctx);

        (damage, handle.slot)
    }
    fn discrete_update<D>(
        &mut self,
        data: &mut T,
        mut cache: Cache,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) -> Option<Self>
    where
        D: 'static,
        D: ViewDispatch,
    {
        let mut handle = ViewHandle {
            slot: None,
            env: self.env.clone(),
            globals: self.globals.clone(),
            state: &mut self.state,
            conn,
            qh,
            surface: &mut self.surface,
        };
        let env = Env::new(self.env.as_ref());
        let mut ctx = UpdateContext::new(data, cache.borrow(), &mut handle);
        self.widget.update(&mut ctx, &env);
        std::mem::drop(ctx);
        handle.slot
    }
    fn update<D>(
        &mut self,
        data: &mut T,
        pool: &mut MultiPool<SurfaceId>,
        mut cache: Cache,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) -> Option<Self>
    where
        D: 'static,
        D: ViewDispatch,
    {
        let mut handle = ViewHandle {
            slot: None,
            env: self.env.clone(),
            globals: self.globals.clone(),
            state: &mut self.state,
            conn,
            qh,
            surface: &mut self.surface,
        };
        let env = Env::new(self.env.as_ref());
        let mut ctx = UpdateContext::new(data, cache.borrow(), &mut handle);
        let damage = self.widget.update(&mut ctx, &env);
        std::mem::drop(ctx);
        let slot = handle.slot;
        match damage {
            Damage::Partial => {
                if !self.state.pending_cb {
                    self.render(data, pool, cache, qh);
                }
            }
            Damage::Frame(draw) => {
                if !self.state.pending_cb {
                    self.surface.frame(qh, SurfaceId(self.surface.id()));
                    self.surface.wl_surface.commit();
                    self.state.pending_cb = true;
                    if draw {
                        self.render(data, pool, cache.borrow(), qh);
                    }
                }
            }
            _ => {}
        };

        slot
    }
    fn dispatch<D>(
        &mut self,
        data: &mut T,
        pool: &mut MultiPool<SurfaceId>,
        mut cache: Cache,
        event: Event,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) -> Option<Self>
    where
        D: 'static,
        D: ViewDispatch,
    {
        let (damage, slot) = self.event(data, cache.borrow(), event, conn, qh);
        match damage {
            Damage::Partial => {
                if !self.state.pending_cb {
                    self.render(data, pool, cache, qh);
                }
            }
            Damage::Frame(draw) => {
                if !self.state.pending_cb {
                    self.surface.frame(qh, SurfaceId(self.surface.id()));
                    self.surface.wl_surface.commit();
                    self.state.pending_cb = true;
                    if draw {
                        self.render(data, pool, cache.borrow(), qh);
                    }
                }
            }
            _ => {}
        }
        slot
    }
    fn render<D>(
        &mut self,
        data: &T,
        pool: &mut MultiPool<SurfaceId>,
        mut cache: Cache,
        qh: &QueueHandle<D>,
    ) where
        D: 'static,
        D: Dispatch<wl_callback::WlCallback, SurfaceId>,
    {
        let scale = self
            .surface
            .output
            .as_ref()
            .and_then(|id| {
                self.globals
                    .borrow()
                    .outputs
                    .get(id)
                    .map(|output| output.scale)
            })
            .unwrap_or(1);
        let mut layout = LayoutContext::new(cache.borrow(), data).with_scale(scale as f32);
        let env = Env::new(self.env.as_ref());
        let Size { width, height } = self.widget.layout(
            &mut if self.state.redraw {
                layout.force()
            } else {
                layout
            },
            &self
                .surface
                .shell
                .toplevel()
                .is_some()
                .then(|| {
                    self.state
                        .constraints
                        .crop(-2. * GEOMETRY_PADDING, -2. * GEOMETRY_PADDING)
                })
                .unwrap_or(self.state.constraints),
            &env,
        );

        let surface = &mut self.surface;

        let abgr = self.globals.borrow().formats().contains(&Format::Abgr8888);

        if width * height == 0. {
            surface.commit(None);
            return;
        } else {
            match buffer(
                pool,
                width as u32 * scale as u32,
                height as u32 * scale as u32,
                if abgr {
                    Format::Abgr8888
                } else {
                    Format::Argb8888
                },
                &SurfaceId(surface.wl_surface.id()),
            ) {
                Ok((offset, wl_buffer, backend)) => {
                    let transform = Transform::from_scale(scale as f32, scale as f32);
                    let region = Rectangle::new(width, height);
                    let mut ctx = DrawContext::new(
                        backend.with_clipmask(self.clipmask.as_mut().unwrap()),
                        cache,
                    )
                    .with_transform(transform);

                    let scene = Scene::new(&mut ctx, &region);

                    if offset != self.state.offset || self.state.redraw {
                        self.state.redraw = false;
                        self.widget.draw_scene(
                            scene.damage(Region::new(0., 0., region.width(), region.height())),
                            &env,
                        );
                    } else {
                        self.widget.draw_scene(scene, &env);
                    }

                    let len = ctx.as_mut().len();
                    let (mut backend, damage_queue) = ctx.unwrap();

                    if !abgr {
                        (0..len)
                            .step_by(4 * width as usize * scale as usize)
                            .enumerate()
                            .flat_map(|(mut line, offset)| {
                                line /= scale as usize;
                                damage_queue.iter().filter_map(move |region| {
                                    (line >= region.y as usize
                                        && line < (region.y + region.height) as usize
                                        && region.x < width)
                                        .then_some({
                                            (
                                                offset + region.x as usize * 4 * scale as usize,
                                                region.width.min(width) as usize
                                                    * 4
                                                    * scale as usize,
                                            )
                                        })
                                })
                            })
                            .for_each(|(offset, size)| {
                                backend.as_mut()[offset..][..size]
                                    .chunks_exact_mut(4)
                                    .for_each(|pixel| pixel.swap(0, 2));
                            });
                    }

                    if let Some(surface) = surface.shell.layer_surface() {
                        surface.set_size(width as u32, height as u32);
                    } else if let Some(surface) = surface.shell.xdg_surface() {
                        surface.set_window_geometry(
                            GEOMETRY_PADDING as i32,
                            GEOMETRY_PADDING as i32,
                            (width - 2. * GEOMETRY_PADDING) as i32,
                            (height - 2. * GEOMETRY_PADDING) as i32,
                        );
                    }

                    self.state.offset = offset;
                    surface.set_buffer_scale(scale);
                    surface.damage(&damage_queue);
                    surface.commit(Some(wl_buffer));
                    return;
                }
                Err(e) => {
                    if let PoolError::Overlap = e {
                        pool.remove(&SurfaceId(surface.wl_surface.id()));
                    }
                }
            }
        }

        if !self.state.pending_cb {
            surface.frame(qh, SurfaceId(surface.id()));
            surface.wl_surface.commit();
            self.state.pending_cb = true;
        }
    }
}

impl GlobalManager {
    fn create_xdg_surface<D>(&self, qh: &QueueHandle<D>) -> Option<Surface>
    where
        D: 'static,
        D: Dispatch<wl_compositor::WlCompositor, ()>,
        D: Dispatch<wl_surface::WlSurface, ()>,
        D: Dispatch<wl_region::WlRegion, ()>,
        D: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
        D: Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>,
    {
        let wm_base = self.wm_base.as_ref()?;
        let compositor = self.compositor.as_ref()?;

        let wl_surface = compositor.create_surface(qh, ());
        let wl_region = compositor.create_region(qh, ());
        let xdg_surface = wm_base.get_xdg_surface(&wl_surface, qh, SurfaceId(wl_surface.id()));
        let toplevel = xdg_surface.get_toplevel(qh, SurfaceId(wl_surface.id()));

        toplevel.set_app_id(self.app_id.clone());
        toplevel.set_title("default".to_string());

        wl_surface.commit();

        Some(Surface::new(
            wl_surface,
            wl_region,
            Shell::Toplevel {
                parent: None,
                xdg_surface,
                toplevel,
            },
            None,
        ))
    }
    fn create_positioner<D>(&self, qh: &QueueHandle<D>) -> Option<xdg_positioner::XdgPositioner>
    where
        D: 'static,
        D: Dispatch<xdg_positioner::XdgPositioner, ()>,
    {
        let wm_base = self.wm_base.as_ref()?;
        Some(wm_base.create_positioner(qh, ()))
    }
    fn create_popup<D>(
        &self,
        parent_surface: Option<&wl_surface::WlSurface>,
        positioner: xdg_positioner::XdgPositioner,
        parent: Option<&xdg_surface::XdgSurface>,
        qh: &QueueHandle<D>,
    ) -> Option<Surface>
    where
        D: 'static,
        D: Dispatch<wl_compositor::WlCompositor, ()>,
        D: Dispatch<wl_surface::WlSurface, ()>,
        D: Dispatch<wl_region::WlRegion, ()>,
        D: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
        D: Dispatch<xdg_popup::XdgPopup, SurfaceId>,
    {
        let wm_base = self.wm_base.as_ref()?;
        let compositor = self.compositor.as_ref()?;

        let wl_surface = compositor.create_surface(qh, ());
        let wl_region = compositor.create_region(qh, ());
        let xdg_surface = wm_base.get_xdg_surface(&wl_surface, qh, SurfaceId(wl_surface.id()));
        let popup = xdg_surface.get_popup(parent, &positioner, qh, SurfaceId(wl_surface.id()));

        Some(Surface::new(
            wl_surface,
            wl_region,
            Shell::Popup {
                parent: parent_surface.cloned(),
                xdg_surface,
                positioner,
                popup,
            },
            None,
        ))
    }
    fn create_surface<D>(&self, qh: &QueueHandle<D>) -> Option<wl_surface::WlSurface>
    where
        D: 'static,
        D: Dispatch<wl_surface::WlSurface, ()>,
        D: Dispatch<wl_compositor::WlCompositor, ()>,
    {
        let compositor = self.compositor.as_ref()?;
        Some(compositor.create_surface(qh, ()))
    }
    fn create_layer_surface<D>(
        &self,
        config: LayerSurfaceBuilder,
        qh: &QueueHandle<D>,
    ) -> Option<Surface>
    where
        D: 'static,
        D: Dispatch<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1, SurfaceId>,
        D: Dispatch<wl_surface::WlSurface, ()>,
        D: Dispatch<wl_compositor::WlCompositor, ()>,
        D: Dispatch<wl_region::WlRegion, ()>,
    {
        let output = self
            .outputs
            .values()
            .find(|output| output.name.eq(config.output.as_str()))
            .map(|output| &output.output);
        let layer_shell = self.layer_shell.as_ref()?;
        let compositor = self.compositor.as_ref()?;

        let wl_surface = compositor.create_surface(qh, ());
        let wl_region = compositor.create_region(qh, ());
        let layer_surface = layer_shell.get_layer_surface(
            &wl_surface,
            output,
            config.layer,
            self.app_id.clone(),
            qh,
            SurfaceId(wl_surface.id()),
        );
        let mut size = Size::new(1, 1);
        if let Some(anchor) = config.anchor {
            if anchor.contains(Anchor::Top) && anchor.contains(Anchor::Bottom) {
                size.height = 0;
            }
            if anchor.contains(Anchor::Left) && anchor.contains(Anchor::Right) {
                size.width = 0;
            }
            layer_surface.set_anchor(anchor);
        }
        layer_surface.set_size(size.width, size.height);
        wl_surface.commit();
        layer_surface.set_margin(
            config.margin[0],
            config.margin[1],
            config.margin[2],
            config.margin[3],
        );
        layer_surface.set_keyboard_interactivity(config.interactivity);
        Some(Surface::new(
            wl_surface,
            wl_region,
            Shell::LayerSurface {
                config,
                layer_surface,
            },
            None,
        ))
    }
}

impl Surface {
    fn new(
        wl_surface: wl_surface::WlSurface,
        wl_region: wl_region::WlRegion,
        shell: Shell,
        previous: Option<Surface>,
    ) -> Self {
        Surface {
            wl_surface,
            shell,
            wl_region,
            output: None,
            previous: previous.map(Box::new),
        }
    }
    fn commit(&mut self, buffer: Option<&wl_buffer::WlBuffer>) {
        self.wl_surface.attach(buffer, 0, 0);
        self.wl_surface.commit();
        self.previous = None;
    }
    fn destroy(&mut self) {
        self.shell.destroy();
        self.wl_region.destroy();
        self.wl_surface.destroy();
        self.destroy_previous();
    }
    fn destroy_previous(&mut self) {
        if let Some(mut surface) = take(&mut self.previous) {
            surface.destroy();
        }
    }
    fn damage(&self, regions: &[Region]) {
        for d in regions {
            self.wl_surface
                .damage(d.x as i32, d.y as i32, d.width as i32, d.height as i32);
        }
    }
}

impl Deref for Surface {
    type Target = wl_surface::WlSurface;
    fn deref(&self) -> &Self::Target {
        &self.wl_surface
    }
}

impl<T, D> ProvidesBoundGlobal<wl_shm::WlShm, 1> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
{
    fn bound_global(&self) -> Result<wl_shm::WlShm, GlobalError> {
        self.globals
            .borrow()
            .shm
            .clone()
            .ok_or(GlobalError::NotReady)
    }
}

impl<T, D> Dispatch<wl_registry::WlRegistry, (), D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self>,
    D: Dispatch<xdg_wm_base::XdgWmBase, ()>,
    D: Dispatch<wl_region::WlRegion, ()>,
    D: Dispatch<xdg_popup::XdgPopup, SurfaceId>,
    D: Dispatch<wl_surface::WlSurface, ()>,
    D: Dispatch<wl_callback::WlCallback, SurfaceId>,
    D: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
    D: Dispatch<wl_subcompositor::WlSubcompositor, ()>,
    D: Dispatch<wl_shm::WlShm, ()>,
    D: Dispatch<wl_seat::WlSeat, ()>,
    D: Dispatch<wl_output::WlOutput, ()>,
    D: Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>,
    D: Dispatch<wl_compositor::WlCompositor, ()>,
    D: Dispatch<wl_shm_pool::WlShmPool, ()>,
    D: Dispatch<wl_data_offer::WlDataOffer, ()>,
    D: Dispatch<wl_data_device::WlDataDevice, ()>,
    D: Dispatch<wl_data_device_manager::WlDataDeviceManager, ()>,
    D: Dispatch<xdg_positioner::XdgPositioner, ()>,
    D: Dispatch<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1, SurfaceId>,
    D: Dispatch<zwlr_layer_shell_v1::ZwlrLayerShellV1, ()>,
    D: Dispatch<wl_registry::WlRegistry, ()>,
{
    fn event(
        data: &mut D,
        registry: &wl_registry::WlRegistry,
        event: <wl_registry::WlRegistry as WlProxy>::Event,
        _: &(),
        _conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        if let wl_registry::Event::Global {
            name,
            interface,
            version,
        } = event
        {
            match interface.as_str() {
                "wl_compositor" => {
                    data.as_mut().globals.borrow_mut().compositor =
                        Some(registry.bind::<wl_compositor::WlCompositor, _, _>(name, 4, qh, ()));
                }
                "wl_subcompositor" => {
                    data.as_mut().globals.borrow_mut().subcompositor = Some(
                        registry.bind::<wl_subcompositor::WlSubcompositor, _, _>(name, 1, qh, ()),
                    );
                }
                "wl_shm" => {
                    let this = data.as_mut();
                    this.globals.borrow_mut().shm =
                        Some(registry.bind::<wl_shm::WlShm, _, _>(name, 1, qh, ()));
                    this.pool = MultiPool::new(this).ok();
                }
                "wl_seat" => {
                    let seat = registry.bind::<wl_seat::WlSeat, _, _>(name, 5, qh, ());
                    if let Some(device_manager) =
                        data.as_mut().globals.borrow().device_manager.as_ref()
                    {
                        device_manager.get_data_device(&seat, qh, ());
                    }
                }
                "wl_output" => {
                    if version < EVT_NAME_SINCE {
                        registry.bind::<wl_output::WlOutput, _, _>(name, version, qh, ())
                    } else {
                        registry.bind::<wl_output::WlOutput, _, _>(name, EVT_NAME_SINCE, qh, ())
                    };
                }
                "wl_data_device_manager" => {
                    let device_manager = registry
                        .bind::<wl_data_device_manager::WlDataDeviceManager, _, _>(
                            name,
                            wl_data_device_manager::REQ_CREATE_DATA_SOURCE_SINCE,
                            qh,
                            (),
                        );
                    data.as_mut().globals.borrow_mut().device_manager = Some(device_manager);
                }
                "zwlr_layer_shell_v1" => {
                    data.as_mut().globals.borrow_mut().layer_shell = Some(
                        registry.bind::<zwlr_layer_shell_v1::ZwlrLayerShellV1, _, _>(
                            name,
                            2,
                            qh,
                            (),
                        ),
                    );
                }
                "xdg_wm_base" => {
                    data.as_mut().globals.borrow_mut().wm_base =
                        Some(if version < EVT_REPOSITIONED_SINCE {
                            registry.bind::<xdg_wm_base::XdgWmBase, _, _>(name, 1, qh, ())
                        } else {
                            registry.bind::<xdg_wm_base::XdgWmBase, _, _>(
                                name,
                                EVT_REPOSITIONED_SINCE,
                                qh,
                                (),
                            )
                        });
                }
                _ => {}
            }
        }
    }
}

impl<T, U, D> Dispatch<wl_shm::WlShm, U, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_shm::WlShm, U>,
{
    fn event(
        data: &mut D,
        _proxy: &wl_shm::WlShm,
        event: <wl_shm::WlShm as WlProxy>::Event,
        _udata: &U,
        _conn: &Connection,
        _qh: &QueueHandle<D>,
    ) {
        match event {
            wl_shm::Event::Format { format } => {
                if let WEnum::Value(format) = format {
                    data.as_mut().globals.borrow_mut().formats.push(format);
                }
            }
            _ => unreachable!(),
        }
    }
}

impl<T, D> Dispatch<xdg_popup::XdgPopup, SurfaceId, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<xdg_popup::XdgPopup, SurfaceId>,
{
    fn event(
        data: &mut D,
        xdg_popup: &xdg_popup::XdgPopup,
        event: <xdg_popup::XdgPopup as WlProxy>::Event,
        id: &SurfaceId,
        _conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(view) = this.views.get_mut(id) {
            match event {
                xdg_popup::Event::Configure {
                    x: _,
                    y: _,
                    width,
                    height,
                } => {
                    if width > 1 && height > 1 {
                        view.state.constraints = BoxConstraints::new(
                            (width as f32, height as f32),
                            (width as f32, height as f32),
                        );
                    } else {
                        let env = Env::new(view.env.as_ref());
                        let cache = Cache::new(&mut this.font, &mut this.source);
                        let mut ctx = LayoutContext::new(cache, &this.data);
                        let size = view
                            .widget
                            .layout(&mut ctx, &BoxConstraints::default(), &env);
                        view.state.pending_cb = true;
                        view.state.constraints = BoxConstraints::new(Size::default(), size);
                        let positioner = view.surface.shell.positioner().unwrap();
                        positioner.set_size(size.width as i32, size.height as i32);
                        xdg_popup.reposition(positioner, id.0.protocol_id())
                    }
                }
                xdg_popup::Event::Repositioned { token: _ } => {
                    view.state.pending_cb = false;
                    let env = Env::new(view.env.as_ref());
                    let cache = Cache::new(&mut this.font, &mut this.source);
                    let mut ctx = LayoutContext::new(cache, &this.data);
                    let size = view.widget.layout(&mut ctx, &view.state.constraints, &env);
                    view.state.constraints = BoxConstraints::new(Size::default(), size);
                }
                xdg_popup::Event::PopupDone => {
                    std::mem::drop(view);
                    if let Some(mut view) = this.views.remove(id) {
                        view.surface.destroy();
                        this.id = view
                            .surface
                            .shell
                            .parent()
                            .map(|surface| SurfaceId(surface.id()));
                        this.pool.as_mut().unwrap().remove(id);
                        this.dispatch(Event::Configure, qh);
                        this.destroy_children(id.clone());
                    }
                    return;
                }
                _ => {}
            }
            if view
                .surface
                .object_data()
                .map(|udata| Arc::strong_count(udata) > 2)
                .unwrap_or_default()
            {
                let id = SurfaceId(view.surface.id());
                this.destroy_children(id);
            }
        }
    }
}

impl<T, D> Dispatch<wl_surface::WlSurface, (), D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
{
    fn event(
        state: &mut D,
        surface: &wl_surface::WlSurface,
        event: wl_surface::Event,
        _: &(),
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = state.as_mut();
        if let wl_surface::Event::Enter { output } = event {
            let globals = this.globals.borrow();
            if let Some(view) = this.views.get_mut(&SurfaceId(surface.id())) {
                let slot;
                let scale = globals
                    .outputs
                    .get(&OutputId(output.id()))
                    .map(|output| output.scale)
                    .unwrap_or_default();
                if let Some(c_output) = view.surface.output.as_ref() {
                    let c_scale = globals
                        .outputs
                        .get(c_output)
                        .map(|output| output.scale)
                        .unwrap_or_default();
                    slot = None;
                    view.surface.output = output.id().try_into().ok();
                    if c_scale != scale {
                        std::mem::drop(output);
                        std::mem::drop(globals);
                        view.state.redraw = true;
                        view.render(
                            &this.data,
                            this.pool.as_mut().unwrap(),
                            Cache::new(&mut this.font, &mut this.source),
                            qh,
                        )
                    }
                } else {
                    let previous_scale = view
                        .surface
                        .output
                        .as_ref()
                        .and_then(|id| globals.outputs.get(id))
                        .map(|output| output.scale)
                        .unwrap_or_default();
                    slot = view.discrete_update(
                        &mut this.data,
                        Cache::new(&mut this.font, &mut this.source),
                        conn,
                        qh,
                    );
                    view.surface.output = output.id().try_into().ok();
                    if scale != previous_scale {
                        view.state.redraw = true;
                        if !view.state.pending_cb {
                            surface.frame(qh, SurfaceId(surface.id()));
                            view.state.pending_cb = true;
                            surface.commit();
                        }
                    }
                }
                if view.surface.shell.parent().is_none() {
                    this.id = Some(SurfaceId(surface.id()));
                }
                if let Some(view) = slot {
                    this.views.insert(SurfaceId(surface.id()), view);
                }
            }
        }
    }
}

impl<T, D> Dispatch<wl_callback::WlCallback, SurfaceId, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_callback::WlCallback, SurfaceId>,
{
    fn event(
        data: &mut D,
        _: &wl_callback::WlCallback,
        event: wl_callback::Event,
        id: &SurfaceId,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let wl_callback::Event::Done { callback_data } = event {
            let pool = this.pool.as_mut().unwrap();
            let mut cache = Cache::new(&mut this.font, &mut this.source);
            if let Some(view) = this.views.get_mut(id) {
                let frame_time = callback_data.saturating_sub(view.state.time);
                view.state.time = callback_data;
                let (damage, slot) = view.event(
                    &mut this.data,
                    cache.borrow(),
                    Event::Callback(frame_time.min(20)),
                    conn,
                    qh,
                );
                view.state.pending_cb = false;
                match damage {
                    Damage::Partial => {
                        view.render(&this.data, pool, cache.borrow(), qh);
                    }
                    Damage::Frame(draw) => {
                        view.state.pending_cb = true;
                        if draw {
                            view.render(&this.data, pool, cache.borrow(), qh);
                        }
                        view.surface.frame(qh, id.clone());
                        view.surface.deref().commit();
                    }
                    _ => {}
                }
                if let Some(view) = slot {
                    this.views.insert(id.clone(), view);
                }
            }
        }
    }
}

fn from_toplevel_state(state: xdg_toplevel::State) -> WindowState {
    match state {
        xdg_toplevel::State::Activated => WindowState::Activated,
        xdg_toplevel::State::Fullscreen => WindowState::Fullscreen,
        xdg_toplevel::State::TiledLeft => WindowState::TiledLeft,
        xdg_toplevel::State::TiledRight => WindowState::TiledRight,
        xdg_toplevel::State::TiledTop => WindowState::TiledTop,
        xdg_toplevel::State::TiledBottom => WindowState::TiledBottom,
        xdg_toplevel::State::Resizing => WindowState::Resizing,
        xdg_toplevel::State::Maximized => WindowState::Maximized,
        _ => unreachable!(),
    }
}

impl<T, D> Dispatch<xdg_toplevel::XdgToplevel, SurfaceId, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<xdg_toplevel::XdgToplevel, SurfaceId>,
{
    fn event(
        data: &mut D,
        _: &xdg_toplevel::XdgToplevel,
        event: <xdg_toplevel::XdgToplevel as WlProxy>::Event,
        id: &SurfaceId,
        _conn: &Connection,
        _: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(view) = this.views.get_mut(id) {
            match event {
                xdg_toplevel::Event::Configure {
                    width,
                    height,
                    states,
                } => {
                    collect_states(&mut view.state.window_state, states);
                    if width * height != 0 {
                        view.state.constraints =
                            view.state.constraints.stretch(width as f32, height as f32);
                    }
                }
                xdg_toplevel::Event::Close => {
                    std::mem::drop(view);
                    if let Some(mut view) = this.views.remove(id) {
                        view.surface.destroy();
                        this.pool.as_mut().unwrap().remove(id);
                        this.destroy_children(id.clone());
                    }
                }
                _ => {}
            }
        }
    }
}

fn collect_states(vec: &mut Vec<WindowState>, states: Vec<u8>) {
    vec.splice(
        0..,
        states
            .chunks(4)
            .filter_map(|endian| {
                xdg_toplevel::State::try_from(u32::from_ne_bytes([
                    endian[0], endian[1], endian[2], endian[3],
                ]))
                .ok()
            })
            .map(from_toplevel_state),
    );
}

impl<T, U, D> Dispatch<xdg_wm_base::XdgWmBase, U, D> for WaylandClientInner<T, D>
where
    D: 'static,
    D: Dispatch<xdg_wm_base::XdgWmBase, U>,
{
    fn event(
        _: &mut D,
        wm_base: &xdg_wm_base::XdgWmBase,
        event: xdg_wm_base::Event,
        _: &U,
        _conn: &Connection,
        _: &QueueHandle<D>,
    ) {
        if let xdg_wm_base::Event::Ping { serial } = event {
            wm_base.pong(serial);
        }
    }
}

impl<T, D> Dispatch<xdg_surface::XdgSurface, SurfaceId, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<xdg_surface::XdgSurface, SurfaceId>,
{
    fn event(
        data: &mut D,
        xdg_surface: &xdg_surface::XdgSurface,
        event: xdg_surface::Event,
        id: &SurfaceId,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let xdg_surface::Event::Configure { serial, .. } = event {
            xdg_surface.ack_configure(serial);
            if let Some(view) = this.views.get_mut(id) {
                let configure = view.state.configured;
                view.state.configured = true;
                if let Some(view) = view.dispatch(
                    &mut this.data,
                    this.pool.as_mut().unwrap(),
                    Cache::new(&mut this.font, &mut this.source),
                    if configure {
                        Event::Configure
                    } else {
                        STYLESHEET_SIGNAL
                    },
                    conn,
                    qh,
                ) {
                    this.views.insert(id.clone(), view);
                }
            }
        }
    }
}

impl<T, D> Dispatch<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1, SurfaceId, D>
    for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1, SurfaceId>,
{
    fn event(
        data: &mut D,
        layer_surface: &zwlr_layer_surface_v1::ZwlrLayerSurfaceV1,
        event: zwlr_layer_surface_v1::Event,
        id: &SurfaceId,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(view) = this.views.get_mut(id) {
            match event {
                zwlr_layer_surface_v1::Event::Configure {
                    serial,
                    width,
                    height,
                } => {
                    layer_surface.ack_configure(serial);
                    if let Shell::LayerSurface { config, .. } = &view.surface.shell {
                        if config.exclusive {
                            if let Some(anchor) = config.anchor {
                                match anchor {
                                    Anchor::Left | Anchor::Right => {
                                        let width = view.state.constraints.maximum_width();
                                        layer_surface.set_exclusive_zone(width as i32)
                                    }
                                    Anchor::Top | Anchor::Bottom => {
                                        let height = view.state.constraints.maximum_height();
                                        layer_surface.set_exclusive_zone(height as i32)
                                    }
                                    _ => {}
                                }
                            }
                        }
                    }
                    view.state.window_state.splice(
                        0..,
                        [WindowState::Activated, WindowState::Resizing].into_iter(),
                    );
                    view.state.constraints = BoxConstraints::new(
                        (width as f32, height as f32),
                        (width as f32, height as f32),
                    );
                    let configure = view.state.configured;
                    view.state.configured = true;
                    if let Some(view) = view.dispatch(
                        &mut this.data,
                        this.pool.as_mut().unwrap(),
                        Cache::new(&mut this.font, &mut this.source),
                        if configure {
                            Event::Configure
                        } else {
                            STYLESHEET_SIGNAL
                        },
                        conn,
                        qh,
                    ) {
                        this.views.insert(id.clone(), view);
                    }
                }
                zwlr_layer_surface_v1::Event::Closed => {
                    std::mem::drop(view);
                    if let Some(mut view) = this.views.remove(id) {
                        view.surface.destroy();
                        this.pool.as_mut().unwrap().remove(id);
                        this.destroy_children(id.clone());
                    }
                }
                _ => unreachable!(),
            }
        }
    }
}

impl<T, D> Dispatch<wl_data_device::WlDataDevice, (), D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_data_device::WlDataDevice, ()>,
    D: Dispatch<wl_data_offer::WlDataOffer, ()>,
{
    fn event(
        data: &mut D,
        _proxy: &wl_data_device::WlDataDevice,
        event: <wl_data_device::WlDataDevice as WlProxy>::Event,
        _udata: &(),
        _conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        match event {
            wl_data_device::Event::DataOffer { id: _ } => {}
            wl_data_device::Event::Enter { serial, x, y, .. } => {
                if let Some(view) = this.id.as_ref().and_then(|id| this.views.get_mut(id)) {
                    view.cursor.pointer = Pointer::Enter;
                    view.cursor.position = Position::new(x as f32, y as f32);
                    view.state.last_serial = serial;
                }
            }
            wl_data_device::Event::Motion { x, y, .. } => {
                if let Some(view) = this.id.as_ref().and_then(|id| this.views.get_mut(id)) {
                    view.cursor.pointer = Pointer::Hover;
                    view.cursor.position = Position::new(x as f32, y as f32);
                    let cursor = view.cursor;
                    this.dispatch(Event::Pointer(cursor), qh);
                }
            }
            wl_data_device::Event::Selection { id: _ } => {}
            _ => {}
        }
    }
    event_created_child!(D, wl_data_device::WlDataDevice, [
        0 => (wl_data_offer::WlDataOffer, ()),
    ]);
}

impl<T, D> Dispatch<wl_data_offer::WlDataOffer, (), D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_data_offer::WlDataOffer, ()>,
{
    fn event(
        _data: &mut D,
        _proxy: &wl_data_offer::WlDataOffer,
        _event: <wl_data_offer::WlDataOffer as WlProxy>::Event,
        _udata: &(),
        _conn: &Connection,
        _qhandle: &QueueHandle<D>,
    ) {
        // match event {
        //     wl_data_offer::Event::Action { dnd_action } => {
        //     }
        //     wl_data_offer::Event::Offer { mime_type } => {
        //     }
        //     wl_data_offer::Event::SourceActions { source_actions } => {
        //     }
        //     _ => {}
        // }
    }
}

impl<T, D> Dispatch<wl_seat::WlSeat, (), D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_pointer::WlPointer, ()>,
    D: Dispatch<wl_keyboard::WlKeyboard, ()>,
    D: Dispatch<wl_seat::WlSeat, ()>,
{
    fn event(
        data: &mut D,
        seat: &wl_seat::WlSeat,
        event: wl_seat::Event,
        udata: &(),
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(seat) = this
            .globals
            .borrow_mut()
            .seats
            .iter_mut()
            .find(|s| s.deref().eq(seat))
        {
            match event {
                wl_seat::Event::Name { ref name } => {
                    seat.name = name.clone();
                }
                wl_seat::Event::Capabilities { capabilities } => {
                    seat.capabilities = capabilities;
                    if let WEnum::Value(capabilities) = capabilities {
                        if capabilities & Capability::Pointer == Capability::Pointer {
                            seat.pointer = Some(seat.get_pointer(qh, ()));
                        }
                        if capabilities & Capability::Keyboard == Capability::Keyboard {
                            let mut rules = String::new();
                            let mut layout = String::new();
                            let mut options = String::new();
                            let keyboard = {
                                *udata;
                                seat.get_keyboard(qh, ())
                            };
                            for (var, name) in std::env::vars() {
                                match var.as_str() {
                                    "XKB_DEFAULT_RULES" => rules = name,
                                    "XKB_DEFAULT_LAYOUT" => layout = name,
                                    "XKB_DEFAULT_OPTIONS" => options = name,
                                    _ => {}
                                }
                            }
                            let keymap = Keymap::new_from_names(
                                &this.context,
                                rules.as_str(),
                                "",
                                layout.as_str(),
                                "",
                                Some(options),
                                0,
                            )
                            .unwrap();
                            let state = xkbcommon::xkb::State::new(&keymap);
                            seat.keyboard = Some(super::KeyboardState {
                                rate: DEFAULT_KEYBOARD_REPEAT_RATE,
                                delay: DEFAULT_KEYBOARD_DELAY,
                                keyboard,
                                state,
                            });
                        }
                    }
                }
                _ => {}
            }
            return;
        }
        this.globals
            .borrow_mut()
            .seats
            .push(Seat::new(seat.clone()));
        Self::event(data, seat, event, udata, conn, qh);
    }
}

use xkbcommon::xkb;

impl<T, D> Dispatch<wl_keyboard::WlKeyboard, (), D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_keyboard::WlKeyboard, ()>,
{
    fn event(
        data: &mut D,
        keyboard: &wl_keyboard::WlKeyboard,
        event: wl_keyboard::Event,
        _: &(),
        _: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(view) = this.id.as_ref().and_then(|id| this.views.get_mut(id)) {
            let mut rate = DEFAULT_KEYBOARD_REPEAT_RATE;
            let mut delay = DEFAULT_KEYBOARD_DELAY;
            let keyboard_state = this
                .globals
                .borrow()
                .seats
                .iter()
                .filter_map(|seat| seat.keyboard.as_ref())
                .find(|keyboard_state| keyboard_state.eq(keyboard))
                .map(|keyboard_state| {
                    rate = keyboard_state.rate;
                    delay = keyboard_state.delay;
                    keyboard_state.state.clone()
                });
            match event {
                wl_keyboard::Event::RepeatInfo { rate, delay } => {
                    if let Some(mut seat) = this
                        .globals
                        .borrow_mut()
                        .seats
                        .iter_mut()
                        .filter_map(|seat| seat.keyboard.as_mut())
                        .find(|keyboard_state| keyboard_state.eq(keyboard))
                    {
                        seat.delay = delay;
                        seat.rate = rate;
                    }
                }
                wl_keyboard::Event::Modifiers {
                    mods_depressed,
                    mods_latched,
                    mods_locked,
                    ..
                } => {
                    if let Some(mut state) = keyboard_state {
                        state.update_mask(mods_depressed, mods_latched, mods_locked, 0, 0, 0);
                    }
                }
                wl_keyboard::Event::Key {
                    serial,
                    time,
                    mut key,
                    state,
                } => {
                    if !view.state.pending_cb {
                        view.state.time = time;
                    }
                    let pressed = match state {
                        WEnum::Value(KeyState::Pressed) => true,
                        _ => false,
                    };
                    view.state.last_serial = serial;
                    if let Some(mut kbstate) = keyboard_state {
                        key += 8;
                        kbstate.update_key(
                            key,
                            if pressed {
                                xkb::KeyDirection::Down
                            } else {
                                xkb::KeyDirection::Up
                            },
                        );
                        let utf8 = kbstate.key_get_utf8(key);
                        let keysym = kbstate.key_get_syms(key).to_vec();

                        view.state.modifiers.ctrl = kbstate
                            .mod_name_is_active(xkb::MOD_NAME_CTRL, xkb::STATE_MODS_EFFECTIVE);
                        view.state.modifiers.alt = kbstate
                            .mod_name_is_active(xkb::MOD_NAME_ALT, xkb::STATE_MODS_EFFECTIVE);
                        view.state.modifiers.shift = kbstate
                            .mod_name_is_active(xkb::MOD_NAME_SHIFT, xkb::STATE_MODS_EFFECTIVE);
                        view.state.modifiers.caps_lock = kbstate
                            .mod_name_is_active(xkb::MOD_NAME_CAPS, xkb::STATE_MODS_EFFECTIVE);
                        view.state.modifiers.logo = kbstate
                            .mod_name_is_active(xkb::MOD_NAME_LOGO, xkb::STATE_MODS_EFFECTIVE);
                        view.state.modifiers.num_lock = kbstate
                            .mod_name_is_active(xkb::MOD_NAME_NUM, xkb::STATE_MODS_EFFECTIVE);
                        view.state.last_serial = serial;

                        let key_event = Key {
                            utf8: Some(&utf8),
                            serial,
                            modifiers: &view.state.modifiers.clone(),
                            value: &keysym,
                            pressed,
                        };

                        let mut repeat = true;
                        let modifiers = view.state.modifiers;

                        this.dispatch(Event::Keyboard(key_event), qh);

                        if pressed {
                            this.loop_handle
                                .insert_source(
                                    Timer::from_duration(Duration::from_millis(delay as u64)),
                                    move |_, _, client| {
                                        let this = client.as_mut();
                                        if let Some(l_serial) = this
                                            .id
                                            .as_ref()
                                            .and_then(|id| this.views.get(id))
                                            .map(|v| v.state.last_serial)
                                        {
                                            if l_serial == serial && repeat {
                                                let key = Key {
                                                    utf8: Some(&utf8),
                                                    serial,
                                                    modifiers: &modifiers,
                                                    value: &keysym,
                                                    pressed,
                                                };
                                                let duration =
                                                    Duration::from_millis(1000 / rate as u64);
                                                Application::event(client, Event::Keyboard(key))
                                                    .unwrap();
                                                return TimeoutAction::ToDuration(duration);
                                            }
                                        }
                                        repeat = false;
                                        TimeoutAction::Drop
                                    },
                                )
                                .expect("Failed to insert a timer!");
                        }
                    }
                }
                _ => {}
            }
        } else {
            match event {
                wl_keyboard::Event::Enter { ref surface, .. } => {
                    this.id = Some(SurfaceId(surface.id()));
                }
                _ => {}
            }
        }
    }
}

impl<T, U, D> Dispatch<wl_pointer::WlPointer, U, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_pointer::WlPointer, U>,
{
    fn event(
        data: &mut D,
        wl_pointer: &wl_pointer::WlPointer,
        event: wl_pointer::Event,
        udata: &U,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(view) = this.id.as_ref().and_then(|id| this.views.get_mut(id)) {
            match event {
                wl_pointer::Event::Button {
                    serial,
                    time,
                    button,
                    state,
                } => {
                    if !view.state.pending_cb {
                        view.state.time = time;
                    }
                    view.state.last_serial = serial;
                    view.cursor.pointer = Pointer::MouseClick {
                        serial,
                        button: MouseButton::new(button),
                        pressed: if let WEnum::Value(state) = state {
                            state == ButtonState::Pressed
                        } else {
                            false
                        },
                    };
                }
                wl_pointer::Event::Axis { time, axis, value } => {
                    if !view.state.pending_cb {
                        view.state.time = time;
                    }
                    if let WEnum::Value(axis) = axis {
                        view.cursor.pointer = Pointer::Scroll {
                            orientation: match axis {
                                Axis::VerticalScroll => Orientation::Vertical,
                                Axis::HorizontalScroll => Orientation::Horizontal,
                                _ => unreachable!(),
                            },
                            step: Step::Value(value as f32),
                        };
                    }
                }
                wl_pointer::Event::AxisDiscrete { axis, discrete } => {
                    if let WEnum::Value(axis) = axis {
                        view.cursor.pointer = Pointer::Scroll {
                            orientation: match axis {
                                Axis::VerticalScroll => Orientation::Vertical,
                                Axis::HorizontalScroll => Orientation::Horizontal,
                                _ => unreachable!(),
                            },
                            step: Step::Increment(discrete),
                        };
                    }
                }
                wl_pointer::Event::Motion {
                    time,
                    surface_x,
                    surface_y,
                } => {
                    view.cursor.pointer = Pointer::Hover;
                    view.cursor.position = Position::new(surface_x as f32, surface_y as f32);
                    if !view.state.pending_cb {
                        view.state.time = time;
                    }
                }
                wl_pointer::Event::Frame => {
                    let cursor = view.cursor;
                    let serial = view.state.last_serial;
                    if cursor.pointer.click().is_some()
                        && view
                            .surface
                            .object_data()
                            .map(|udata| Arc::strong_count(udata) > 2)
                            .unwrap_or_default()
                    {
                        let id = SurfaceId(view.surface.id());
                        this.destroy_children_popup(id);
                    }
                    match cursor.pointer {
                        Pointer::Enter => {
                            this.dispatch(Event::Focus(Focus::global_focus().serial(serial)), qh);
                        }
                        Pointer::Leave => {
                            this.dispatch(Event::Focus(Focus::global_unfocus().serial(serial)), qh);
                        }
                        _ => {
                            this.dispatch(Event::Pointer(cursor), qh);
                        }
                    }
                }
                wl_pointer::Event::Leave { serial, .. } => {
                    view.state.last_serial = serial;
                    view.cursor.pointer = Pointer::Leave;
                }
                wl_pointer::Event::Enter {
                    serial,
                    surface_x,
                    surface_y,
                    surface,
                } => {
                    view.handle(conn, qh).set_cursor(Cursor::Arrow);
                    view.state.enter_serial = serial;
                    view.state.last_serial = serial;
                    view.cursor.position = Position::new(surface_x as f32, surface_y as f32);
                    view.cursor.pointer = Pointer::Enter;
                    this.id = Some(SurfaceId(surface.id()));
                }
                _ => {}
            }
        } else {
            match event {
                wl_pointer::Event::Enter { ref surface, .. } => {
                    this.id = Some(SurfaceId(surface.id()));
                    Self::event(data, wl_pointer, event, udata, conn, qh);
                }
                _ => {}
            }
        }
    }
}

impl<T, U, D> Dispatch<wl_output::WlOutput, U, D> for WaylandClientInner<T, D>
where
    T: 'static,
    D: 'static,
    D: AsMut<Self> + ViewDispatch,
    D: Dispatch<wl_output::WlOutput, U>,
{
    fn event(
        data: &mut D,
        wl_output: &wl_output::WlOutput,
        event: <wl_output::WlOutput as WlProxy>::Event,
        udata: &U,
        conn: &Connection,
        qh: &QueueHandle<D>,
    ) {
        let this = data.as_mut();
        if let Some(output) = this
            .globals
            .borrow_mut()
            .outputs
            .get_mut(&wl_output.id().try_into().unwrap())
        {
            match event {
                wl_output::Event::Geometry {
                    physical_width,
                    physical_height,
                    model,
                    ..
                } => {
                    output.name = model;
                    output.physical_width = physical_width;
                    output.physical_height = physical_height;
                }
                wl_output::Event::Mode {
                    flags: _,
                    width,
                    height,
                    refresh,
                } => {
                    output.width = width;
                    output.height = height;
                    output.refresh = refresh;
                }
                wl_output::Event::Name { name } => {
                    output.name = name;
                }
                wl_output::Event::Scale { factor } => {
                    output.scale = factor;
                }
                wl_output::Event::Done => {}
                _ => {}
            }
            return;
        }
        data.as_mut().globals.borrow_mut().outputs.insert(
            wl_output.id().try_into().unwrap(),
            Output::new(wl_output.clone()),
        );
        Self::event(data, wl_output, event, udata, conn, qh);
    }
}

macro_rules! dispatch {
    ($interface: ty, $udata: ty) => {
        impl<T> Dispatch<$interface, $udata> for WaylandClient<T>
        where
            T: 'static,
        {
            fn event(
                state: &mut Self,
                proxy: &$interface,
                event: <$interface as WlProxy>::Event,
                data: &$udata,
                conn: &Connection,
                qhandle: &QueueHandle<Self>,
            ) {
                <WaylandClientInner<T, Self> as Dispatch<$interface, $udata, Self>>::event(
                    state, proxy, event, data, conn, qhandle,
                )
            }

            fn event_created_child(
                opcode: u16,
                qhandle: &QueueHandle<Self>,
            ) -> ::std::sync::Arc<dyn backend::ObjectData> {
                <WaylandClientInner<T, Self> as Dispatch<$interface, $udata, Self>>::event_created_child(
                    opcode, qhandle,
                )
            }
        }
    };
}

macro_rules! default_dispatch {
    ($interface: ty) => {
        impl<T, D> Dispatch<$interface, (), D> for WaylandClientInner<T, D>
        where
            T: 'static,
            D: AsMut<Self>,
            D: Dispatch<$interface, ()>,
        {
            fn event(
                _: &mut D,
                _: &$interface,
                _: <$interface as WlProxy>::Event,
                _: &(),
                _: &Connection,
                _: &QueueHandle<D>,
            ) {
            }
        }

        impl<T> Dispatch<$interface, ()> for WaylandClient<T>
        where
            T: 'static,
        {
            fn event(
                state: &mut Self,
                proxy: &$interface,
                event: <$interface as WlProxy>::Event,
                data: &(),
                conn: &Connection,
                qhandle: &QueueHandle<Self>,
            ) {
                <WaylandClientInner<T, Self> as Dispatch<$interface, (), Self>>::event(
                    state, proxy, event, data, conn, qhandle,
                )
            }

            fn event_created_child(
                opcode: u16,
                qhandle: &QueueHandle<Self>,
            ) -> ::std::sync::Arc<dyn backend::ObjectData> {
                <WaylandClientInner<T, Self> as Dispatch<$interface, (), Self>>::event_created_child(
                    opcode, qhandle,
                )
            }
        }
    };
}

// Dispatch implementations for WaylandClient
dispatch!(wl_output::WlOutput, ());
dispatch!(wl_pointer::WlPointer, ());
dispatch!(wl_seat::WlSeat, ());
dispatch!(wl_registry::WlRegistry, ());
dispatch!(wl_surface::WlSurface, ());
dispatch!(wl_data_offer::WlDataOffer, ());
dispatch!(wl_data_device::WlDataDevice, ());
dispatch!(wl_keyboard::WlKeyboard, ());
dispatch!(xdg_wm_base::XdgWmBase, ());
dispatch!(xdg_surface::XdgSurface, SurfaceId);
dispatch!(xdg_toplevel::XdgToplevel, SurfaceId);
dispatch!(xdg_popup::XdgPopup, SurfaceId);
dispatch!(wl_callback::WlCallback, SurfaceId);
dispatch!(zwlr_layer_surface_v1::ZwlrLayerSurfaceV1, SurfaceId);
dispatch!(wl_shm::WlShm, ());
default_dispatch!(zwlr_layer_shell_v1::ZwlrLayerShellV1);
default_dispatch!(wl_buffer::WlBuffer);
default_dispatch!(wl_compositor::WlCompositor);
default_dispatch!(wl_shm_pool::WlShmPool);
default_dispatch!(wl_subcompositor::WlSubcompositor);
default_dispatch!(wl_data_device_manager::WlDataDeviceManager);
default_dispatch!(wl_subsurface::WlSubsurface);
default_dispatch!(wl_region::WlRegion);
default_dispatch!(xdg_positioner::XdgPositioner);
