//! Implementation of a wayland backend.

mod backend;

pub use backend::*;

use smithay_client_toolkit::reexports::client::{
    backend::ObjectId,
    protocol::wl_buffer::WlBuffer,
    protocol::wl_compositor::WlCompositor,
    protocol::wl_data_device_manager::WlDataDeviceManager,
    protocol::wl_keyboard,
    protocol::wl_output::WlOutput,
    protocol::wl_pointer::WlPointer,
    protocol::wl_region::WlRegion,
    protocol::wl_registry::WlRegistry,
    protocol::wl_seat::{Capability, WlSeat},
    protocol::wl_shm::{Format, WlShm},
    protocol::wl_subcompositor::WlSubcompositor,
    protocol::wl_surface::WlSurface,
    Proxy as WlProxy, WEnum,
};
use smithay_client_toolkit::reexports::{
    protocols::xdg::shell::client::{
        xdg_popup, xdg_positioner, xdg_surface, xdg_toplevel, xdg_wm_base,
    },
    protocols_wlr::layer_shell::v1::client::{
        zwlr_layer_shell_v1::{Layer, ZwlrLayerShellV1},
        zwlr_layer_surface_v1::{Anchor, KeyboardInteractivity, ZwlrLayerSurfaceV1},
    },
};
use wayland_cursor::CursorTheme;

use smithay_client_toolkit::shm::multi::{MultiPool, PoolError};
use snui::tiny_skia::PixmapMut;
use snui::widgets::Alignment;
use snui::Backend;
use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};

/// A surface identifier.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SurfaceId(pub(crate) ObjectId);

impl TryFrom<ObjectId> for SurfaceId {
    type Error = ObjectId;

    fn try_from(value: ObjectId) -> Result<Self, Self::Error> {
        if value.interface().name == WlSurface::interface().name {
            Ok(SurfaceId(value))
        } else {
            Err(value)
        }
    }
}

fn buffer<'b>(
    pool: &'b mut MultiPool<SurfaceId>,
    width: u32,
    height: u32,
    format: Format,
    surface: &SurfaceId,
) -> Result<(usize, &'b WlBuffer, Backend<'b>), PoolError> {
    let stride = width * 4;
    pool.create_buffer(width as i32, stride as i32, height as i32, surface, format)
        .map(|(offset, buffer, slice)| {
            PixmapMut::from_bytes(slice, width, height)
                .map(|pixmap| {
                    (
                        offset,
                        buffer,
                        Backend::Pixmap {
                            pixmap,
                            clipmask: None,
                        },
                    )
                })
                .unwrap()
        })
}

#[derive(Debug, Clone)]
/// A layer surface builder.
pub struct LayerSurfaceBuilder {
    pub layer: Layer,
    pub output: String,
    pub anchor: Option<Anchor>,
    pub exclusive: bool,
    pub interactivity: KeyboardInteractivity,
    pub margin: [i32; 4],
}

/// Analog to [`Layer`].
pub enum DesktopLayer {
    Overlay,
    Top,
    Bottom,
    Background,
}

impl From<DesktopLayer> for Layer {
    fn from(layer: DesktopLayer) -> Layer {
        match layer {
            DesktopLayer::Background => Layer::Background,
            DesktopLayer::Bottom => Layer::Bottom,
            DesktopLayer::Top => Layer::Top,
            DesktopLayer::Overlay => Layer::Overlay,
        }
    }
}

impl LayerSurfaceBuilder {
    pub fn set_anchor(&mut self, x: Alignment, y: Alignment) {
        let mut anchor = self.anchor.unwrap_or(Anchor::empty());
        match x {
            Alignment::Start => anchor.insert(Anchor::Left),
            Alignment::End => anchor.insert(Anchor::Right),
            _ => {}
        }
        match y {
            Alignment::Start => anchor.insert(Anchor::Top),
            Alignment::End => anchor.insert(Anchor::Bottom),
            _ => {}
        }
        self.anchor = Some(anchor);
    }
    pub fn anchor(mut self, x: Alignment, y: Alignment) -> Self {
        self.set_anchor(x, y);
        self
    }
    pub fn output(mut self, name: String) -> Self {
        self.output = name;
        self
    }
    pub fn exclusive(mut self) -> Self {
        self.exclusive = true;
        self
    }
    pub fn keyboard_grab(mut self) -> Self {
        self.interactivity = KeyboardInteractivity::Exclusive;
        self
    }
    pub fn keyboard_interactive(mut self) -> Self {
        self.interactivity = KeyboardInteractivity::OnDemand;
        self
    }
    pub fn margin(mut self, top: i32, right: i32, bottom: i32, left: i32) -> Self {
        self.margin = [top, right, bottom, left];
        self
    }
    pub fn layer(mut self, layer: DesktopLayer) -> Self {
        self.layer = layer.into();
        self
    }
    pub fn background(mut self) -> Self {
        self.layer = Layer::Background;
        self.anchor = Some(Anchor::all());
        self
    }
    pub fn fullscreen(mut self) -> Self {
        self.layer = Layer::Overlay;
        self.anchor = Some(Anchor::all());
        self
    }
}

impl Default for LayerSurfaceBuilder {
    fn default() -> Self {
        Self {
            layer: Layer::Top,
            output: String::new(),
            anchor: None,
            exclusive: false,
            interactivity: KeyboardInteractivity::None,
            margin: [0; 4],
        }
    }
}

#[derive(Debug, Clone)]
enum Shell {
    LayerSurface {
        config: LayerSurfaceBuilder,
        layer_surface: ZwlrLayerSurfaceV1,
    },
    Toplevel {
        parent: Option<WlSurface>,
        xdg_surface: xdg_surface::XdgSurface,
        toplevel: xdg_toplevel::XdgToplevel,
    },
    Popup {
        parent: Option<WlSurface>,
        xdg_surface: xdg_surface::XdgSurface,
        positioner: xdg_positioner::XdgPositioner,
        popup: xdg_popup::XdgPopup,
    },
}

impl Shell {
    pub fn destroy(&self) {
        match self {
            Shell::LayerSurface {
                config: _,
                layer_surface,
            } => {
                layer_surface.destroy();
            }
            Self::Toplevel {
                xdg_surface,
                toplevel,
                ..
            } => {
                toplevel.destroy();
                xdg_surface.destroy();
            }
            Self::Popup {
                xdg_surface,
                positioner,
                popup,
                ..
            } => {
                positioner.destroy();
                popup.destroy();
                xdg_surface.destroy();
            }
        }
    }
    pub fn popup(&self) -> Option<&xdg_popup::XdgPopup> {
        match self {
            Self::Popup { popup, .. } => Some(popup),
            Self::Toplevel { .. } => None,
            Shell::LayerSurface { .. } => None,
        }
    }
    pub fn positioner(&self) -> Option<&xdg_positioner::XdgPositioner> {
        match self {
            Self::Popup { positioner, .. } => Some(positioner),
            Self::Toplevel { .. } => None,
            Shell::LayerSurface { .. } => None,
        }
    }
    pub fn toplevel(&self) -> Option<&xdg_toplevel::XdgToplevel> {
        match self {
            Self::Toplevel { toplevel, .. } => Some(toplevel),
            _ => None,
        }
    }
    pub fn xdg_surface(&self) -> Option<&xdg_surface::XdgSurface> {
        match self {
            Self::Toplevel { xdg_surface, .. } => Some(xdg_surface),
            Self::Popup { xdg_surface, .. } => Some(xdg_surface),
            Shell::LayerSurface { .. } => None,
        }
    }
    pub fn layer_surface(&self) -> Option<&ZwlrLayerSurfaceV1> {
        match self {
            Shell::LayerSurface { layer_surface, .. } => Some(layer_surface),
            _ => None,
        }
    }
    pub fn parent(&self) -> Option<&WlSurface> {
        match self {
            Self::Popup { parent, .. } | Self::Toplevel { parent, .. } => parent.as_ref(),
            _ => None,
        }
    }
    pub fn set_parent(&mut self, surface: WlSurface) {
        match self {
            Self::Popup { parent, .. } | Self::Toplevel { parent, .. } => *parent = Some(surface),
            _ => {}
        }
    }
}

#[derive(Debug, Clone)]
struct Surface {
    shell: Shell,
    wl_region: WlRegion,
    wl_surface: WlSurface,
    output: Option<OutputId>,
    previous: Option<Box<Self>>,
}

impl Drop for Surface {
    fn drop(&mut self) {
        self.shell.destroy();
        self.wl_region.destroy();
        self.wl_surface.destroy();
    }
}

/// An [`Output`] identifier.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct OutputId(ObjectId);

impl TryFrom<ObjectId> for OutputId {
    type Error = ObjectId;

    fn try_from(value: ObjectId) -> Result<Self, Self::Error> {
        if value.interface().name == WlOutput::interface().name {
            Ok(OutputId(value))
        } else {
            Err(value)
        }
    }
}

/// The state of a [`WlOutput`].
#[derive(Debug, Clone)]
pub struct Output {
    pub width: i32,
    pub height: i32,
    pub scale: i32,
    pub physical_width: i32,
    pub physical_height: i32,
    pub name: String,
    pub output: WlOutput,
    pub refresh: i32,
}

impl Output {
    pub fn new(output: WlOutput) -> Self {
        Output {
            output,
            name: String::new(),
            refresh: 60,
            scale: 1,
            height: 1080,
            width: 1920,
            physical_height: 1080,
            physical_width: 1920,
        }
    }
}

impl Deref for Output {
    type Target = WlOutput;

    fn deref(&self) -> &Self::Target {
        &self.output
    }
}

use xkbcommon::xkb::State;

#[derive(Clone)]
struct KeyboardState {
    pub(crate) rate: i32,
    pub(crate) delay: i32,
    pub(crate) state: State,
    pub(crate) keyboard: wl_keyboard::WlKeyboard,
}

impl Deref for KeyboardState {
    type Target = wl_keyboard::WlKeyboard;

    fn deref(&self) -> &Self::Target {
        &self.keyboard
    }
}

#[derive(Clone)]
struct Seat {
    pub seat: WlSeat,
    pub name: String,
    pub pointer: Option<WlPointer>,
    pub keyboard: Option<KeyboardState>,
    pub capabilities: WEnum<Capability>,
}

impl Deref for Seat {
    type Target = WlSeat;

    fn deref(&self) -> &Self::Target {
        &self.seat
    }
}

impl Seat {
    fn new(seat: WlSeat) -> Self {
        Self {
            seat,
            pointer: None,
            keyboard: None,
            name: String::new(),
            capabilities: WEnum::Unknown(0),
        }
    }
}

pub struct GlobalManager {
    app_id: String,
    outputs: HashMap<OutputId, Output>,
    seats: Vec<Seat>,
    shm: Option<WlShm>,
    formats: Vec<Format>,
    registry: WlRegistry,
    device_manager: Option<WlDataDeviceManager>,
    pointer_surface: Option<WlSurface>,
    compositor: Option<WlCompositor>,
    cursor_theme: HashMap<u32, CursorTheme>,
    subcompositor: Option<WlSubcompositor>,
    wm_base: Option<xdg_wm_base::XdgWmBase>,
    layer_shell: Option<ZwlrLayerShellV1>,
}

impl GlobalManager {
    pub fn new(registry: WlRegistry) -> Self {
        Self {
            app_id: String::from("snui"),
            outputs: HashMap::new(),
            seats: Vec::new(),
            formats: Vec::new(),
            shm: None,
            registry,
            device_manager: None,
            pointer_surface: None,
            cursor_theme: HashMap::new(),
            compositor: None,
            subcompositor: None,
            wm_base: None,
            layer_shell: None,
        }
    }
    pub fn formats(&self) -> &[Format] {
        self.formats.as_slice()
    }
    pub fn outputs(&self) -> impl Iterator<Item = &Output> {
        self.outputs.values()
    }
    pub fn destroy(&self) {
        if let Some(subcompositor) = &self.subcompositor {
            subcompositor.destroy();
        }
        if let Some(wm_base) = &self.wm_base {
            wm_base.destroy();
        }
        if let Some(pointer_surface) = &self.pointer_surface {
            pointer_surface.destroy();
        }
        if let Some(layer_shell) = &self.layer_shell {
            layer_shell.destroy();
        }
        for output in self.outputs.values() {
            output.output.release();
        }
        for seat in &self.seats {
            seat.seat.release();
        }
    }
}

impl Deref for GlobalManager {
    type Target = WlRegistry;
    fn deref(&self) -> &Self::Target {
        &self.registry
    }
}

impl DerefMut for GlobalManager {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.registry
    }
}
