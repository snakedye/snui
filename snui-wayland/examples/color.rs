use msg_api::*;
use snui::environment::*;
use snui::scene::*;
use snui::widgets::*;
use snui::*;
use snui_wayland::*;

// The application data.
#[derive(Clone, Debug)]
struct Color {
    color: tiny_skia::Color,
}

Message! {
    /// The color in hex format.
    Hex<String>
}

Messages! {
    ColorChannel<f32> {
        Blue,
        Red,
        Green,
        Alpha,
    }
}

MessageHandler! {
    Color {
        ColorChannel => (this, id, value) {
            match id {
                ColorChannel::Red => this.color.set_red(value),
                ColorChannel::Green => this.color.set_green(value),
                ColorChannel::Blue => this.color.set_blue(value),
                ColorChannel::Alpha => this.color.set_alpha(value),
            }
        }
    }
}

impl FieldParameter for ColorChannel {}

RequestManager! {
    Color {
        Hex => (this, desc) {
            desc.from(format!("{:#010X}", this.color.to_color_u8().get()).replace("0x", "#"))
        }
        ColorChannel => (this, desc) {
            match desc {
                ColorChannel::Red => desc.from(this.color.red()),
                ColorChannel::Green => desc.from(this.color.green()),
                ColorChannel::Blue => desc.from(this.color.blue()),
                ColorChannel::Alpha => desc.from(this.color.alpha()),
            }
        }
    }
}

#[derive(Clone, Debug)]
struct ColorBlock {
    rect: Rectangle,
    position: Position,
}

impl Widget<Color> for ColorBlock {
    fn draw_scene(&mut self, mut scene: Scene, _env: &Env) {
        self.position = scene.position();
        scene.borrow().draw(&self.rect)
    }
    fn update(&mut self, ctx: &mut UpdateContext<Color>, _env: &Env) -> Damage {
        ctx.prepare();
        self.rect.set_texture(ctx.color);
        self.rect.damage()
    }
    fn event(&mut self, ctx: &mut UpdateContext<Color>, event: Event, _: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer,
                ref position,
            }) => {
                if let Some(serial) = pointer
                    .left_button_click()
                    .filter(|_| self.rect.contains(position))
                {
                    ctx.create_popup(|ctx| {
                        let label = popup(ctx);
                        PopupBuilder::default()
                            .serial(serial)
                            .anchor(START, START)
                            .offset(self.position + *position)
                            .size(Size::new(110., 30.))
                            .finish(label)
                    })
                }
            }
            _ => {}
        }
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<Color>, bc: &BoxConstraints, _env: &Env) -> Size {
        let size = bc.maximum_width().min(bc.maximum_height());
        self.rect.set_size(size, size);
        self.rect.size()
    }
}

fn popup(color: &Color) -> impl Widget<Color> {
    Label::from(format!("{:#010X}", color.color.to_color_u8().get()).replace("0x", "#"))
        .class("important")
        .background(color.color)
        .border(color::BG2, 1.)
        .padding(5.)
        .radius(RelativeUnit::Unit(5.))
}

fn sliders() -> impl Widget<Color> {
    [
        ColorChannel::Red,
        ColorChannel::Green,
        ColorChannel::Blue,
        ColorChannel::Alpha,
    ]
    .into_iter()
    .map(|channel| {
        Slider::new(channel)
            .class(match channel {
                ColorChannel::Red => "accent-color-6",
                ColorChannel::Green => "accent-color-2",
                ColorChannel::Blue => "accent-color-3",
                _ => "",
            })
            .with_fixed_height(10.)
            .anchor(START, CENTER)
            .padding_top(2.)
            .padding_bottom(2.)
    })
    .collect::<Row<_, _>>()
}

fn ui_builder() -> impl Widget<Color> {
    Row!(
        Row!(
            DynLabel::new(Hex).class("important").with_fixed_height(50.),
            ColorBlock {
                position: Position::default(),
                rect: Rectangle::default().radius(RelativeUnit::Percent(5.))
            }
            .with_min_size(200., 200.)
            .with_max_size(600., 600.)
            .flex(Orientation::Horizontal)
        )
        .clamp(),
        sliders().with_max_width(800.).clamp()
    )
}

fn main() {
    let color = Color {
        color: tiny_skia::Color::WHITE,
    };
    WaylandClient::init(color, |client, qh| {
        client.set_app_id("org.snui.example.color");
        let window = default_window(
            Label::from("Color").class("title"),
            ui_builder().clamp().padding(10.),
        );
        let mut view = client.create_window(window, qh);
        view.set_min_size(250, 400);
        view.set_title("Color".to_owned());
    })
}
