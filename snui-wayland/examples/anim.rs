use msg_api::{FieldParameter, MessageBuilder, MessageHandler, Messages, RequestManager};
use snui::{environment::*, utils::*, widgets::*, *};
use snui_wayland::*;

/// The duration of the animation.
const DURATION: u32 = 2000;

Messages! {
    Control<bool>,
}

Messages! {
    AnimationState<()> {
        Stop,
        Start,
        Pause
    }
}

MessageHandler! {
    AnimationState {
        Control => (this, msg) {
            if msg.get() {
                *this = AnimationState::Start;
            } else {
                *this = AnimationState::Pause;
            }
        }
    }
}

MessageHandler! {
    AnimationState {
        AnimationState => (this, id, _) {
            *this = id;
        }
    }
}

RequestManager! {
    AnimationState {
        Control => (this, desc) {
            desc.from(AnimationState::Start.eq(this))
        }
        AnimationState => (this, _) {
            this.from(())
        }
    }
}

impl FieldParameter for Control {}
impl FieldParameter for AnimationState {}

// Moves a rectangle across a box
struct Animate<E: Easer> {
    start: bool,
    cursor: Positioner<Proxy<AnimationState, Rectangle>>,
    easer: E,
}

impl<E: Easer> Geometry for Animate<E> {
    fn width(&self) -> f32 {
        400.
    }
    fn height(&self) -> f32 {
        30.
    }
}

impl<E: Easer> Widget<AnimationState> for Animate<E> {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        Widget::<AnimationState>::draw_scene(&mut self.cursor, scene, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<AnimationState>, env: &Env) -> Damage {
        ctx.prepare();
        match **ctx {
            AnimationState::Start => {
                self.start = true;
                if self.easer.step_by(0.).is_none() {
                    self.easer = Easer::new(0., 1.);
                }
                return Damage::Frame(self.cursor.touched());
            }
            AnimationState::Pause => {
                self.start = false;
            }
            AnimationState::Stop => {
                self.start = false;
            }
        }
        Widget::<AnimationState>::update(&mut self.cursor, ctx, env)
    }
    fn event(
        &mut self,
        ctx: &mut UpdateContext<AnimationState>,
        event: Event,
        env: &Env,
    ) -> Damage {
        match event {
            Event::Callback(frame_time) => {
                if self.start {
                    let steps = self.easer.partial_steps(frame_time, DURATION);
                    let position = self.easer.step_by(steps);
                    self.cursor.set_position(
                        (self.width() - 20.) * position.unwrap_or_else(|| self.easer.position()),
                        0.,
                    );
                    if position.is_none() {
                        ctx.post(&AnimationState::Stop, ());
                        self.start = false;
                    }
                    return Damage::Frame(self.cursor.touched());
                }
            }
            _ => {}
        }
        Widget::<AnimationState>::event(&mut self.cursor, ctx, event, env)
    }
    fn layout(
        &mut self,
        ctx: &mut LayoutContext<AnimationState>,
        bc: &BoxConstraints,
        env: &Env,
    ) -> Size {
        Widget::<AnimationState>::layout(&mut self.cursor, ctx, &bc.with_max(20., 30.), env);
        self.size()
    }
}

impl<E: Easer> Animate<E> {
    fn new() -> Self {
        Animate {
            start: false,
            cursor: Positioner::new(Proxy::new(Rectangle::new(20., 30.).texture(color::RED))),
            easer: Easer::new(0., 1.),
        }
    }
}

// Displays the frame rate of the animation.
struct FrameRate {
    label: Label,
}

impl<T> Widget<T> for FrameRate {
    fn draw_scene(&mut self, scene: scene::Scene, _env: &Env) {
        scene.draw(&self.label)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.label.update(ctx, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        if let Event::Callback(frame_time) = event {
            if frame_time > 0 {
                let frame_rate = 1000 / frame_time;
                self.label.edit(&frame_rate.to_string());
            }
        }
        self.label.event(ctx, event, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        Widget::<T>::layout(&mut self.label, ctx, bc, env)
    }
}

// Creates our user interface.
fn ui() -> impl Widget<AnimationState> {
    Row!(
        FrameRate {
            label: "frame rate".into(),
        }
        .with_min_height(20.),
        Animate::<Quadratic>::new().clamp(),
        Animate::<Sinus>::new().clamp(),
        Toggle::<_, Quadratic>::new(Control)
            .radius(RelativeUnit::Percent(50.))
            .texture(color::BG0)
            .duration(200)
            .background(color::BG2)
            .radius(RelativeUnit::Percent(50.))
            .activate(Control, |this, active, _, _| match active {
                true => this.set_texture(color::RED),
                false => this.set_texture(color::BG2),
            })
            .clamp(),
    )
}

fn main() {
    WaylandClient::init(AnimationState::Stop, |client, qh| {
        client.set_app_id("org.snui.example.anim");
        let window = default_window(Label::from("Animation").class("title"), ui());
        let mut view = client.create_window(window, qh);
        view.set_min_size(410, 200);
        view.set_title("Animation".to_string())
    })
}
