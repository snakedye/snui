# snui

**snui** is a simple gui library.

## Dependencies 

- rust
- libxkbcommon (linux)
- fontconfig (linux)

## Example

Here's a simple window with a label.

```rust
use snui::{utils::*, widgets::*, *};
use snui_wayland::*;

fn ui() -> impl Widget<()> {
	default_window(Label::from("Window"), Label::from("Hello World!").clamp())
}

fn main() {
    WaylandClient::init((), |client, qh| {
        let mut view = client.create_window(ui(), qh);
        view.set_title("Example".to_string());
    })
}
```

### Related projects

Name    |   Description     
:-------|:-------------------
[siv](https://gitlab.com/snakedye/siv)     |   An image viewer |
[salut](https://gitlab.com/snakedye/salut)   |   A notification daemon   |
[snui-adwaita](https://gitlab.com/snakedye/snui-adwaita)   |   A snui library implementing GNOME's Human interface guidelines.   |
