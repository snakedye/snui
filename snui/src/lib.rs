//! A simple GUI library.

pub mod cache;
mod context;
mod core;
pub mod environment;
mod keyboard;
mod mouse;
pub mod scene;
pub mod widgets;

pub mod utils;

pub use crate::core::*;
pub use context::*;
pub use keyboard::*;
pub use mouse::*;
pub use msg_api;
use std::ops::{Deref, DerefMut};
pub use tiny_skia;
use tiny_skia::*;
use widgets::Activate;
use widgets::Button;
use widgets::Positioner;
use widgets::WidgetBox;
use widgets::WidgetStyle;

/// Event.
///
/// Corresponds to an event from the platform or a message from the user.
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Event<'a> {
    /// Sent by the platform when the application needs to be reconfigured.
    Configure,
    Focus(Focus),
    Message(&'a str),
    // Sent on a frame callback with the frame time in ms.
    Callback(u32),
    // Keyboard key press.
    Keyboard(Key<'a>),
    /// Pointer position and type.
    Pointer(MouseEvent),
    Action(u32, &'a str),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum FocusType {
    Local,
    Global,
    Pointer,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Focus {
    pub state: bool,
    pub serial: Option<u32>,
    pub focus_type: FocusType,
}

impl std::ops::Not for Focus {
    type Output = Self;
    fn not(self) -> Self::Output {
        Self {
            serial: None,
            state: !self.state,
            focus_type: self.focus_type,
        }
    }
}

impl Focus {
    pub fn serial(self, serial: u32) -> Self {
        Self {
            serial: Some(serial),
            focus_type: self.focus_type,
            state: self.state,
        }
    }
    pub fn local_focus() -> Self {
        Self {
            serial: None,
            focus_type: FocusType::Local,
            state: true,
        }
    }
    pub fn local_unfocus() -> Self {
        Self {
            serial: None,
            focus_type: FocusType::Local,
            state: false,
        }
    }
    pub fn pointer_focus() -> Self {
        Self {
            serial: None,
            focus_type: FocusType::Pointer,
            state: true,
        }
    }
    pub fn pointer_unfocus() -> Self {
        Self {
            serial: None,
            focus_type: FocusType::Pointer,
            state: false,
        }
    }
    pub fn global_focus() -> Self {
        Self {
            serial: None,
            focus_type: FocusType::Global,
            state: true,
        }
    }
    pub fn global_unfocus() -> Self {
        Self {
            serial: None,
            focus_type: FocusType::Global,
            state: false,
        }
    }
    pub fn is_local_focus(&self) -> bool {
        matches!(self.focus_type, FocusType::Local) && self.state
    }
    pub fn is_local_unfocus(&self) -> bool {
        matches!(self.focus_type, FocusType::Local) && self.state
    }
    pub fn is_pointer_focus(&self) -> bool {
        matches!(self.focus_type, FocusType::Pointer) && self.state
    }
    pub fn is_pointer_unfocus(&self) -> bool {
        matches!(self.focus_type, FocusType::Pointer) && self.state
    }
    pub fn is_global_focus(&self) -> bool {
        matches!(self.focus_type, FocusType::Global) && self.state
    }
    pub fn is_global_unfocus(&self) -> bool {
        matches!(self.focus_type, FocusType::Global) && self.state
    }
}

impl<'a> Event<'a> {
    pub fn is_callback(&self) -> bool {
        matches!(self, Self::Callback(_))
    }
    pub fn is_configure(&self) -> bool {
        matches!(self, Self::Configure)
    }
}

/// An object with a dimension.
pub trait Geometry {
    fn width(&self) -> f32;
    fn height(&self) -> f32;
    fn size(&self) -> Size {
        Size::new(self.width(), self.height())
    }
    fn contains(&self, position: &Position) -> bool {
        position.x.is_sign_positive()
            && position.y.is_sign_positive()
            && position.x < self.width()
            && position.y < self.height()
    }
}

/// A drawable object.
///
/// They are given access to the drawing backend.
pub trait Drawable: Geometry {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform);
}

use scene::{Position, Scene};

/// The trait that defines all widgets.
pub trait Widget<T> {
    /// This method is called after `event` if the application was mutated.
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage;
    /// Handle an event.
    ///
    /// The widget is provided an [`Event`] and the [`UpdateContext`].
    /// The application can be accessed from the context.
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage;
    /// The layout is expected to be computed here.
    ///
    /// Widgets are layouted before draw.
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size;
    /// Update the scene.
    ///
    /// The [`Scene`] is wrapper around the graphical state of your application.
    fn draw_scene(&mut self, scene: Scene, env: &Env);
}
