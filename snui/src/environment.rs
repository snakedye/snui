use super::scene::Texture;
use super::utils::List;
use crate::widgets::Style;
use crate::*;
use std::convert::TryFrom;
use std::marker::PhantomData;
use std::str::FromStr;

pub mod color {
    //! <https://gitlab.com/snakedye/chocolate>
    use crate::scene::Texture;
    pub const FG0: u32 = 0xff_C8_BA_A4;
    pub const FG1: u32 = 0xff_cd_c0_ad;
    pub const FG2: u32 = 0xff_be_ae_94;
    pub const BG0: u32 = 0xff_25_22_21;
    pub const BG1: u32 = 0xa0_26_23_22;
    pub const BG2: u32 = 0xff_30_2c_2b;
    pub const BG3: u32 = 0xff_3d_38_37;
    pub const BG4: u32 = 0xff_41_3c_3a;
    pub const YELLOW: u32 = 0xff_d9_b2_7c;
    pub const GREEN: u32 = 0xff_95_a8_82;
    pub const BLUE: u32 = 0xff_72_87_97;
    pub const PURPLE: u32 = 0xff_99_83_96;
    pub const BEIGE: u32 = 0xff_ab_93_82;
    pub const ORANGE: u32 = 0xff_d0_8b_65;
    pub const RED: u32 = 0xff_c6_5f_5f;
    pub const TRANSPARENT: Texture = Texture::Transparent;
}

/// Message that signal that the style should be updated.
pub const STYLESHEET_MESSAGE: &str = "theme";
/// Event on which the style should be updated.
pub const STYLESHEET_SIGNAL: Event<'static> = Event::Message(STYLESHEET_MESSAGE);

/// An unit with an absolute or relative value.
///
/// The absolute value is a [`f32`] and the relative value is a percentage.
#[derive(Clone, Debug, PartialEq, Copy)]
pub enum RelativeUnit {
    Unit(f32),
    Percent(f32),
}

impl Default for RelativeUnit {
    fn default() -> Self {
        Self::Unit(0.)
    }
}

impl RelativeUnit {
    /// Returns the absolute value in the [`RelativeUnit`] otherwise returns an absolute value relative to the provided base.
    pub fn as_abs(self, base: f32) -> f32 {
        match self {
            Self::Unit(f) => f,
            Self::Percent(f) => f * base / 100.,
        }
    }
}

impl FromStr for RelativeUnit {
    type Err = ();
    fn from_str(_: &str) -> Result<Self, Self::Err> {
        Err(())
    }
}

/// A data type that represent the values the environment can provide.
#[derive(Clone, Debug, PartialEq)]
pub enum Value<'a> {
    Unit(RelativeUnit),
    Texture(Texture),
    Bool(bool),
    String(String),
    Str(&'a str),
    Int(i32),
    Usize(usize),
}

macro_rules! value {
    ($field:ident, $type:ty) => {
        impl<'a> TryFrom<Value<'a>> for $type {
            type Error = Value<'a>;
            fn try_from(value: Value<'a>) -> Result<Self, Self::Error> {
                match value {
                    Value::$field(t) => Ok(t),
                    Value::String(ref t) => t.parse::<$type>().map_err(|_| value),
                    Value::Str(t) => t.parse::<$type>().map_err(|_| value),
                    _ => Err(value),
                }
            }
        }

        impl From<$type> for Value<'_> {
            fn from(t: $type) -> Self {
                Value::$field(t)
            }
        }
    };
}

value!(Int, i32);
value!(Unit, RelativeUnit);
value!(Bool, bool);
value!(Usize, usize);

impl<'a> TryFrom<Value<'a>> for f32 {
    type Error = Value<'a>;
    fn try_from(value: Value<'a>) -> Result<Self, Self::Error> {
        match value {
            Value::Unit(RelativeUnit::Unit(f)) => Ok(f),
            _ => Err(value),
        }
    }
}

impl<'a> From<f32> for Value<'a> {
    fn from(f: f32) -> Self {
        Value::Unit(RelativeUnit::Unit(f))
    }
}

impl<'a> TryFrom<Value<'a>> for Texture {
    type Error = Value<'a>;
    fn try_from(value: Value<'a>) -> Result<Self, Self::Error> {
        match value {
            Value::Texture(texture) => Ok(texture),
            Value::Str(s) => s.parse().map_err(|_| value),
            _ => Err(value),
        }
    }
}

impl<'a> TryFrom<Value<'a>> for Color {
    type Error = Value<'a>;
    fn try_from(value: Value<'a>) -> Result<Self, Self::Error> {
        match value {
            Value::Texture(ref texture) => {
                if let Texture::Color(c) = texture {
                    Ok(*c)
                } else {
                    Err(value)
                }
            }
            _ => Err(value),
        }
    }
}

impl<'a> TryFrom<Value<'a>> for &'a str {
    type Error = Value<'a>;
    fn try_from(value: Value<'a>) -> Result<Self, Self::Error> {
        match value {
            Value::Str(string) => Ok(string),
            _ => Err(value),
        }
    }
}

impl<'a> TryFrom<Value<'a>> for String {
    type Error = Value<'a>;
    fn try_from(value: Value<'a>) -> Result<Self, Self::Error> {
        match value {
            Value::Str(string) => Ok(string.to_owned()),
            Value::String(string) => Ok(string),
            _ => Err(value),
        }
    }
}

impl<'a> From<&'a str> for Value<'a> {
    fn from(s: &'a str) -> Self {
        Value::Str(s)
    }
}

impl<'a, T> From<T> for Value<'a>
where
    T: Into<Texture>,
{
    fn from(texture: T) -> Self {
        Value::Texture(texture.into())
    }
}

/// An object that serves to identify items.
///
/// It can be used to extract related [`Value`]s from the [`Environment`].
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Id<'a> {
    item: &'a str,
    class: Option<&'a str>,
}

impl<'a> From<&'a str> for Id<'a> {
    fn from(item: &'a str) -> Self {
        Self { item, class: None }
    }
}

impl<'a> Id<'a> {
    pub fn new(item: &'a str, class: Option<&'a str>) -> Self {
        Self { item, class }
    }
    pub fn item(&self) -> &str {
        self.item
    }
    pub fn class(&self) -> Option<&str> {
        self.class
    }
}

impl Default for Id<'_> {
    fn default() -> Self {
        Id {
            item: "root",
            class: None,
        }
    }
}

/// An item that can be configured by the [`Environment`].
pub trait Themeable {
    /// The name of the themeable item.
    const NAME: &'static str;
    fn theme(&mut self, list: List<Id>, env: &dyn Environment);
    fn id<'i>(&self, class: Option<&'i str>) -> Id<'i> {
        Id {
            item: Self::NAME,
            class,
        }
    }
}

/// An environment provided to all widgets.
///
/// An environment is an object that holds data that doesn't necessaily fit inside the applications
/// and can be dynamically fetched.
///
/// It is notably used by widgets to fetch their style.
pub trait Environment {
    /// Retreive a [`Value`].
    fn get(&self, list: List<Id>, property: &str) -> Option<Value>;
}

/// A helper trait for [`Environment`]s.
pub trait EnvironmentExt: Environment {
    /// Retreives a [`Value`] and downcasts it to a type.
    fn try_get<'a, T>(&'a self, list: List<Id>, property: &str) -> Option<T>
    where
        T: TryFrom<Value<'a>>,
        T::Error: std::fmt::Debug,
    {
        Environment::get(self, list, property).and_then(|value| value.try_into().ok())
    }
}

impl<T> EnvironmentExt for T where T: Environment + ?Sized {}

impl Environment for () {
    fn get(&self, _list: List<Id>, _property: &str) -> Option<Value> {
        None
    }
}

/// A wrapper around your environment.
///
/// It additionaly holds state related to it.
#[derive(Clone, Copy)]
pub struct Env<'a, 'b> {
    list: List<'a, Id<'b>>,
    environment: &'a dyn Environment,
}

impl<'a, 'b> Env<'a, 'b> {
    pub fn new(environment: &'a dyn Environment) -> Self {
        Self {
            list: List::new(Id::default()),
            environment,
        }
    }
    pub fn map(&self, themeable: &mut impl Themeable) {
        themeable.theme(self.list, self.environment)
    }
    pub fn push_class(&'a self, class: &'b str) -> Env {
        self.push(Id::new(self.list.item(), Some(class)))
    }
    pub fn set_class(&self, class: &'b str) -> Self {
        Self {
            list: self
                .list
                .previous()
                .map(|list| {
                    list.push(Id {
                        item: self.list.item,
                        class: Some(class),
                    })
                })
                .unwrap(),
            environment: self.environment,
        }
    }
    pub fn push(&'a self, id: Id<'b>) -> Self {
        Self {
            list: self.list.push(id),
            environment: self.environment,
        }
    }
    pub fn get<'c, T>(&'c self, property: &str) -> Option<T>
    where
        T: TryFrom<Value<'c>>,
    {
        self.environment
            .get(self.list, property)
            .and_then(|value| value.try_into().ok())
    }
    pub fn list(&self) -> List<Id> {
        self.list
    }
}

impl<'a, 'b> Deref for Env<'a, 'b> {
    type Target = dyn Environment + 'a;

    fn deref(&self) -> &Self::Target {
        self.environment
    }
}

/// The default [`Environment`] for snui.
pub struct ThemeManager;

/// Retreives the last class in the [`List`].
fn last_class<'a>(list: &'a List<Id>) -> Option<&'a str> {
    list.iter().filter_map(|i| i.class()).next()
}

fn accent_color<'a>(class: &'a str) -> Option<Value<'static>> {
    match class {
        "button" => Some(color::BG3.into()),
        "accent-color-1" => Some(Value::from(color::YELLOW)),
        "accent-color-2" => Some(Value::from(color::GREEN)),
        "accent-color-3" => Some(Value::from(color::BLUE)),
        "accent-color-4" => Some(Value::from(color::PURPLE)),
        "accent-color-5" => Some(Value::from(color::BEIGE)),
        "accent-color-6" => Some(Value::from(color::RED)),
        "accent-color-7" | "alert-color" => Some(Value::from(color::RED)),
        _ => None,
    }
}

impl Environment for ThemeManager {
    fn get(&self, list: List<Id>, property: &str) -> Option<Value> {
        match property {
            "foreground" => last_class(&list).and_then(|class| match class {
                "urgent" | "alert" => color::RED.try_into().ok(),
                "discrete" => color::BG1.try_into().ok(),
                _ => None,
            }),
            "font-style" => last_class(&list).map(|class| match class {
                "important" | "title" => "Bold".into(),
                _ => "".into(),
            }),
            "font-family" => Some(Value::from("Cantarell")),
            "font-size" => last_class(&list)
                .map(|class| match class {
                    "title" => 18.,
                    _ => 16.,
                })
                .map(Value::from),
            "highlight" => matches!(list.item(), "slider" | "progress")
                .then(|| list.class().and_then(accent_color))
                .flatten(),
            "ratio" => matches!(list.item(), "handle").then_some(Value::from(4.)),
            "color" | "background" => match list.item() {
                "cursor" => color::FG0.try_into().ok(),
                "header" => color::BG3.try_into().ok(),
                "window" => color::BG0.try_into().ok(),
                "handle" => color::BG4.try_into().ok(),
                "progress" | "slider" => color::BG2.try_into().ok(),
                _ => list.class().and_then(accent_color),
            },
            "border-texture" => color::BG2.try_into().ok(),
            "radius"
            | "top-left-radius"
            | "top-right-radius"
            | "bottom-right-radius"
            | "bottom-left-radius" => match list.item {
                "cursor" => Some(Value::from(0.)),
                "window" => Some(Value::from(10.)),
                "progress" | "slider" => Some(Value::Unit(RelativeUnit::Percent(50.))),
                _ => list
                    .class()
                    .map(|c| matches!(c, "button"))
                    .unwrap_or_default()
                    .then_some(Value::from(5.)),
            },
            "padding" | "padding-top" | "padding-right" | "padding-bottom" | "padding-left" => {
                match list.item {
                    "switch" => Some(Value::from(3.)),
                    "" => Some(Value::from(10.)),
                    _ => list
                        .class()
                        .map(|c| matches!(c, "button"))
                        .unwrap_or_default()
                        .then_some(Value::from(8.)),
                }
            }
            "border-width" => list
                .iter()
                .any(|id| id.item() == "window" || id.class() == Some("button"))
                .then_some(Value::from(1.)),
            "accent-color-1" => Some(Value::from(color::YELLOW)),
            "accent-color-2" => Some(Value::from(color::GREEN)),
            "accent-color-3" => Some(Value::from(color::BLUE)),
            "accent-color-4" => Some(Value::from(color::PURPLE)),
            "accent-color-5" => Some(Value::from(color::BEIGE)),
            "accent-color-6" => Some(Value::from(color::RED)),
            "accent-color-7" | "alert-color" => Some(Value::from(color::RED)),
            _ => None,
        }
    }
}

/// A widget that applies a theme on its child.
///
/// It pushes an [`Id`] with the name of the item and its class inside the [`Env`].
pub struct Stylesheet<T, W: Widget<T> + Themeable> {
    inner: W,
    class: String,
    configured: bool,
    _data: PhantomData<T>,
}

impl<T, W: Widget<T> + Themeable> Stylesheet<T, W> {
    /// Create a new [`Stylesheet`] with a class.
    ///
    /// # Arguments
    ///
    /// * `inner` - The widget that will be styled.
    /// * `class` - The class associated to the styled item.
    pub fn new(inner: W, class: impl Into<String>) -> Self {
        Self {
            inner,
            class: class.into(),
            configured: false,
            _data: PhantomData,
        }
    }
    pub fn set_env(&mut self, env: &Env) {
        self.configured = false;
        env.push(self.inner.id(Some(self.class.as_str())))
            .map(&mut self.inner)
    }
    pub fn set_class(&mut self, class: impl Into<String>) {
        self.configured = false;
        self.class = class.into();
    }
}

impl<T, W: Widget<T> + Themeable> Themeable for Stylesheet<T, W> {
    const NAME: &'static str = W::NAME;

    fn theme(&mut self, list: List<Id>, env: &dyn Environment) {
        self.inner.theme(
            list.push(Id::new(Self::NAME, Some(self.class.as_str()))),
            env,
        );
        self.configured = true;
    }
}

impl<T, W: Widget<T> + Themeable + Style> Style for Stylesheet<T, W> {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_left_radius(radius)
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_right_radius(radius)
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_left_radius(radius)
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_right_radius(radius)
    }
    fn set_texture<B: Into<Texture>>(&mut self, texture: B) {
        self.inner.set_texture(texture)
    }
}

impl<T, W: Widget<T> + Themeable + Guarded> Guarded for Stylesheet<T, W> {
    #[inline]
    fn restore(&mut self) {
        self.inner.restore()
    }
    #[inline]
    fn touched(&self) -> bool {
        self.inner.touched()
    }
}

impl<T, W: Widget<T> + Themeable> Widget<T> for Stylesheet<T, W> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner
            .draw_scene(scene, &env.push(self.inner.id(Some(self.class.as_str()))));
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let env = env.push(self.inner.id(Some(self.class.as_str())));
        if event == STYLESHEET_SIGNAL || !self.configured {
            env.map(&mut self.inner);
            self.configured = true;
        }
        self.inner.event(ctx, event, &env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        let env = env.push(self.inner.id(Some(self.class.as_str())));
        if !self.configured {
            env.map(&mut self.inner);
            self.configured = true;
        }
        self.inner.update(ctx, &env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner
            .layout(ctx, bc, &env.push(self.inner.id(Some(self.class.as_str()))))
    }
}

impl<T, W: Widget<T> + Themeable> Deref for Stylesheet<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W: Widget<T> + Themeable> DerefMut for Stylesheet<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
