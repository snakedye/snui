//! Keyboard related types.

/// Keyboard modifiers.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Modifiers {
    pub ctrl: bool,
    pub alt: bool,
    pub shift: bool,
    pub caps_lock: bool,
    pub logo: bool,
    pub num_lock: bool,
}

impl Default for Modifiers {
    fn default() -> Self {
        Modifiers {
            ctrl: false,
            alt: false,
            shift: false,
            caps_lock: false,
            logo: false,
            num_lock: false,
        }
    }
}

/// A keyboard key.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Key<'k> {
    pub serial: u32,
    pub utf8: Option<&'k String>,
    pub value: &'k [u32],
    pub modifiers: &'k Modifiers,
    pub pressed: bool,
}
