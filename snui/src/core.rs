//! Essential types and utilities.

use crate::widgets::Expander;
use crate::widgets::Filter;
use crate::*;
pub(crate) use environment::{
    color, Environment, EnvironmentExt, Id, Themeable, STYLESHEET_MESSAGE, STYLESHEET_SIGNAL,
};
pub use environment::{Env, RelativeUnit};
use std::ops::Add;
pub use utils::guard::*;
pub use widgets::proxy::*;

/// Vertical or horizontal orientation.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Orientation {
    Vertical,
    Horizontal,
}

/// Converts a [`u32`] into a [`Color`] struct.
pub fn to_color(color: u32) -> Color {
    let color = color.to_le_bytes();
    Color::from_rgba8(color[2], color[1], color[0], color[3])
}

/// The type of damage.
#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub enum Damage {
    #[default]
    /// Nothing needs to be damaged.
    None,
    /// Something needs to be damaged.
    Partial,
    /// Request a new frame.
    ///
    /// If the flag is set to true, it indicates a damage.
    Frame(bool),
}

impl Damage {
    pub fn is_partial(&self) -> bool {
        !matches!(self, Damage::None | Damage::Frame(false))
    }
    pub fn is_none(&self) -> bool {
        matches!(self, Damage::None)
    }
    pub fn is_some(&self) -> bool {
        !self.is_none()
    }
    pub fn upgrade(&self) -> Self {
        (*self).max(Damage::Partial)
    }
}

use std::cmp::Ordering;

impl PartialOrd for Damage {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(match self {
            Self::None => match other {
                Self::None => Ordering::Equal,
                _ => Ordering::Less,
            },
            Self::Partial => match other {
                Self::None => Ordering::Greater,
                Self::Partial => Ordering::Equal,
                _ => Ordering::Less,
            },
            Self::Frame(a) => match other {
                Self::Frame(b) => {
                    if a == b {
                        Ordering::Equal
                    } else if *a {
                        Ordering::Greater
                    } else {
                        Ordering::Less
                    }
                }
                Self::Partial => {
                    if *a {
                        Ordering::Greater
                    } else {
                        Ordering::Less
                    }
                }
                Self::None => Ordering::Greater,
            },
        })
    }
}

impl Eq for Damage {}

impl Ord for Damage {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
    fn max(self, other: Self) -> Self
    where
        Self: Sized,
    {
        if matches!(self, Self::Frame(false)) && matches!(other, Self::Partial) {
            Self::Frame(true)
        } else if self.lt(&other) {
            other
        } else {
            self
        }
    }
}

/// 2D dimensions.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Size<T = f32> {
    pub width: T,
    pub height: T,
}

impl<'a, T: Clone> From<Size<T>> for std::borrow::Cow<'a, Size<T>> {
    fn from(size: Size<T>) -> Self {
        std::borrow::Cow::Owned(size)
    }
}

impl<'a, T: Clone> From<&'a Size<T>> for std::borrow::Cow<'a, Size<T>> {
    fn from(size: &'a Size<T>) -> Self {
        std::borrow::Cow::Borrowed(size)
    }
}

impl<T: PartialEq + PartialOrd> PartialOrd for Size<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.width < other.width && self.height < other.height {
            Some(Ordering::Greater)
        } else if self.width == other.width && self.height == other.height {
            Some(Ordering::Equal)
        } else if self.width < other.width && self.height < other.height {
            Some(Ordering::Less)
        } else {
            None
        }
    }
}

impl<T: Add<Output = T>> Add for Size<T> {
    type Output = Size<T>;
    fn add(self, rhs: Self) -> Self::Output {
        Size::new(self.width + rhs.width, self.height + rhs.height)
    }
}

impl Geometry for Size {
    fn width(&self) -> f32 {
        self.width
    }
    fn height(&self) -> f32 {
        self.height
    }
}

impl<T> Size<T> {
    pub fn new(width: T, height: T) -> Self {
        Size { width, height }
    }
}

impl Size {
    pub fn round(&self) -> Self {
        Size::new(self.width.round(), self.height.round())
    }
    pub fn ceil(&self) -> Self {
        Size::new(self.width.ceil(), self.height.ceil())
    }
    pub fn floor(&self) -> Self {
        Size::new(self.width.floor(), self.height.floor())
    }
    pub fn max(self, other: Self) -> Self {
        Size::new(self.width.max(other.width), self.height.max(other.height))
    }
    pub fn min(self, other: Self) -> Self {
        Size::new(self.width.min(other.width), self.height.min(other.height))
    }
}

impl From<(f32, f32)> for Size {
    fn from((width, height): (f32, f32)) -> Self {
        Size { width, height }
    }
}

impl From<Size> for (f32, f32) {
    fn from(Size { width, height }: Size) -> Self {
        (width, height)
    }
}

impl<T: Default> Default for Size<T> {
    fn default() -> Self {
        Size {
            width: T::default(),
            height: T::default(),
        }
    }
}

/// Constraints for widget layout.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct BoxConstraints {
    minimum: Size,
    maximum: Size,
}

impl Default for BoxConstraints {
    fn default() -> Self {
        Self {
            minimum: Default::default(),
            maximum: Default::default(),
        }
    }
}

impl BoxConstraints {
    /// Create a new box constraints.
    pub fn new<S: Into<Size>>(minimum: S, maximum: S) -> Self {
        BoxConstraints {
            minimum: minimum.into(),
            maximum: maximum.into(),
        }
    }
    pub fn infinite() -> Self {
        BoxConstraints {
            minimum: (0., 0.).into(),
            maximum: (std::f32::INFINITY, std::f32::INFINITY).into(),
        }
    }
    /// Create a new version of the constraint with the minimum size being zero.
    pub fn loosen(&self) -> Self {
        BoxConstraints {
            minimum: Size::default(),
            maximum: self.maximum,
        }
    }
    pub fn stretch(&self, width: f32, height: f32) -> Self {
        BoxConstraints {
            minimum: self.minimum,
            maximum: Size::new(
                width.max(self.minimum_width()),
                height.max(self.minimum_height()),
            ),
        }
    }
    /// Creates a new box constraints with the given maximum dimensions.
    pub fn with_max(&self, mut width: f32, mut height: f32) -> Self {
        width = width.max(0.);
        height = height.max(0.);
        BoxConstraints {
            minimum: Size::new(
                width.min(self.minimum_width()),
                height.min(self.minimum_height()),
            ),
            maximum: Size::new(width, height),
        }
    }
    /// Creates a new box constraints with the given minimum dimensions.
    pub fn with_min(&self, width: f32, height: f32) -> Self {
        let width = width.round().max(0.);
        let height = height.round().max(0.);
        BoxConstraints {
            minimum: Size::new(width, height),
            maximum: Size::new(
                width.max(self.maximum_width()),
                height.max(self.maximum_height()),
            ),
        }
    }
    pub fn crop(&self, dx: f32, dy: f32) -> Self {
        let width = (self.maximum_width() - dx).max(0.).round();
        let height = (self.maximum_height() - dy).max(0.).round();
        BoxConstraints {
            minimum: Size::new(
                self.minimum_width().min(width),
                self.minimum_height().min(height),
            ),
            maximum: Size::new(width, height),
        }
    }
    pub fn minimum_width(&self) -> f32 {
        self.minimum.width
    }
    pub fn minimum_height(&self) -> f32 {
        self.minimum.height
    }
    pub fn maximum_width(&self) -> f32 {
        self.maximum.width
    }
    pub fn maximum_height(&self) -> f32 {
        self.maximum.height
    }
    pub fn maximum(&self) -> Size {
        self.maximum
    }
    pub fn minimum(&self) -> Size {
        self.minimum
    }
    pub fn contains(&self, size: &Size) -> bool {
        &self.minimum < size && size < &self.maximum
    }
}

/// The state of the window.
///
/// Can be retreived by calling `get_state` on [`WindowHandle`].
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum WindowState {
    Maximized,
    Resizing,
    Fullscreen,
    /// Client window decorations should be painted as if the window is active.
    Activated,
    TiledLeft,
    TiledRight,
    TiledBottom,
    TiledTop,
}

use scene::Texture;
use widgets::Padding;
use widgets::Style;
use widgets::Stylesheet;

/// Additional method to help build common widgets.
pub trait WidgetExt<T>: Widget<T> + Sized {
    /// Wrap this [`Themeable`] widget inside a [`Stylesheet`].
    fn class(self, class: impl Into<String>) -> Stylesheet<T, Self>
    where
        Self: Themeable,
    {
        Stylesheet::new(self, class)
    }
    /// Wrap this widget inside a [`WidgetStyle`] and a [`Stylesheet`].
    fn style(self, class: impl Into<String>) -> Stylesheet<T, WidgetStyle<T, Self>> {
        Stylesheet::new(WidgetStyle::new(self), class)
    }
    /// Wrap this widget in a [`WidgetBox`].
    fn clamp(self) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
    }
    /// Wrap this widget inside a [`WidgetStyle`] with a background.
    fn background(self, background: impl Into<Texture>) -> WidgetStyle<T, Self> {
        Style::texture(WidgetStyle::new(self), background)
    }
    /// Wrap this widget inside a [`WidgetStyle`] with borders.
    fn border(self, texture: impl Into<Texture>, width: f32) -> WidgetStyle<T, Self> {
        WidgetStyle::new(self).border(texture, width)
    }
    /// Add padding around this widget.
    fn padding(self, padding: f32) -> Padding<T, Self> {
        Padding::new(self).padding(padding)
    }
    /// Add padding at the top of this widget.
    fn padding_top(self, padding: f32) -> Padding<T, Self> {
        Padding::new(self).padding_top(padding)
    }
    /// Add padding at the right of this widget.
    fn padding_right(self, padding: f32) -> Padding<T, Self> {
        Padding::new(self).padding_right(padding)
    }
    /// Add padding beneath this widget.
    fn padding_bottom(self, padding: f32) -> Padding<T, Self> {
        Padding::new(self).padding_bottom(padding)
    }
    /// Add padding at the left of this widget.
    fn padding_left(self, padding: f32) -> Padding<T, Self> {
        Padding::new(self).padding_left(padding)
    }
    /// Wrap this widget in [`WidgetBox`] with a minimum width.
    fn with_min_width(self, width: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Downward)
            .with_width(width)
    }
    /// Wrap this widget in [`WidgetBox`] with a minimum height.
    fn with_min_height(self, height: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Downward)
            .with_height(height)
    }
    /// Wrap this widget in [`WidgetBox`] with a minimum size.
    fn with_min_size(self, width: f32, height: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Downward)
            .with_size(width, height)
    }
    /// Wrap this widget in [`WidgetBox`] with a maximum width.
    fn with_max_width(self, width: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Upward)
            .with_width(width)
    }
    /// Wrap this widget in [`WidgetBox`] with a maximum height.
    fn with_max_height(self, height: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Upward)
            .with_height(height)
    }
    /// Wrap this widget in [`WidgetBox`] with a maximum size.
    fn with_max_size(self, width: f32, height: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Upward)
            .with_size(width, height)
    }
    /// Wrap this widget in [`WidgetBox`] with a fixed width.
    fn with_fixed_width(self, width: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Fixed)
            .with_width(width)
    }
    /// Wrap this widget in [`WidgetBox`] with a fixed height.
    fn with_fixed_height(self, height: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Fixed)
            .with_height(height)
    }
    /// Wrap this widget in [`WidgetBox`] with a fixed size.
    fn with_fixed_size(self, width: f32, height: f32) -> WidgetBox<T, Self> {
        WidgetBox::new(self)
            .constraint(widgets::Constraint::Fixed)
            .with_size(width, height)
    }
    /// Wrap this widget in an [`Activate`] widget.
    fn activate<M, F>(self, message: M, cb: F) -> Activate<T, Self, M, F>
    where
        Self: Widget<T>,
        F: for<'a> FnMut(&'a mut Self, bool, &'a mut UpdateContext<T>, &Env),
    {
        Activate::new(self, message, cb)
    }
    fn filter<F>(self, cb: F) -> Filter<T, Self, F>
    where
        Self: Widget<T>,
        F: for<'a> FnMut(&'a mut Self, &'a mut UpdateContext<T>, &Env),
    {
        Filter::new(self, cb)
    }
    // TO-DO
    // This could be a macro
    /// Wrap this widget in an [`Button`] widget.
    fn button<F>(self, cb: F) -> Proxy<T, Button<T, Self, F>>
    where
        Self: Widget<T>,
        F: for<'a> FnMut(&'a mut Self, &'a mut UpdateContext<T>, &Env, Pointer),
    {
        Button::new(self, cb)
    }
    fn flex(self, orientation: Orientation) -> Expander<T, Self> {
        Expander::horizontal(self).orientation(orientation)
    }
}

impl<'a, W, T> Widget<T> for &'a mut W
where
    W: Widget<T>,
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        W::draw_scene(self, scene, env);
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        W::event(self, ctx, event, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        W::layout(self, ctx, bc, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        W::update(self, ctx, env)
    }
}

/// Additional methods to configure the geometry of some types.
pub trait GeometryExt: Sized {
    /// Set the width of the item.
    fn set_width(&mut self, width: f32);
    /// Set the height of the item.
    fn set_height(&mut self, height: f32);
    /// Set the size of the item.
    fn set_size(&mut self, width: f32, height: f32) {
        self.set_width(width);
        self.set_height(height);
    }
    /// Set the width of the item.
    fn with_width(mut self, width: f32) -> Self {
        self.set_width(width);
        self
    }
    /// Set the height of the item.
    fn with_height(mut self, height: f32) -> Self {
        self.set_height(height);
        self
    }
    /// Set the size of the item.
    fn with_size(mut self, width: f32, height: f32) -> Self {
        self.set_size(width, height);
        self
    }
}

impl<T, W> WidgetExt<T> for W where W: Widget<T> {}

impl<T> Widget<T> for Box<dyn Widget<T>> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.deref_mut().draw_scene(scene, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.deref_mut().update(ctx, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.deref_mut().event(ctx, event, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.deref_mut().layout(ctx, bc, env)
    }
}

impl<T> Themeable for Box<T>
where
    T: Themeable,
{
    const NAME: &'static str = T::NAME;
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        self.deref_mut().theme(list, env)
    }
}
