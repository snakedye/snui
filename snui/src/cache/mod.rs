//! Cached ressources.

pub mod font;
pub mod source;

use self::source::*;
use font::FontCache;

/// Non owning cache.
pub struct Cache<'a> {
    pub font_cache: &'a mut FontCache,
    pub source_cache: &'a mut SourceCache,
}

impl<'a> Cache<'a> {
    pub fn new(font_cache: &'a mut FontCache, source_cache: &'a mut SourceCache) -> Self {
        Cache {
            font_cache,
            source_cache,
        }
    }
    pub fn borrow<'b>(&'b mut self) -> Cache<'b>
    where
        'a: 'b,
    {
        Cache::new(self.font_cache, self.source_cache)
    }
    pub fn font_cache(&mut self) -> &mut FontCache {
        self.font_cache
    }
    pub fn source_cache(&mut self) -> &mut SourceCache {
        self.source_cache
    }
}

/// Get a [`Source`] from a cache.
pub fn get_source<P>(cache: &mut Cache, path: P) -> Option<Source>
where
    P: AsRef<std::path::Path>,
{
    cache.source_cache.get(path).ok()
}
