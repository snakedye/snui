//! Graphical ressources.

#[cfg(feature = "img")]
use crate::Geometry;
#[cfg(feature = "img")]
use image::io::Reader as ImageReader;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
#[cfg(any(feature = "img", feature = "svg"))]
use std::{borrow::Borrow, sync::Arc};
#[cfg(feature = "img")]
use tiny_skia::PixmapRef;
#[cfg(feature = "svg")]
use usvg::{Options, Tree};

#[cfg(feature = "img")]
/// A raw pixel buffer.
#[derive(Clone, PartialEq, Debug)]
pub struct RawImage {
    image: Arc<[u8]>,
    width: u32,
    height: u32,
}

#[derive(Clone)]
pub enum Source {
    #[cfg(feature = "svg")]
    Vector(Arc<Tree>),
    #[cfg(feature = "img")]
    Pixmap(RawImage),
}

pub struct SourceCache {
    cache: HashMap<PathBuf, Source>,
}

impl Default for SourceCache {
    fn default() -> Self {
        Self {
            cache: HashMap::new(),
        }
    }
}

#[cfg(feature = "img")]
impl RawImage {
    pub fn from_raw(buf: Vec<u8>, width: u32, height: u32) -> Option<Self> {
        let image: Arc<[u8]> = buf.into();
        if width * height * 4 == image.len() as u32 {
            Some(Self {
                image,
                width,
                height,
            })
        } else {
            None
        }
    }
    pub fn pixmap(&self) -> PixmapRef {
        PixmapRef::from_bytes(self.image.as_ref(), self.width as u32, self.height as u32).unwrap()
    }
}

#[cfg(feature = "img")]
impl Geometry for RawImage {
    fn width(&self) -> f32 {
        self.width as f32
    }
    fn height(&self) -> f32 {
        self.height as f32
    }
}

#[cfg(feature = "img")]
impl AsRef<[u8]> for RawImage {
    fn as_ref(&self) -> &[u8] {
        self.image.as_ref()
    }
}

impl SourceCache {
    pub fn try_get<P>(&self, path: P) -> Option<&Source>
    where
        P: AsRef<Path>,
    {
        self.cache.get(path.as_ref())
    }
    /// Clears the cache.
    pub fn clear(&mut self) {
        self.cache.clear();
    }
    /// Tries to retreive the image from the cache.
    /// If it fails it will attempt to load it from the given path.
    pub fn get<P>(&mut self, path: P) -> Result<Source, Box<dyn std::error::Error>>
    where
        P: AsRef<Path>,
    {
        match self.cache.get(path.as_ref()) {
            Some(shader) => Ok(shader.clone()),
            None => {
                #[cfg(feature = "img")]
                if let Ok(dyn_image) = ImageReader::open(path.borrow())?.decode() {
                    let dyn_image = dyn_image.to_rgba8();

                    let (width, height) = dyn_image.dimensions();
                    let image: Arc<[u8]> = dyn_image.into_raw().into();

                    let raw = RawImage {
                        image,
                        width,
                        height,
                    };

                    self.cache
                        .insert(path.as_ref().to_path_buf(), Source::Pixmap(raw.clone()));

                    return Ok(Source::Pixmap(raw));
                }
                #[cfg(feature = "svg")]
                if let Ok(data) = std::fs::read(path.borrow()) {
                    let vec = Tree::from_data(&data, &Options::default().to_ref())?;

                    let svg = Source::Vector(Arc::new(vec));

                    self.cache.insert(path.as_ref().to_path_buf(), svg.clone());

                    return Ok(svg);
                }

                Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "enable \"img\" or \"svg\" features.",
                )))
            }
        }
    }
}
