//! Font related types and traits for the backend.

use crate::widgets::LabelRef;
#[cfg(feature = "linux")]
use fontconfig::*;
use fontdue::{
    layout::{GlyphPosition, GlyphRasterConfig, Layout, LayoutSettings, TextStyle},
    Font, FontResult,
};
use std::collections::HashMap;
use std::fs::read;
use std::ops::{Index, IndexMut};
use std::path::Path;
use tiny_skia::Color;

/// Returns the size of a text layout.
pub(crate) fn get_size<U: Copy + Clone>(glyphs: &[GlyphPosition<U>]) -> (f32, f32) {
    glyphs
        .iter()
        .map(|gp| (gp.width as f32 + gp.x, gp.height as f32 + gp.y))
        .reduce(|(acc_w, acc_h), (w, h)| (acc_w.max(w), acc_h.max(h)))
        .unwrap_or_default()
}

/// Unicode character range.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum CharacterRange {
    Latin,
    Greek,
    Cyrillic,
    Hebrew,
    Arabic,
    Cjk,
    Emoji,
    Surrogate,
}

impl Default for CharacterRange {
    fn default() -> Self {
        Self::Latin
    }
}

impl From<char> for FontPattern<'_> {
    fn from(c: char) -> Self {
        let pattern = FontPattern::default();
        if matches!(c, '぀'..='ヿ' | 'ㇰ'..='鿿') {
            pattern.lang(CharacterRange::Cjk)
        } else if matches!(c, ''..='') {
            pattern.family("FontAwesome")
        } else if matches!(c, '⌚'..='🧦') {
            pattern.lang(CharacterRange::Emoji)
        } else if matches!(c, 'Ͱ'..='Ͽ' | 'ἀ'..='῿') {
            pattern.lang(CharacterRange::Greek)
        } else if matches!(c, 'Ѐ'..='ӿ') {
            pattern.lang(CharacterRange::Cyrillic)
        } else if matches!(c, '֐'..='׿') {
            pattern.lang(CharacterRange::Hebrew)
        } else if matches!(c, '�'..='�') {
            pattern.lang(CharacterRange::Surrogate)
        } else if matches!(c, '؀'..='ۿ' | 'ﭐ'..='﷿' | 'ﹰ'..='﻾') {
            pattern.lang(CharacterRange::Arabic)
        } else {
            pattern.lang(CharacterRange::Latin)
        }
    }
}

impl CharacterRange {
    fn as_str(&self) -> &[&str] {
        match self {
            Self::Latin => &[""],
            Self::Arabic => &["ar"],
            Self::Greek => &["el"],
            Self::Hebrew => &["he"],
            Self::Emoji => &["und-zsye"],
            Self::Cjk => &["ja", "ko", "zh-cn"],
            Self::Surrogate => &["ja", "zh-cn"],
            Self::Cyrillic => &["ru", "ba", "sl", "bg", "mk", "ky"],
        }
    }
}

/// The font style of a text.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Default)]
pub enum FontStyle {
    #[default]
    Regular,
    Italic,
    Bold,
    Light,
    BoldItalic,
    LightItalic,
}

impl From<&'_ str> for FontStyle {
    fn from(style: &'_ str) -> Self {
        match style {
            "Bold" | "bold" => Self::Bold,
            "Light" | "light" => Self::Light,
            "Italic" | "italic" => Self::Italic,
            "BoldItalic" | "bold-italic" | "bold italic" => Self::BoldItalic,
            "LightItalic" | "light-italic" | "light italic" => Self::LightItalic,
            _ => Self::Regular,
        }
    }
}

impl FontStyle {
    fn as_str(&self) -> Option<&str> {
        match self {
            Self::Regular => None,
            Self::Italic => Some("Italic"),
            Self::BoldItalic => Some("Bold Italic"),
            Self::LightItalic => Some("Light Italic"),
            Self::Bold => Some("Bold"),
            Self::Light => Some("Light"),
        }
    }
}

/// Properties of a font.
///
/// It's the key of the [`HashMap`] in the [`FontCache`].
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct FontProperty {
    pub family: String,
    pub style: FontStyle,
}

impl Default for FontProperty {
    fn default() -> Self {
        FontProperty::from("")
    }
}

impl<T> From<T> for FontProperty
where
    T: Into<String>,
{
    fn from(family: T) -> Self {
        FontProperty {
            family: family.into(),
            style: FontStyle::Regular,
        }
    }
}

impl From<FontHeader> for FontProperty {
    fn from(fh: FontHeader) -> Self {
        Self {
            family: fh.name,
            style: fh.style,
        }
    }
}

use std::ffi::CString;
use std::path::PathBuf;

/// A pattern used to identify fonts.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct FontPattern<'f> {
    family: Option<&'f str>,
    style: FontStyle,
    language: Option<CharacterRange>,
}

impl Default for FontPattern<'_> {
    fn default() -> Self {
        Self {
            family: None,
            style: FontStyle::Regular,
            language: None,
        }
    }
}

impl<'f> From<&'f FontProperty> for FontPattern<'f> {
    fn from(prop: &'f FontProperty) -> Self {
        FontPattern {
            family: Some(prop.family.as_str()),
            style: prop.style,
            language: None,
        }
    }
}

impl<'f> FontPattern<'f> {
    pub fn anonymous(mut self) -> Self {
        self.family = None;
        self
    }
    pub fn family(mut self, family: &'f str) -> Self {
        self.family = Some(family);
        self
    }
    pub fn style(mut self, style: FontStyle) -> Self {
        self.style = style;
        self
    }
    pub fn lang(mut self, lang: CharacterRange) -> Self {
        self.language = Some(lang);
        self
    }
}

/// A type with high level information about a font and its location.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct FontHeader {
    name: String,
    path: PathBuf,
    style: FontStyle,
}

/// A type that can provide fonts to the [`FontCache`].
pub trait FontProvider {
    /// Retreives a font matching the given [`FontProperty`].
    fn load_font(&self, font_property: &FontProperty) -> Option<(FontHeader, GlyphCache)>;
    fn find(&self, pattern: &FontPattern) -> Option<FontHeader>;
}

#[cfg(feature = "linux")]
impl FontProvider for Fontconfig {
    fn load_font(&self, font_property: &FontProperty) -> Option<(FontHeader, GlyphCache)> {
        self.find(&font_property.family, font_property.style.as_str())
            .and_then(|fc_font| {
                eprintln!("info: fontconfig: {:?}, {:?}", fc_font.name, fc_font.path);
                let gc = GlyphCache::load(fc_font.path.as_path());
                let fh = FontHeader {
                    name: fc_font.name,
                    path: fc_font.path,
                    style: font_property.style,
                };
                Some(fh).zip(gc.ok())
            })
    }
    fn find(&self, font_pattern: &FontPattern) -> Option<FontHeader> {
        let mut fc = Pattern::new(self);
        if let Some(family) = font_pattern.family {
            fc.add_string(
                FC_FAMILY.as_cstr(),
                CString::new(family).unwrap().as_c_str(),
            )
        }
        if let Some(langs) = font_pattern.language {
            for lang in langs.as_str() {
                fc.add_string(FC_LANG.as_cstr(), CString::new(*lang).unwrap().as_c_str())
            }
        }
        if let Some(style) = font_pattern.style.as_str() {
            fc.add_string(FC_STYLE.as_cstr(), CString::new(style).unwrap().as_c_str());
        }
        list_fonts(&fc, None).iter().next().map(|pattern| {
            eprintln!(
                "info: fontconfig: {:?}, {:?}",
                pattern.name().unwrap_or_default(),
                pattern.filename().unwrap_or_default()
            );
            FontHeader {
                style: font_pattern.style,
                name: String::from(pattern.name().unwrap_or_default()),
                path: PathBuf::from(pattern.filename().unwrap_or_default()),
            }
        })
    }
}

pub(crate) struct FontMap {
    fonts: Vec<GlyphCache>,
    cache: HashMap<FontProperty, usize>,
}

impl AsRef<[GlyphCache]> for FontMap {
    fn as_ref(&self) -> &[GlyphCache] {
        self.fonts.as_slice()
    }
}

impl Index<usize> for FontMap {
    type Output = GlyphCache;

    fn index(&self, index: usize) -> &Self::Output {
        &self.fonts[index]
    }
}

impl IndexMut<usize> for FontMap {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.fonts[index]
    }
}

impl FontMap {
    fn new() -> Self {
        FontMap {
            fonts: Vec::new(),
            cache: HashMap::new(),
        }
    }
    fn get(&self, property: &FontProperty) -> Option<&Font> {
        self.cache
            .get(property)
            .and_then(|i| self.fonts.get(*i))
            .map(|gc| &gc.font)
    }
    fn insert(&mut self, property: FontProperty, glyphs: GlyphCache) -> Option<&mut Font> {
        self.fonts.push(glyphs);
        self.cache
            .insert(property, self.fonts.len() - 1)
            .and_then(|i| self.fonts.get_mut(i))
            .map(|gc| &mut gc.font)
    }
}

/// A font cache contains all the glyphs and fonts needed by an application.
pub struct FontCache {
    pub(crate) fc: Box<dyn FontProvider>,
    pub(crate) map: FontMap,
}

impl<T> From<T> for FontCache
where
    T: FontProvider + 'static,
{
    fn from(provider: T) -> Self {
        FontCache {
            map: FontMap::new(),
            fc: Box::new(provider),
        }
    }
}

#[cfg(feature = "linux")]
impl Default for FontCache {
    fn default() -> Self {
        Self::from(Fontconfig::new().expect("Failed to initialize fontconfig!"))
    }
}

fn load_font(map: &mut FontMap, fc: &dyn FontProvider, prop: &FontProperty) {
    if map.get(prop).is_none() {
        // Try to use the FontHeader eventually
        if let Some((_, gc)) = fc.load_font(prop) {
            map.insert(prop.clone(), gc);
        }
    }
}

fn find(map: &mut FontMap, fc: &dyn FontProvider, pattern: &FontPattern) -> Option<FontProperty> {
    fc.find(pattern).map(|header| {
        let prop = FontProperty {
            family: header.name,
            style: header.style,
        };
        if map.get(&prop).is_none() {
            map.insert(
                prop.clone(),
                GlyphCache::load(header.path.as_path()).unwrap(),
            );
        }
        prop
    })
}

fn layout_into(
    map: &mut FontMap,
    fc: &dyn FontProvider,
    layout: &mut Layout<Color>,
    label: &LabelRef,
) {
    let mut tmp = [0u8; 4];
    let mut font_index = map.cache.get(label.font).copied().unwrap_or_default();
    load_font(map, fc, label.font);
    layout.reset(label.settings);
    for c in label.text.chars() {
        if map[font_index].font.lookup_glyph_index(c) < 1 {
            font_index = map
                .as_ref()
                .iter()
                .map(|gc| &gc.font)
                .enumerate()
                .find(|(i, f)| *i != font_index && f.lookup_glyph_index(c) > 0)
                .map(|(font_index, _)| font_index)
                .unwrap_or_else(|| {
                    find(map, fc, &FontPattern::from(c))
                        .and_then(|prop| map.cache.get(&prop))
                        .copied()
                        .unwrap_or(font_index)
                })
        }
        layout.append(
            map.as_ref(),
            &TextStyle::with_user_data(
                c.encode_utf8(&mut tmp),
                label.font_size,
                font_index,
                label.color,
            ),
        );
    }
}

fn write_into<'a>(
    map: &mut FontMap,
    fc: &dyn FontProvider,
    layout: &mut Layout<Color>,
    iter: impl Iterator<Item = LabelRef<'a>>,
    settings: &LayoutSettings,
) {
    layout.reset(settings);
    for label in iter {
        load_font(map, fc, label.font);
        if let Some(font_index) = map.cache.get(label.font) {
            layout.append(
                map.as_ref(),
                &TextStyle::with_user_data(label.text, label.font_size, *font_index, label.color),
            );
        }
    }
}

impl FontCache {
    pub fn write_into<'a>(
        &mut self,
        layout: &mut Layout<Color>,
        iter: impl Iterator<Item = LabelRef<'a>>,
        settings: &LayoutSettings,
    ) {
        write_into(&mut self.map, self.fc.as_ref(), layout, iter, settings)
    }
    pub fn layout_into(&mut self, layout: &mut Layout<Color>, label: &LabelRef) -> crate::Size {
        layout_into(&mut self.map, self.fc.as_ref(), layout, label);
        get_size(layout.glyphs()).into()
    }
}

#[derive(Debug, Clone)]
// A collection of rasterized glyphs associated with a font.
pub struct GlyphCache {
    pub font: fontdue::Font,
    glyphs: HashMap<GlyphKey, Vec<u32>>,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct GlyphKey {
    color: u32,
    config: GlyphRasterConfig,
}

impl From<&'_ GlyphPosition<Color>> for GlyphKey {
    fn from(gp: &'_ GlyphPosition<Color>) -> Self {
        GlyphKey {
            color: gp.user_data.to_color_u8().get(),
            config: gp.key,
        }
    }
}

use std::borrow::Borrow;

impl Borrow<fontdue::Font> for GlyphCache {
    fn borrow(&self) -> &fontdue::Font {
        &self.font
    }
}

impl GlyphCache {
    pub fn new(font: fontdue::Font) -> Self {
        Self {
            font,
            glyphs: HashMap::new(),
        }
    }
    pub fn load(path: &Path) -> FontResult<Self> {
        match read(path) {
            Ok(bytes) => match fontdue::Font::from_bytes(bytes, fontdue::FontSettings::default()) {
                Ok(font) => Ok(Self {
                    font,
                    glyphs: HashMap::new(),
                }),
                Err(_) => FontResult::Err("Unable to find font."),
            },
            Err(_) => FontResult::Err("Invalid path."),
        }
    }
    pub fn get(&mut self, glyph: &GlyphPosition<Color>) -> Option<&[u8]> {
        (!glyph.char_data.is_missing()).then(|| {
            let pixmap = self.glyphs.entry(glyph.into()).or_insert_with(|| {
                let (_, coverage) = self.font.rasterize_config(glyph.key);
                let pixmap: Vec<u32> = coverage
                    .into_iter()
                    .map(|a| {
                        if a == 0 {
                            0
                        } else {
                            let mut color = glyph.user_data;
                            color.apply_opacity(a as f32 / 255.);
                            color.premultiply().to_color_u8().get()
                        }
                    })
                    .collect();
                pixmap
            });
            unsafe {
                std::slice::from_raw_parts(
                    pixmap.as_ptr() as *mut u8,
                    pixmap.len() * std::mem::size_of::<u32>(),
                )
            }
        })
    }
}
