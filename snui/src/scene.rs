//!	Visual presentation of an application.

use crate::utils::List;
use crate::*;
use cache::Cache;
use context::DrawContext;
use tiny_skia::*;

#[cfg(feature = "img")]
use cache::source::RawImage as Image;
use widgets::Rectangle;

use std::cell::RefCell;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::num::ParseIntError;
use std::ops::{Add, Sub};
use std::rc::Rc;
use std::str::FromStr;

/// Surface coordinates.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Position<T = f32> {
    pub x: T,
    pub y: T,
}

pub type Vec2D = Position;

impl Default for Position {
    fn default() -> Self {
        Position { x: 0., y: 0. }
    }
}

impl From<(f32, f32)> for Position {
    fn from(position: (f32, f32)) -> Self {
        Position {
            x: position.0,
            y: position.1,
        }
    }
}

impl From<&Position> for Point {
    fn from(position: &Position) -> Self {
        Point::from_xy(position.x, position.y)
    }
}

impl From<Position> for Point {
    fn from(position: Position) -> Self {
        Point::from_xy(position.x, position.y)
    }
}

impl<T: Add<Output = T>> Position<T> {
    pub fn new(x: T, y: T) -> Self {
        Position { x, y }
    }
    pub fn translate(self, x: T, y: T) -> Self {
        Self {
            x: self.x + x,
            y: self.y + y,
        }
    }
}

impl Position {
    pub fn to_region(&self, g: &impl Geometry) -> Region {
        Region::new(self.x, self.y, g.width(), g.height())
    }
}

impl<T: Add<Output = T>> Add for Position<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        self.translate(rhs.x, rhs.y)
    }
}

impl<T: Sub<Output = T>> Sub for Position<T> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

/// A linear gradient texture.
#[derive(Debug, Clone, PartialEq)]
pub struct LinearGradient {
    pub orientation: Orientation,
    pub stops: Rc<[GradientStop]>,
    pub mode: SpreadMode,
}

/// A linear gradient texture.
#[derive(Debug, Clone, PartialEq)]
pub struct RadialGradient {
    pub mode: SpreadMode,
    pub stops: Rc<[GradientStop]>,
}

impl From<RadialGradient> for Texture {
    fn from(grad: RadialGradient) -> Self {
        Texture::RadialGradient(grad)
    }
}

impl LinearGradient {
    pub fn new(stops: Vec<GradientStop>) -> LinearGradient {
        LinearGradient {
            stops: stops.into(),
            mode: SpreadMode::Reflect,
            orientation: Orientation::Horizontal,
        }
    }
    pub fn mode(mut self, mode: SpreadMode) -> Self {
        self.mode = mode;
        self
    }
    pub fn orientation(mut self, orientation: Orientation) -> Self {
        self.orientation = orientation;
        self
    }
}

impl RadialGradient {
    pub fn new(stops: Vec<GradientStop>, mode: SpreadMode) -> RadialGradient {
        RadialGradient {
            stops: stops.into(),
            mode,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Texture {
    Transparent,
    #[cfg(feature = "img")]
    Image(Image),
    Color(Color),
    LinearGradient(LinearGradient),
    RadialGradient(RadialGradient),
}

impl Texture {
    pub fn is_transparent(&self) -> bool {
        match &self {
            Texture::Color(color) => !color.is_opaque(),
            Texture::Transparent => true,
            _ => false,
        }
    }
    pub fn is_opaque(&self) -> bool {
        !self.is_transparent()
    }
}

impl Default for Texture {
    fn default() -> Self {
        Self::Transparent
    }
}

impl From<u32> for Texture {
    fn from(color: u32) -> Self {
        Texture::Color(to_color(color))
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ParseTextureError {
    Color(ParseIntError),
    InvalidToken(String),
}

impl FromStr for Texture {
    type Err = ParseTextureError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with('#') {
            s.get(1..)
                .ok_or_else(|| ParseTextureError::InvalidToken(s.to_owned()))
                .and_then(|s| {
                    u32::from_str_radix(s, 16)
                        .map_err(ParseTextureError::Color)
                        .map(|uint| to_color(uint).into())
                })
        } else {
            Err(ParseTextureError::InvalidToken(s.to_owned()))
        }
    }
}

impl From<(u8, u8, u8, u8)> for Texture {
    fn from(rgba: (u8, u8, u8, u8)) -> Self {
        let (r, g, b, a) = rgba;
        Texture::Color(Color::from_rgba8(r, g, b, a))
    }
}

impl From<(u8, u8, u8, f32)> for Texture {
    fn from(rgba: (u8, u8, u8, f32)) -> Self {
        let (r, g, b, a) = rgba;
        Texture::Color(Color::from_rgba8(r, g, b, (a * 255.) as u8))
    }
}

impl From<Color> for Texture {
    fn from(color: Color) -> Self {
        Texture::Color(color)
    }
}

impl From<ColorU8> for Texture {
    fn from(color: ColorU8) -> Self {
        color.get().into()
    }
}

impl From<LinearGradient> for Texture {
    fn from(gradient: LinearGradient) -> Self {
        Self::LinearGradient(gradient)
    }
}

#[cfg(feature = "img")]
impl From<Image> for Texture {
    fn from(raw: Image) -> Self {
        Texture::Image(raw)
    }
}

/// A rectangular region.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Region {
    pub x: f32,
    pub y: f32,
    pub width: f32,
    pub height: f32,
}

impl From<Region> for Position {
    fn from(r: Region) -> Self {
        Position::new(r.x, r.y)
    }
}

impl From<&Region> for Rect {
    fn from(r: &Region) -> Self {
        Rect::from_xywh(r.x, r.y, r.width, r.height).unwrap()
    }
}

impl From<Region> for Rect {
    fn from(r: Region) -> Self {
        Rect::from_xywh(r.x, r.y, r.width, r.height).unwrap()
    }
}

impl Default for Region {
    fn default() -> Self {
        Region::new(0., 0., 0., 0.)
    }
}

impl Geometry for Region {
    fn width(&self) -> f32 {
        self.width
    }
    fn height(&self) -> f32 {
        self.height
    }
}

impl Region {
    pub fn new(x: f32, y: f32, width: f32, height: f32) -> Region {
        Region {
            x,
            y,
            width: width.max(0.),
            height: height.max(0.),
        }
    }
    pub fn from_position(start: &Position, end: &Position) -> Self {
        let x = start.x.min(end.x);
        let y = start.y.min(end.y);
        Region {
            x,
            y,
            width: start.x.max(end.x) - x,
            height: start.y.max(end.y) - y,
        }
    }
    /// Returns the region other instersect Self
    pub fn crop(&self, other: &Self) -> Region {
        let x = self.x.max(other.x);
        let y = self.y.max(other.y);
        let width = (self.x + self.width).min(other.x + other.width) - x;
        let height = (self.y + self.height).min(other.y + other.height) - y;
        Region::new(x, y, width, height)
    }
    /// Returns true if two regions intersect each others.
    pub fn intersect(&self, other: &Self) -> bool {
        let merge = self.merge(other);
        self.width + other.width > merge.width && self.height + other.height > merge.height
    }
    /// Returns regions other doesn't occupy in Self.
    pub fn substract(&self, other: Self) -> [Self; 4] {
        let crop = self.crop(&other);
        [
            Region::new(self.x, self.y, crop.x - self.x, self.height),
            Region::new(
                crop.x + crop.width,
                self.y,
                self.x + self.width - crop.x - crop.width,
                self.height,
            ),
            Region::new(crop.x, self.y, crop.width, crop.y - self.y),
            Region::new(
                crop.x,
                crop.y + crop.height,
                crop.width,
                self.y + self.height - crop.y - crop.height,
            ),
        ]
    }
    /// Combines two region into a single that occupies the space of both
    pub fn merge(&self, other: &Self) -> Self {
        let x = self.x.min(other.x);
        let y = self.y.min(other.y);
        let fx = (self.x + self.width).max(other.x + other.width);
        let fy = (self.y + self.height).max(other.y + other.height);

        Region {
            x,
            y,
            width: fx - x,
            height: fy - y,
        }
    }
    pub fn from_transform(transform: Transform, width: f32, height: f32) -> Self {
        Self::new(
            transform.tx,
            transform.ty,
            width * transform.sx,
            height * transform.sy,
        )
    }
    /// The position of the scene.
    pub fn position(&self) -> Position {
        Position::new(self.x, self.y)
    }
    /// Translate the region by a delta.
    pub fn translate(&self, x: f32, y: f32) -> Self {
        Region::new(self.x + x, self.y + y, self.width, self.height)
    }
    pub fn relative_to(&self, x: f32, y: f32) -> Self {
        Region::new(self.x - x, self.y - y, self.width, self.height)
    }
    // self fit in other
    pub fn fit(&self, other: &Self) -> bool {
        self.x >= other.x
            && self.x + self.width <= other.x + other.width
            && self.y >= other.y
            && self.y + self.height <= other.y + other.height
    }
    pub fn rfit(&self, other: &Self) -> bool {
        other.fit(self)
    }
    /// Checks if a point fits in a Region
    pub fn contains(&self, x: f32, y: f32) -> bool {
        self.x < x && x - self.x < self.width && self.y < y && y - self.y < self.height
    }
    pub fn scale(&self, sx: f32, sy: f32) -> Self {
        Self::new(self.x * sx, self.y * sy, self.width * sx, self.height * sy)
    }
    pub fn is_empty(&self) -> bool {
        self.width == 0. || self.height == 0.
    }
    pub fn top_anchor(&self) -> Position {
        Position::new(self.x + self.width / 2., self.y)
    }
    pub fn left_anchor(&self) -> Position {
        Position::new(self.x, self.y + self.height / 2.)
    }
    pub fn bottom_anchor(&self) -> Position {
        Position::new(self.x + self.width / 2., self.y + self.height)
    }
    pub fn right_anchor(&self) -> Position {
        Position::new(self.x + self.width, self.y + self.height / 2.)
    }
    pub fn start(&self) -> Position {
        Position::new(self.x, self.y)
    }
    pub fn center(&self) -> Position {
        Position::new(self.x + self.width / 2., self.y + self.height / 2.)
    }
    pub fn end(&self) -> Position {
        Position::new(self.x + self.width, self.y + self.height)
    }
    pub fn pad(&self, padding: f32) -> Region {
        Self {
            x: self.x - padding,
            y: self.y - padding,
            width: self.width + 2. * padding,
            height: self.height + 2. * padding,
        }
    }
}

use std::borrow::Cow;

/// A background texture.
pub trait DynBackground: AsRef<Texture> + Drawable {}

impl<T> DynBackground for T where T: AsRef<Texture> + Drawable {}

/// A stack of textures tied to a region.
#[derive(Clone)]
pub struct Background<'b> {
    depth: usize,
    pub(crate) background: &'b dyn DynBackground,
    pub(crate) position: Cow<'b, Position>,
}

impl<'b> Deref for Background<'b> {
    type Target = &'b dyn DynBackground;
    fn deref(&self) -> &Self::Target {
        &self.background
    }
}

impl<'b> Background<'b> {
    /// Create a new [`Background`].
    pub fn new(background: &'b Rectangle) -> Self {
        Background {
            depth: 0,
            position: Default::default(),
            background,
        }
    }
    /// The depth of the background.
    pub fn depth(&self) -> usize {
        self.depth
    }
    /// The texture of the background.
    pub fn texture(&self) -> &Texture {
        self.background.as_ref()
    }
    /// The region occupied by the background.
    pub fn region(&self) -> Region {
        Region::new(
            self.position.x,
            self.position.y,
            self.background.width(),
            self.background.height(),
        )
    }
}

// A widget that can me drawn.
pub struct RenderWidget<'a, W, T> {
    inner: &'a RefCell<W>,
    clip: Option<Region>,
    previous_region: Option<Region>,
    env: &'a Env<'a, 'a>,
    _data: PhantomData<T>,
}

impl<'a, W, T> RenderWidget<'a, W, T> {
    fn new<'b>(inner: &'a RefCell<W>, scene: &Scene, env: &'a Env<'a, 'a>) -> Self {
        Self {
            inner,
            clip: scene.clip.as_ref().map(|clip| **clip),
            previous_region: scene.previous_region.copied(),
            env,
            _data: PhantomData,
        }
    }
}

impl<'a, W, T> Geometry for RenderWidget<'a, W, T>
where
    W: Widget<T> + Geometry,
{
    fn width(&self) -> f32 {
        self.inner.borrow().width()
    }
    fn height(&self) -> f32 {
        self.inner.borrow().height()
    }
}

impl<'a, W, T> Drawable for RenderWidget<'a, W, T>
where
    W: Widget<T> + Geometry,
{
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        let mut context = ctx.borrow().with_transform(transform);
        let rectangle = Rectangle::new(self.width(), self.height());
        let scene = Scene {
            damage: false,
            depth: 0,
            position: Default::default(),
            clip: self.clip.as_ref().map(Cow::Borrowed),
            previous_region: self.previous_region.as_ref(),
            background: List::new(Background::new(&rectangle)),
            context: &mut context,
        };
        self.inner.borrow_mut().draw_scene(scene, self.env)
    }
}

impl<'a, W, T> AsRef<Texture> for RenderWidget<'a, W, T> {
    fn as_ref(&self) -> &Texture {
        &Texture::Transparent
    }
}

/// The graphical state of the application.
pub struct Scene<'s, 'c, 'b> {
    damage: bool,
    depth: usize,
    position: Position,
    clip: Option<Cow<'s, Region>>,
    previous_region: Option<&'s Region>,
    background: List<'s, Background<'b>>,
    context: &'s mut DrawContext<'c>,
}

impl<'s, 'c, 'b> Drop for Scene<'s, 'c, 'b> {
    fn drop(&mut self) {
        // Clear the clipmask in case it was set
        if let Some(ref region) = self.clip {
            if let Cow::Owned(_) = region {
                if let Backend::Pixmap { clipmask, .. } = self.context.deref_mut() {
                    if let Some(inner) = clipmask.as_mut() {
                        inner.clear()
                    }
                }
            }
        }
    }
}

impl<'s, 'c, 'b> AsMut<Cache<'c>> for Scene<'s, 'c, 'b> {
    fn as_mut(&mut self) -> &mut Cache<'c> {
        &mut self.context.cache
    }
}

impl<'s, 'c, 'b> Scene<'s, 'c, 'b> {
    /// Create a new scene.
    pub fn new(context: &'s mut DrawContext<'c>, rectangle: &'b Rectangle) -> Self {
        Self {
            damage: false,
            depth: 0,
            position: Default::default(),
            clip: None,
            previous_region: None,
            context,
            background: List::new(Background::new(rectangle)),
        }
    }
    /// Offsets the scene's position.
    pub fn translate(mut self, x: f32, y: f32) -> Self {
        self.position.x += x.round();
        self.position.y += y.round();
        self
    }
    /// Create another [`Scene`] by borrowing one's content.
    pub fn borrow<'n>(&'n mut self) -> Scene<'n, 'c, 'b> {
        Scene {
            depth: self.depth + 1,
            damage: self.damage,
            clip: self.clip.as_ref().map(|clip| Cow::Borrowed(clip.as_ref())),
            previous_region: None,
            position: self.position,
            background: self.background.clone(),
            context: self.context,
        }
    }
    /// Reborrow the [`Scene`] to bypass liftime limitations.
    pub fn scope(self) -> Self {
        self
    }
    /// Constraint the [`Scene`] to a [`Region`].
    pub(crate) fn clamp<'n>(&'n mut self, region: &'n Region) -> Scene<'n, 'c, 'b>
    where
        's: 'n,
    {
        Scene {
            depth: self.depth,
            damage: self.damage,
            background: self.background.clone(),
            previous_region: Some(region),
            position: self.position,
            context: self.context,
            clip: self.clip.as_ref().map(|clip| Cow::Borrowed(clip.as_ref())),
        }
    }
    /// The absolute position of the scene.
    pub fn position(&self) -> Position {
        self.position
    }
    /// The size of the [`Scene`].
    pub fn size(&self) -> Size {
        self.clip
            .as_ref()
            .map(|region| self.background.region().crop(region))
            .zip(self.previous_region)
            .map(|(region, previous)| region.crop(previous).size())
            .unwrap_or_else(|| self.background.region().size())
    }
    /// The clipped region.
    pub fn clip(&self) -> Option<&Region> {
        self.clip.as_deref()
    }
    pub(crate) fn context(&mut self) -> &mut DrawContext<'c> {
        self.context
    }
    /// Returns the damage state of the scene.
    pub fn damaged(&self) -> bool {
        self.damage
    }
    /// Damage the scene.
    ///
    /// The damage will be passed down to all child scenes.
    pub fn damage(mut self, region: Region) -> Self {
        if !self.damage {
            self.damage = true;
            let merge = self
                .previous_region
                .map(|previous| region.merge(previous))
                .unwrap_or(region);
            if let Some(clip) = self
                .clip
                .as_ref()
                .filter(|clip| merge.intersect(clip) && !merge.fit(clip))
            {
                self.context.set_clip(clip);
            }
            self.context.clear(
                find_background(&self.background),
                self.clip
                    .as_ref()
                    .map(|clip| merge.crop(clip))
                    .unwrap_or(merge),
            );
        }
        self
    }
    /// Applies a background to the scene.
    ///
    /// The background is passed as reference to all the child scenes.
    pub fn apply_background<'n>(&'n mut self, rect: &'n dyn DynBackground) -> Scene<'n, 'c, 'n>
    where
        's: 'n,
    {
        let t_background = Background {
            depth: self.depth,
            position: Cow::Borrowed(&self.position),
            background: rect,
        };
        if self.damage {
            let transform = self.context.transform();
            let region = t_background.region();
            if let Some(clip) = self
                .clip
                .as_ref()
                .filter(|clip| region.intersect(clip) && !region.fit(clip))
            {
                self.context.set_clip(clip);
            }
            rect.draw(
                self.context,
                transform.pre_translate(self.position.x, self.position.y),
            );
        }
        Scene {
            depth: self.depth,
            damage: self.damage,
            background: self.background.push(t_background),
            previous_region: self.previous_region,
            position: self.position,
            clip: self.clip.as_ref().map(|clip| Cow::Borrowed(clip.as_ref())),
            context: self.context,
        }
    }
    /// Clip a region of the scene.
    pub fn apply_clip<'n>(&'n mut self, size: Size) -> Scene<'n, 'c, 'b>
    where
        's: 'n,
    {
        if size >= self.context.size() && self.position == Position::default() {
            return self.borrow();
        }
        let region = Region::new(self.position.x, self.position.y, size.width, size.height);
        let clip = self
            .clip
            .as_ref()
            .map(|clip| clip.crop(&region))
            .unwrap_or(region);
        Scene {
            depth: self.depth,
            damage: self.damage,
            clip: Some(Cow::Owned(clip)),
            background: self.background.clone(),
            previous_region: self.previous_region,
            position: self.position,
            context: self.context,
        }
    }
    // Since primitives are at the end of a branch of the scene graph
    // and widget tree, they would only be inserted if the proxies
    // gates up to it were open.
    /// Draw an item.
    pub fn draw(self, drawable: &impl Drawable) {
        let transform = self.context.transform();
        let region = self.position.to_region(drawable);
        let merge = self
            .previous_region
            .as_ref()
            .map(|prev| prev.merge(&region))
            .unwrap_or_else(|| region);
        let background = background_region(&self.background);
        if self
            .clip
            .as_ref()
            .map(|clip| merge.intersect(clip.as_ref()))
            .unwrap_or(background.intersect(&merge))
        {
            if let Some(clip) = self.clip.as_ref().filter(|clip| !region.fit(clip)) {
                self.context.set_clip(clip);
            }
            if !self.damage {
                if self.background.depth == self.depth {
                    clear_background(
                        &self.background,
                        self.context,
                        self.depth,
                        &self.clip,
                        merge,
                    );
                } else {
                    self.context.clear(
                        &self.background,
                        self.clip
                            .as_ref()
                            .map(|clip| clip.crop(&merge))
                            .unwrap_or(merge),
                    );
                }
            }
            drawable.draw(
                self.context,
                transform.pre_translate(self.position.x, self.position.y),
            );
        }
    }
    pub fn as_drawable<'a, T, W: Widget<T>>(
        &self,
        cell: &'a RefCell<W>,
        env: &'a Env<'a, 'a>,
    ) -> RenderWidget<'a, W, T> {
        RenderWidget::new(cell, self, env)
    }
}

fn background_region(background: &List<Background>) -> Region {
    let depth = background.depth;
    background
        .iter()
        .map_while(|background| (background.depth == depth).then(|| background.region()))
        .reduce(|merge, region| merge.merge(&region))
        .unwrap()
}

fn clear_background(
    background: &List<Background>,
    context: &mut DrawContext,
    depth: usize,
    clip: &Option<Cow<Region>>,
    region: Region,
) {
    if background.depth >= depth {
        let l_region = background.region();
        if let Some(previous) = background
            .previous()
            .filter(|p| region.intersect(&p.region()))
        {
            clear_background(
                previous,
                context,
                depth,
                clip,
                l_region
                    .intersect(&region)
                    .then(|| region.merge(&l_region))
                    .unwrap_or(region),
            );
        } else {
            context.clear(
                background,
                clip.as_ref()
                    .map(|clip| clip.crop(&region))
                    .unwrap_or(region),
            )
        }
        if background.depth == depth && l_region.intersect(&region) {
            let transform = context.transform();
            background.draw(
                context,
                transform.pre_translate(background.position.x, background.position.y),
            );
        }
    } else {
        context.clear(
            background,
            clip.as_ref()
                .map(|clip| clip.crop(&region))
                .unwrap_or(region),
        )
    }
}

fn find_background<'a>(list: &'a List<Background>) -> &'a List<'a, Background<'a>> {
    if Texture::Transparent.ne(list.texture()) {
        list
    } else if let Some(previous) = list.previous() {
        find_background(previous)
    } else {
        list
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fit() {
        let a = Region::new(0., 0., 100., 100.);
        let b = Region::new(10., 10., 80., 80.);
        assert!(b.fit(&a));
        let a = Region::new(226., 186., 9., 9.);
        let b = Region::new(133., 95., 184., 109.);
        assert!(a.fit(&b));
        let a = Region::new(133., 42., 194., 40.);
        let b = Region::new(133., 92., 194., 109.);
        assert!(!a.fit(&b));
        assert!(!b.fit(&a));
    }
}
