//! Contexts provided to snui widgets.

use crate::utils::List;
use crate::*;
use crate::{
    cache::{font::FontCache, Cache},
    widgets::Alignment,
};
use fontdue::layout::GlyphPosition;
use msg_api::{post, FieldParameter, MessageBuilder, MessageHandler, Request};
use scene::*;

pub(crate) const TEXT: PixmapPaint = PixmapPaint {
    blend_mode: BlendMode::SourceOver,
    opacity: 1.0,
    quality: FilterQuality::Bilinear,
};

/// Available rendering backends.
pub enum Backend<'b> {
    /// A tiny-skia buffer.
    Pixmap {
        pixmap: PixmapMut<'b>,
        clipmask: Option<&'b mut ClipMask>,
    },
    /// Doesn't do anything. Meant for testing.
    Dummy,
}

impl<'b> Backend<'b> {
    pub fn borrow<'a>(&'a mut self) -> Backend<'a> {
        match self {
            Backend::Pixmap { pixmap, clipmask } => {
                let width = pixmap.width();
                let height = pixmap.height();
                Backend::Pixmap {
                    pixmap: PixmapMut::from_bytes(pixmap.data_mut(), width, height).unwrap(),
                    clipmask: if let Some(clipmask) = clipmask {
                        Some(clipmask)
                    } else {
                        None
                    },
                }
            }
            _ => Backend::Dummy,
        }
    }
    pub fn with_clipmask<'a>(self, clipmask: &'a mut ClipMask) -> Self
    where
        'a: 'b,
    {
        match self {
            Backend::Pixmap { pixmap, .. } => Backend::Pixmap {
                pixmap,
                clipmask: Some(clipmask),
            },
            _ => panic!("Cannot attach clipmask to Dummy backend"),
        }
    }
}

/// A context provided to widgets with `event` and `update`.
///
/// Acts as a guard around T to determine whether or not it was mutated.
pub struct UpdateContext<'a, 'b, T> {
    updated: Gbool<'a>,
    ready: GboolPair<'a>,
    data: &'b mut T,
    pub cache: Cache<'a>,
    window: &'a mut dyn WindowHandle<T>,
}

/// A context provided during `draw`.
pub struct DrawContext<'a> {
    transform: Transform,
    path_builder: Option<PathBuilder>,
    pub(crate) backend: Backend<'a>,
    pub cache: Cache<'a>,
    pub(crate) damage: Vec<Region>,
}

/// A context provided during `layout`.
pub struct LayoutContext<'a, 'b, T> {
    /// Forces widgets to propagate the layout
    pub(crate) force: bool,
    pub cache: Cache<'a>,
    data: &'b T,
    scale: f32,
}

impl<'a, 'b, T> LayoutContext<'a, 'b, T> {
    pub fn new(cache: Cache<'a>, data: &'b T) -> Self {
        LayoutContext {
            force: false,
            scale: 1.,
            data,
            cache,
        }
    }
    /// The scaling factor of the application.
    pub fn scale(&self) -> f32 {
        self.scale
    }
    pub fn with_scale(mut self, scale: f32) -> Self {
        self.scale = scale;
        self
    }
    /// Creates a new instance of the [`LayoutContext`] with the force field enabled.
    pub fn force<'s>(&'s mut self) -> LayoutContext<'s, 's, T> {
        LayoutContext {
            scale: self.scale,
            force: true,
            data: self.data,
            cache: self.cache.borrow(),
        }
    }
    pub fn borrow<'c>(&'c mut self) -> LayoutContext<'c, 'c, T> {
        LayoutContext {
            force: self.force,
            scale: self.scale,
            data: self.data,
            cache: self.cache.borrow(),
        }
    }
}

impl<'a, 'b, T> Deref for LayoutContext<'a, 'b, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.data
    }
}

/// A context menu or popup.
pub enum Menu<T> {
    /// The platform's menu.
    System { position: Position, serial: u32 },
    /// A custom menu widget.
    Popup {
        size: Size,
        serial: Option<u32>,
        offset: Position,
        anchor: (Alignment, Alignment),
        widget: Box<dyn Widget<T>>,
    },
    Toplevel {
        title: String,
        size: BoxConstraints,
        widget: Box<dyn Widget<T>>,
    },
}

/// A popup builder.
///
/// Used to build a popup menu.
#[derive(Clone, Debug, PartialEq)]
pub struct PopupBuilder {
    size: Size,
    position: Position,
    serial: Option<u32>,
    anchor: (Alignment, Alignment),
}

impl Default for PopupBuilder {
    fn default() -> Self {
        Self {
            size: Size::new(1., 1.),
            position: Default::default(),
            serial: Default::default(),
            anchor: Default::default(),
        }
    }
}

impl PopupBuilder {
    pub fn size(mut self, size: Size) -> Self {
        self.size = size;
        self
    }
    pub fn offset(mut self, position: Position) -> Self {
        self.position = self.position + position;
        self
    }
    pub fn anchor(mut self, x: Alignment, y: Alignment) -> Self {
        self.anchor = (x, y);
        self
    }
    pub fn serial(mut self, serial: u32) -> Self {
        self.serial = Some(serial);
        self
    }
    pub fn finish<T: 'static>(self, widget: impl Widget<T> + 'static) -> Menu<T> {
        Menu::Popup {
            anchor: self.anchor,
            offset: self.position,
            serial: self.serial,
            size: self.size,
            widget: Box::new(widget),
        }
    }
}

impl<T> Menu<T> {
    pub fn toplevel(title: impl Into<String>, widget: impl Widget<T> + 'static) -> Self {
        Self::Toplevel {
            title: title.into(),
            widget: Box::new(widget),
            size: BoxConstraints::new((0., 0.), ((1 << 10) as f32, (1 << 10) as f32)),
        }
    }
}

/// A handle to the window.
pub trait WindowHandle<T> {
    /// Closes the window.
    fn close(&mut self);
    fn minimize(&mut self);
    fn maximize(&mut self);
    /// Show a context menu
    fn show_menu(&mut self, menu: Menu<T>);
    /// Move the window.
    fn _move(&mut self, serial: u32);
    /// Set the window title.
    fn set_title(&mut self, title: String);
    /// Set the cursor.
    fn set_cursor(&mut self, cursor: Cursor);
    /// The amount of children windows.
    fn children(&self) -> usize;
    /// Retreive the state of the window.
    fn get_state(&self) -> &[WindowState];
    /// Starts and interactive resize from the anchor.
    fn resize(&mut self, serial: u32, anchor: (Alignment, Alignment));
}

impl<'b> Geometry for Backend<'b> {
    fn width(&self) -> f32 {
        match self {
            Backend::Dummy => 0.,
            Backend::Pixmap { pixmap, .. } => pixmap.width() as f32,
        }
    }
    fn height(&self) -> f32 {
        match self {
            Backend::Dummy => 0.,
            Backend::Pixmap { pixmap, .. } => pixmap.height() as f32,
        }
    }
}

impl<'b> AsRef<[u8]> for Backend<'b> {
    fn as_ref(&self) -> &[u8] {
        match self {
            Backend::Pixmap { pixmap, .. } => pixmap.as_ref().data(),
            _ => &[],
        }
    }
}

impl<'b> AsMut<[u8]> for Backend<'b> {
    fn as_mut(&mut self) -> &mut [u8] {
        match self {
            Backend::Pixmap { pixmap, .. } => pixmap.data_mut(),
            _ => &mut [],
        }
    }
}

impl<'a, 'b, T> Guarded for UpdateContext<'a, 'b, T> {
    fn restore(&mut self) {
        self.updated.take();
    }
    fn touched(&self) -> bool {
        self.updated.get()
    }
}

impl<'a, 'b, T> UpdateContext<'a, 'b, T> {
    /// Create a new [`UpdateContext`].
    pub fn new(data: &'b mut T, cache: Cache<'a>, window: &'a mut dyn WindowHandle<T>) -> Self {
        Self {
            data,
            cache,
            window,
            ready: GboolPair::default(),
            updated: Gbool::default(),
        }
    }
    /// Return a reference to the [`WindowHandle`].
    pub fn window(&mut self) -> &mut dyn WindowHandle<T> {
        self.window
    }
    /// Creates a custom popup passed to the [`WindowHandle`].
    pub fn create_popup<F>(&mut self, f: F)
    where
        F: FnOnce(&mut LayoutContext<T>) -> Menu<T>,
    {
        let mut layout = LayoutContext {
            scale: 1.,
            force: false,
            data: self.data,
            cache: self.cache.borrow(),
        };
        let menu = f(&mut layout);
        self.window.show_menu(menu);
    }
    /// Create an handle to the [`UpdateContext`].
    pub(crate) fn borrow<'s>(&'s mut self) -> UpdateContext<'s, 's, T>
    where
        'a: 's,
        'b: 's,
    {
        UpdateContext {
            updated: self.updated.borrow(),
            ready: self.ready.map(Gbool::default()),
            data: self.data,
            cache: self.cache.borrow(),
            window: self.window,
        }
    }
    /// Puts the context in an updated state.
    pub fn update(&mut self) {
        self.updated.set(true);
    }
    /// Puts the context in a ready state.
    ///
    /// This is necessary for any widgets that relies on update.
    ///
    /// Preparing the context during update will ensure your widget will always be in the update loop.
    pub fn prepare(&mut self) {
        self.ready.set(true);
    }
    pub fn ready(&self) -> bool {
        self.ready.get()
    }
    pub fn layout(&mut self) -> LayoutContext<T> {
        LayoutContext {
            scale: 1.,
            cache: self.cache.borrow(),
            data: self.data,
            force: false,
        }
    }
}

impl<'a, 'b, T> UpdateContext<'a, 'b, T> {
    pub fn post<'r, R>(&'r mut self, req: &R, data: R::Data<'r>)
    where
        R: MessageBuilder,
        R::Message<'r>: PartialEq + FieldParameter,
        T: Request<R>,
        T: MessageHandler<R::Message<'r>>,
    {
        self.updated.upgrade(post(self.data, req, data));
    }
}

impl<'a, 'b, M, T> MessageHandler<M> for UpdateContext<'a, 'b, T>
where
    T: MessageHandler<M>,
{
    fn post(&mut self, message: M) {
        self.data.post(message);
        self.updated.upgrade(true);
    }
}

impl<'a, 'b, T> Deref for UpdateContext<'a, 'b, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.data
    }
}

use tiny_skia::Transform;

impl<'a> DrawContext<'a> {
    /// Create a new [`DrawContext`].
    pub fn new(backend: Backend<'a>, cache: Cache<'a>) -> Self {
        Self {
            cache,
            backend,
            damage: Vec::new(),
            transform: Transform::identity(),
            path_builder: Some(PathBuilder::new()),
        }
    }
    /// Set a transformation for the context.
    pub fn with_transform(mut self, transform: Transform) -> Self {
        self.transform = transform;
        self
    }
    /// Reborrow the context.
    pub fn borrow(&mut self) -> DrawContext {
        DrawContext {
            cache: self.cache.borrow(),
            backend: self.backend.borrow(),
            damage: Vec::new(),
            transform: self.transform,
            path_builder: Some(PathBuilder::new()),
        }
    }
    /// The scale factor of the application.
    pub fn scale(&self) -> f32 {
        self.transform.sx.max(self.transform.sy)
    }
    /// Draw a shape from a path.
    pub fn draw<F>(&mut self, f: F)
    where
        F: FnOnce(&mut Self, PathBuilder) -> Path,
    {
        let pb = self.path_builder.take().unwrap();
        self.path_builder = Some(f(self, pb).clear());
    }
    pub(crate) fn transform(&self) -> Transform {
        self.transform
    }
    /// Push a damaged [`Region`].
    pub fn commit(&mut self, region: Region) {
        if !region.is_empty() {
            if let Some(r) = self.damage.last_mut() {
                if region.intersect(r) {
                    let merge = r.merge(&region);
                    *r = merge;
                } else {
                    self.damage.push(region);
                }
            } else {
                self.damage.push(region);
            }
        }
    }
    pub(crate) fn set_clip(&mut self, region: &Region) {
        let width = self.width();
        let height = self.height();
        match &mut self.backend {
            Backend::Pixmap { clipmask, .. } => {
                if let Some(clipmask) = clipmask {
                    let mut pb = self.path_builder.take().unwrap();
                    pb.push_rect(region.x, region.y, region.width, region.height);
                    if let Some(path) = pb.finish().and_then(|path| path.transform(self.transform))
                    {
                        clipmask.set_path(
                            width as u32,
                            height as u32,
                            &path,
                            FillRule::Winding,
                            false,
                        );
                        self.path_builder = Some(path.clear());
                    }
                }
            }
            _ => {}
        }
    }
    /// Cleans a region of the buffer.
    pub(crate) fn clear(&mut self, background: &List<Background>, region: Region) {
        let blend;
        if let Some(background) = background
            .previous()
            .filter(|_| background.texture().is_transparent())
        {
            self.clear(background, region);
            blend = BlendMode::SourceOver;
        } else {
            if let Some(last) = self.damage.last() {
                if last.intersect(&region) {
                    for subregion in last.merge(&region).substract(*last) {
                        if !subregion.is_empty() {
                            self.clear(background, subregion);
                        }
                    }
                    return;
                }
            }
            blend = BlendMode::Source;
            self.commit(region);
        }
        match &mut self.backend {
            Backend::Pixmap { pixmap, clipmask } => {
                let clip_mask = clipmask
                    .as_ref()
                    .and_then(|clipmask| (!clipmask.is_empty()).then_some(&**clipmask));
                match background.texture() {
                    Texture::Color(color) => {
                        pixmap.fill_rect(
                            region.into(),
                            &Paint {
                                shader: Shader::SolidColor(*color),
                                blend_mode: blend,
                                anti_alias: false,
                                force_hq_pipeline: false,
                            },
                            self.transform,
                            clip_mask,
                        );
                    }
                    Texture::RadialGradient(gradient) => {
                        let background_region = background.region();
                        let diameter = background_region.width.min(background_region.height);
                        let center = background_region.center();
                        pixmap.fill_rect(
                            region.into(),
                            &Paint {
                                shader: RadialGradient::new(
                                    Position::default().into(),
                                    Position::default().into(),
                                    diameter / 2.,
                                    gradient.stops.to_vec(),
                                    gradient.mode,
                                    Transform::identity()
                                        .pre_scale(
                                            region.width / diameter,
                                            region.height / diameter,
                                        )
                                        .post_translate(center.x, center.y),
                                )
                                .expect("Failed to build RadialGradient shader"),
                                blend_mode: blend,
                                anti_alias: false,
                                force_hq_pipeline: false,
                            },
                            self.transform,
                            clip_mask,
                        );
                    }
                    Texture::LinearGradient(gradient) => {
                        let background_region = background.region();
                        let start = match gradient.orientation {
                            Orientation::Horizontal => background_region.start(),
                            Orientation::Vertical => background_region.top_anchor(),
                        };
                        let end = match gradient.orientation {
                            Orientation::Horizontal => background_region.end(),
                            Orientation::Vertical => background_region.bottom_anchor(),
                        };
                        pixmap.fill_rect(
                            region.into(),
                            &Paint {
                                shader: LinearGradient::new(
                                    start.into(),
                                    end.into(),
                                    gradient.stops.to_vec(),
                                    gradient.mode,
                                    self.transform,
                                )
                                .expect("Failed to build LinearGradient shader"),
                                blend_mode: blend,
                                anti_alias: false,
                                force_hq_pipeline: false,
                            },
                            self.transform,
                            clip_mask,
                        );
                    }
                    #[cfg(feature = "img")]
                    Texture::Image(image) => {
                        let sx = background.width() / image.width();
                        let sy = background.height() / image.height();
                        pixmap.fill_rect(
                            region.into(),
                            &Paint {
                                shader: Pattern::new(
                                    image.pixmap(),
                                    SpreadMode::Repeat,
                                    FilterQuality::Bilinear,
                                    1.0,
                                    self.transform
                                        .post_translate(
                                            background.position.x,
                                            background.position.y,
                                        )
                                        .post_scale(sx, sy),
                                ),
                                blend_mode: blend,
                                anti_alias: false,
                                force_hq_pipeline: false,
                            },
                            self.transform,
                            clip_mask,
                        );
                    }
                    Texture::Transparent => {
                        if background.depth() == 0 {
                            pixmap.fill_rect(
                                region.into(),
                                &Paint {
                                    shader: Shader::SolidColor(Color::TRANSPARENT),
                                    blend_mode: BlendMode::Clear,
                                    anti_alias: false,
                                    force_hq_pipeline: false,
                                },
                                self.transform,
                                clip_mask,
                            );
                        }
                    }
                }
            }
            _ => {}
        }
    }
    /// Create a [`TextRenderer`].
    pub fn text<'t>(&'t mut self) -> TextRenderer<'t, 'a>
    where
        'a: 't,
    {
        TextRenderer {
            font_cache: self.cache.font_cache,
            backend: &mut self.backend,
        }
    }
    /// The list of damaged regions.
    pub fn damage_queue(&self) -> &[Region] {
        self.damage.as_slice()
    }
    /// Returns the backend and the damaged regions.
    pub fn unwrap(self) -> (Backend<'a>, Vec<Region>) {
        (self.backend, self.damage)
    }
}

/// A text rendering helper.
pub struct TextRenderer<'a, 'b> {
    font_cache: &'a mut FontCache,
    backend: &'a mut Backend<'b>,
}

impl<'a, 'b> Deref for TextRenderer<'a, 'b> {
    type Target = FontCache;
    fn deref(&self) -> &Self::Target {
        &*self.font_cache
    }
}

impl<'a, 'b> DerefMut for TextRenderer<'a, 'b> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut *self.font_cache
    }
}

impl<'a, 'b> TextRenderer<'a, 'b> {
    /// Draw glyphs.
    pub fn draw_glyphs(&mut self, glyphs: &[GlyphPosition<Color>], transform: Transform) {
        match &mut self.backend {
            Backend::Pixmap { pixmap, clipmask } => {
                let clipmask = clipmask
                    .as_ref()
                    .and_then(|clipmask| (!clipmask.is_empty()).then_some(&**clipmask));

                for gp in glyphs {
                    if let Some(glyph) = self.font_cache.map[gp.font_index].get(gp) {
                        if let Some(glyph) =
                            PixmapRef::from_bytes(glyph, gp.width as u32, gp.height as u32)
                        {
                            pixmap.draw_pixmap(
                                0,
                                0,
                                glyph,
                                &TEXT,
                                transform
                                    .pre_scale(transform.sx.powi(-1), transform.sy.powi(-1))
                                    .post_translate(gp.x, gp.y),
                                clipmask,
                            );
                        }
                    }
                }
            }
            _ => {}
        }
    }
}

impl<'a> Deref for DrawContext<'a> {
    type Target = Backend<'a>;
    fn deref(&self) -> &Self::Target {
        &self.backend
    }
}

impl<'a> DerefMut for DrawContext<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.backend
    }
}
