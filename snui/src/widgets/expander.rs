use crate::widgets::{Alignment, Orientation};
use crate::*;

use super::Positionable;

/// A container that occupies space.
///
/// An expander is a layout widget that will attempt to occupy as much vertical
/// or horizontal space as possible.
pub struct Expander<T, W: Widget<T>> {
    inner: Positioner<Proxy<T, W>>,
    alignment: Alignment,
    orientation: Orientation,
}

impl<T, W: Widget<T>> Deref for Expander<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W: Widget<T>> DerefMut for Expander<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<T, W: Themeable + Widget<T>> Themeable for Expander<T, W> {
    const NAME: &'static str = W::NAME;
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        self.inner.theme(list, env)
    }
}

impl<T, W: Widget<T>> Expander<T, W> {
    /// An horizontal expander.
    ///
    /// The inner widget will take as much horizontal space as possible.
    pub fn horizontal(inner: W) -> Self {
        Self::from(inner)
    }
    /// A vertical expander.
    ///
    /// The inner widget will take as much vertical space as possible.
    pub fn vertical(inner: W) -> Self {
        Self::from(inner).orientation(Orientation::Vertical)
    }
    /// The orientation of the [`Expander`].
    pub fn orientation(mut self, orientation: Orientation) -> Self {
        self.orientation = orientation;
        self
    }
    /// The alignment of the [`Expander`].
    pub fn alignment(mut self, alignment: Alignment) -> Self {
        self.alignment = alignment;
        self
    }
}

impl<T, W: Widget<T>> From<W> for Expander<T, W> {
    fn from(inner: W) -> Self {
        Self {
            inner: Positioner::new(Proxy::new(inner)),
            alignment: Alignment::Center,
            orientation: Orientation::Horizontal,
        }
    }
}

impl<T, W: Widget<T>> Widget<T> for Expander<T, W> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner.draw_scene(scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.inner.event(ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.inner.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let inner_size = self.inner.layout(ctx, bc, env);
        let mut size = inner_size;
        match self.orientation {
            Orientation::Horizontal => size.width = size.width.max(bc.maximum_width()),
            Orientation::Vertical => size.height = size.height.max(bc.maximum_height()),
        }
        match self.alignment {
            Alignment::Start => {}
            Alignment::Center => {
                self.inner.set_position(
                    (size.width - inner_size.width) / 2.,
                    (size.height - inner_size.height) / 2.,
                );
            }
            Alignment::End => {
                self.inner
                    .set_position(size.height - inner_size.width, inner_size.height);
            }
        }
        size
    }
}
