use crate::scene::Region;
use crate::*;
use msg_api::*;
use std::collections::HashMap;
use std::hash::Hash;

pub mod msg {
    use msg_api::*;
    use std::marker::PhantomData;

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    /// A [`MessageBuilder`] for the identifier of a view.
    pub struct CurrentView<T>(PhantomData<T>);

    impl<T> CurrentView<T> {
        pub(crate) fn new() -> Self {
            CurrentView(PhantomData)
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    /// A [`MessageBuilder`] for the existence of a view.
    pub struct HasView<'a, T>(&'a T);

    impl<T> MessageBuilder for CurrentView<T> {
        type Data<'a> = T;
        type Message<'a> = T;

        fn from<'a>(&self, data: Self::Data<'a>) -> Self::Message<'a> {
            data
        }
    }

    impl<T: PartialEq> MessageBuilder for HasView<'_, T> {
        type Data<'a> = T;
        type Message<'a> = bool;

        fn from<'a>(&self, data: Self::Data<'a>) -> Self::Message<'a> {
            data.eq(self.0)
        }
    }

    impl<'a, T> HasView<'a, T> {
        pub(crate) fn new(view: &'a T) -> Self {
            HasView(view)
        }
        pub fn get(self) -> &'a T {
            self.0
        }
    }
}

use msg::*;

/// A widget with an indexed list of tabbed widgets.
pub struct TabView<T, M, W: Widget<T> = Box<dyn Widget<T>>> {
    key: M,
    previous: Option<Region>,
    views: HashMap<M, Proxy<T, W>>,
}

impl<T, M: PartialEq + Default, W: Widget<T>> Default for TabView<T, M, W> {
    fn default() -> Self {
        Self {
            previous: None,
            key: M::default(),
            views: HashMap::new(),
        }
    }
}

impl<T, M: Eq + Hash, W: Widget<T>> TabView<T, M, W> {
    /// A builder style variant of `add`.
    pub fn with(mut self, key: M, widget: W) -> Self {
        self.add(key, widget);
        self
    }
    /// Add a widget to the tab container.
    pub fn add(&mut self, key: M, widget: W) {
        self.views.insert(key, Proxy::new(widget));
    }
    /// Focus the view with the associated key.
    pub fn select<Q: Eq + Hash>(&mut self, key: &Q)
    where
        M: std::borrow::Borrow<Q> + Clone,
    {
        if let Some(key) = self.views.get_key_value(key).map(|(key, _)| key.clone()) {
            self.key = key;
        }
    }
    pub fn remove<Q: Eq + Hash>(&mut self, key: &Q) -> Option<Proxy<T, W>>
    where
        M: std::borrow::Borrow<Q>,
    {
        self.views.remove(key)
    }
}

impl<T, M> TabView<T, M, Box<dyn Widget<T>>>
where
    M: Eq + Hash,
    T: 'static,
{
    /// Add a any widget to the tab container.
    pub fn add_view<W: Widget<T> + 'static>(&mut self, key: M, widget: W) {
        self.views.insert(key, Proxy::new(Box::new(widget)));
    }
    /// A builder style variant of `add_view`.
    pub fn with_view<W: Widget<T> + 'static>(mut self, key: M, widget: W) -> Self {
        self.add_view(key, widget);
        self
    }
}

impl<T, M, W: Widget<T>> Widget<T> for TabView<T, M, W>
where
    M: Eq + Hash,
    for<'m> T: Request<CurrentView<M>>,
    for<'m> T: Request<HasView<'m, M>>,
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        if let Some(view) = self.views.get_mut(&self.key) {
            if let Some(region) = self.previous.take() {
                view.draw_scene(scene.damage(region), env)
            } else {
                view.draw_scene(scene, env)
            }
        } else if let Some(region) = self.previous.take() {
            scene.damage(region);
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.views
            .get_mut(&self.key)
            .map(|view| view.event(ctx, event, env))
            .unwrap_or_default()
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        ctx.prepare();
        let key = ctx.get(&CurrentView::new());
        if let Some(key) = {
            self.key.ne(&key).then(|| {
                if let Some(view) = self.views.get_mut(&self.key).filter(|_| self.key.ne(&key)) {
                    view.upgrade();
                    self.previous = view.region();
                    key
                } else {
                    key
                }
            })
        } {
            self.key = key;
            if let Some(view) = self.views.get_mut(&self.key) {
                view.upgrade();
            }
        }
        self.views.retain(|key, view| {
            let res = ctx.get(&HasView::new(key));
            if !res {
                self.previous = view.region();
            }
            res
        });
        self.views
            .get_mut(&self.key)
            .map(|view| view.update(ctx, env))
            .or_else(|| self.previous.is_some().then_some(Damage::Partial))
            .unwrap_or_default()
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.views
            .get_mut(&self.key)
            .map(|view| view.layout(ctx, bc, env))
            .unwrap_or_default()
    }
}

#[macro_export]
/// A [`TabView`] widget builder.
macro_rules! Tab {
    ($($key:expr => $child:expr),* $(,)?) => {
        {
            let mut f = $crate::widgets::TabView::default();

            $(
                f.add_view($key, $child);
            )*

            f
        }
    };
}
