use snui_derive::Guarded;

use crate::widgets::layout::Positionable;
use crate::*;
use std::ops::{Deref, DerefMut};

/// A widget restricting another to a clipped region.
#[derive(Guarded)]
pub struct Clipbox<T, W>
where
    W: Widget<T>,
{
    size: Guard<Size>,
    inner: Positioner<Proxy<T, W>>,
}

impl<T, W: Widget<T>> Clipbox<T, W> {
    pub fn new(inner: W) -> Self {
        Self {
            size: Default::default(),
            inner: Positioner::new(Proxy::new(inner)),
        }
    }
    pub fn width(&self) -> f32 {
        self.size.width
    }
    pub fn height(&self) -> f32 {
        self.size.height
    }
    pub fn size(&self) -> Size {
        self.size.get()
    }
    /// The size of the clipped region.
    pub fn clip_size(&self) -> Size {
        self.size.get()
    }
    /// The size of the inner widget.
    pub fn inner_size(&self) -> Size {
        self.inner.size()
    }
    pub fn position(&self) -> Position {
        self.inner.position()
    }
}

impl<T, W: Widget<T>> Geometry for Clipbox<T, W> {
    fn width(&self) -> f32 {
        self.size.width
    }
    fn height(&self) -> f32 {
        self.size.height
    }
}

impl<T, W: Widget<T>> Positionable for Clipbox<T, W> {
    type Value = f32;

    fn set_position(&mut self, x: Self::Value, y: Self::Value) {
        self.inner.set_position(x, y);
        if self.touched() {
            self.inner.upgrade()
        }
    }
}

impl<T, W: Widget<T>> GeometryExt for Clipbox<T, W> {
    fn set_width(&mut self, width: f32) {
        self.size.make_mut().width = width;
    }
    fn set_height(&mut self, height: f32) {
        self.size.make_mut().height = height;
    }
}

impl<T, W: Widget<T>> Widget<T> for Clipbox<T, W> {
    fn draw_scene(&mut self, mut scene: Scene, env: &Env) {
        if self.size.reset() {
            self.inner.upgrade()
        }
        self.inner
            .draw_scene(scene.apply_clip(self.size.get().ceil()), env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.damage().max(self.inner.event(ctx, event, env))
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.damage().max(self.inner.update(ctx, env))
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env);
        let Size { width, height } = self.size.get();
        Size::new(
            width.max(bc.maximum_width()).ceil(),
            height.max(bc.maximum_height()),
        )
    }
}

impl<T, W: Widget<T>> Deref for Clipbox<T, W> {
    type Target = Proxy<T, W>;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W: Widget<T>> DerefMut for Clipbox<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
