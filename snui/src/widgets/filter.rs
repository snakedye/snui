use crate::*;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

use super::Style;

pub struct Filter<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    cb: F,
    inner: W,
    _data: PhantomData<T>,
}

impl<T, W, F> Guarded for Filter<T, W, F>
where
    W: Widget<T> + Guarded,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    fn restore(&mut self) {
        self.inner.restore()
    }
    fn touched(&self) -> bool {
        self.inner.touched()
    }
}

impl<T, W, F> Filter<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    /// Create a new [`Filter`].
    pub fn new(inner: W, cb: F) -> Self {
        Self {
            cb,
            inner,
            _data: PhantomData,
        }
    }
}

impl<T, W, F> Widget<T> for Filter<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner.draw_scene(scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.inner.event(ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        (self.cb)(&mut self.inner, ctx, env);
        self.inner.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env)
    }
}

impl<T, W, F> Themeable for Filter<T, W, F>
where
    W: Widget<T> + Themeable,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    const NAME: &'static str = W::NAME;

    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        self.inner.theme(list, env)
    }
}

impl<T, W, F> Style for Filter<T, W, F>
where
    W: Widget<T> + Style,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.inner.set_texture(texture);
    }
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_left_radius(radius);
    }
}

impl<T, W, F> Deref for Filter<T, W, F>
where
    W: Widget<T> + Themeable,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W, F> DerefMut for Filter<T, W, F>
where
    W: Widget<T> + Themeable,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &'a Env),
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
