use crate::cache::source::{RawImage, Source};
use crate::utils::List;
use crate::*;

use std::ops::Deref;
use std::path::{Path, PathBuf};

use super::shapes::{Rectangle, Style};

/// Aspect ratio of an image.
#[derive(Clone, Copy, PartialEq, Debug, Hash, Eq)]
pub enum Scale {
    /// Takes all the size available.
    Fill,
    /// Tries to fill a region while respecting the original aspect ratio of the image.
    KeepAspect,
}

/// A widget that renders a pixmap.
pub struct Image {
    path: PathBuf,
    size: Option<Size>,
    scale: Scale,
    pub(super) inner: Option<InnerImage>,
}

impl Image {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        Self {
            path: path.as_ref().to_path_buf(),
            size: None,
            scale: Scale::Fill,
            inner: None,
        }
    }
    pub fn scale(mut self, scale: Scale) -> Self {
        self.scale = scale;
        self
    }
}

impl<T> Widget<T> for Image {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        if let Some(inner) = self.inner.as_ref() {
            scene.draw(&inner.image)
        }
    }
    fn event(&mut self, _ctx: &mut UpdateContext<T>, _event: Event, _env: &Env) -> Damage {
        Damage::None
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        Damage::None
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        match self.inner.as_mut() {
            Some(inner) => match inner.scale {
                Scale::KeepAspect => {
                    let raw = match inner.image.as_ref() {
                        scene::Texture::Image(raw) => raw,
                        _ => unreachable!(),
                    };
                    let bc = if bc.maximum_width() < bc.maximum_height() {
                        bc.with_max(
                            bc.maximum_width(),
                            bc.maximum_width() * raw.height() / raw.width(),
                        )
                    } else {
                        bc.with_max(
                            bc.maximum_height() * raw.width() / raw.height(),
                            bc.maximum_height(),
                        )
                    };
                    Widget::<T>::layout(&mut inner.image, ctx, &bc, env)
                }
                Scale::Fill => Widget::<T>::layout(&mut inner.image, ctx, bc, env),
            },
            None => {
                if let Ok(raw) = ctx.cache.source_cache.get(self.path.as_path()) {
                    if let Source::Pixmap(raw) = raw {
                        let mut inner = InnerImage::from(raw);
                        inner.set_scale(self.scale);
                        if let Some(Size { width, height }) = self.size.take() {
                            inner.image.set_width(width as f32);
                            inner.image.set_height(height as f32);
                        }
                        let size = inner.size();
                        self.inner = Some(inner);
                        return size;
                    }
                } else {
                    // Creates an empty InnerImage so we don't request an image again.
                    self.inner = Some(InnerImage {
                        scale: Scale::Fill,
                        image: Rectangle::default(),
                    });
                }
                Size::default()
            }
        }
    }
}

#[derive(Clone, PartialEq)]
pub struct InnerImage {
    pub(super) scale: Scale,
    pub(super) image: Rectangle,
}

impl From<RawImage> for InnerImage {
    fn from(raw: RawImage) -> Self {
        Self {
            scale: Scale::KeepAspect,
            image: Rectangle::new(raw.width(), raw.height()).texture(raw),
        }
    }
}

impl From<InnerImage> for RawImage {
    fn from(inner: InnerImage) -> Self {
        match inner.image.inner.unwrap().texture {
            scene::Texture::Image(raw) => raw,
            _ => unreachable!(),
        }
    }
}

impl Themeable for InnerImage {
    const NAME: &'static str = "image";

    fn theme(&mut self, list: List<Id>, env: &dyn Environment) {
        if let Some(scale) = env.try_get(list, "display") {
            self.scale = match scale {
                "fill" => Scale::Fill,
                "fit" => Scale::KeepAspect,
                _ => {
                    eprintln!(
                        "err: {} is not a valid token for {}.display",
                        scale,
                        Self::NAME
                    );
                    self.scale
                }
            };
        }
        self.image.theme(list, env)
    }
}

impl InnerImage {
    pub fn from_raw(buf: Vec<u8>, width: u32, height: u32) -> Self {
        RawImage::from_raw(buf, width, height).unwrap().into()
    }
    pub fn set_scale(&mut self, scale: Scale) {
        self.scale = scale;
    }
}

impl Geometry for InnerImage {
    fn width(&self) -> f32 {
        self.image.width()
    }
    fn height(&self) -> f32 {
        self.image.height()
    }
}

impl Drawable for InnerImage {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        self.image.draw(ctx, transform)
    }
}

impl Deref for InnerImage {
    type Target = RawImage;
    fn deref(&self) -> &Self::Target {
        match self.image.as_ref() {
            scene::Texture::Image(raw) => raw,
            _ => unreachable!(),
        }
    }
}
