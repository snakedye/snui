use crate::*;
use crate::{scene::Texture, widgets::*};
use snui_derive::Guarded;
use std::ops::{Deref, DerefMut};
use widgets::shapes::Style;

struct Close {
    rect: Rectangle,
}

impl Close {
    fn new() -> Self {
        Close {
            rect: Rectangle::new(15., 15.).texture(color::BG4),
        }
    }
}

impl<T> Widget<T> for Close {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.rect
            .set_texture(env.get("accent-color-6").unwrap_or(Texture::Transparent));
        scene.draw(&self.rect)
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        Damage::None
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, _env: &Env) -> Damage {
        if let Event::Pointer(MouseEvent {
            pointer,
            ref position,
        }) = event
        {
            match pointer {
                Pointer::Enter => ctx.window().set_cursor(Cursor::OpenHand),
                Pointer::Leave => ctx.window().set_cursor(Cursor::Arrow),
                _ => {
                    if self.rect.contains(position) && pointer.left_button_click().is_some() {
                        ctx.window().close();
                    }
                }
            }
        }
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, _bc: &BoxConstraints, _env: &Env) -> Size {
        self.rect.size()
    }
}

struct Maximize {
    maximized: bool,
    rect: BorderedRectangle,
}

impl Maximize {
    fn new() -> Self {
        Maximize {
            maximized: false,
            rect: BorderedRectangle::default()
                .texture(color::BG4)
                .border_width(2.)
                .with_size(15., 15.),
        }
    }
}

impl<T> Widget<T> for Maximize {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.rect
            .set_texture(env.get("accent-color-3").unwrap_or(Texture::Transparent));
        scene.draw(&self.rect)
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        Damage::None
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, _env: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer,
                ref position,
            }) => match pointer {
                Pointer::Enter => ctx.window().set_cursor(Cursor::OpenHand),
                Pointer::Leave => ctx.window().set_cursor(Cursor::Arrow),
                _ => {
                    if self.rect.contains(position) && pointer.left_button_click().is_some() {
                        ctx.window().maximize();
                    }
                }
            },
            Event::Configure => {
                self.maximized = ctx
                    .window()
                    .get_state()
                    .iter()
                    .any(|s| WindowState::Maximized.eq(s));
            }
            _ => {}
        }
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, _bc: &BoxConstraints, _env: &Env) -> Size {
        self.rect.size()
    }
}

struct Minimize {
    rect: Rectangle,
}

impl Minimize {
    fn new() -> Minimize {
        Minimize {
            rect: Rectangle::new(15., 4.).texture(color::BG4),
        }
    }
}

impl Geometry for Minimize {
    fn height(&self) -> f32 {
        15.
    }
    fn width(&self) -> f32 {
        15.
    }
}

impl<T> Widget<T> for Minimize {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        let width = 3.;
        self.rect
            .set_texture(env.get("accent-color-1").unwrap_or(Texture::Transparent));
        scene
            .translate(0., (self.height() - width) / 2.)
            .draw(&self.rect)
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        Damage::None
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, _env: &Env) -> Damage {
        if let Event::Pointer(MouseEvent {
            pointer,
            ref position,
        }) = event
        {
            match pointer {
                Pointer::Enter => ctx.window().set_cursor(Cursor::OpenHand),
                Pointer::Leave => ctx.window().set_cursor(Cursor::Arrow),
                _ => {
                    if self.contains(position) && pointer.left_button_click().is_some() {
                        ctx.window().minimize();
                    }
                }
            }
        }
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, _bc: &BoxConstraints, _env: &Env) -> Size {
        self.size()
    }
}

/// Window header.
struct Header<T, W: Widget<T>> {
    inner: Positioner<Proxy<T, W>>,
}

impl<T, W: Widget<T>> Header<T, W> {
    /// Create a new window header.
    pub fn new(inner: W) -> Self {
        Header {
            inner: Positioner::new(Proxy::new(inner)),
        }
    }
}

impl<T, W: Widget<T>> Widget<T> for Header<T, W> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner.draw_scene(scene, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.inner.update(ctx, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let damage = self.inner.event(ctx, event, env);
        if damage.is_none() && !ctx.touched() {
            match event {
                Event::Pointer(MouseEvent { pointer, position }) => {
                    if self.inner.contains(&position) {
                        if let Some(serial) = pointer.left_button_click() {
                            ctx.window()._move(serial);
                        } else if let Some(serial) = pointer.right_button_click() {
                            // Cursor position is relative.
                            // Only works because the Header is at 0, 0
                            ctx.window().show_menu(Menu::System { position, serial });
                        }
                    }
                }
                _ => {}
            }
        }
        damage
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env)
    }
}

impl<T, W: Widget<T>> Deref for Header<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        self.inner.deref()
    }
}

impl<T, W: Widget<T>> DerefMut for Header<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.inner.deref_mut()
    }
}

#[derive(Clone, Debug, PartialEq, Default, Guarded)]
struct WindowBorder(BorderedRectangle);

impl Deref for WindowBorder {
    type Target = BorderedRectangle;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for WindowBorder {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl AsRef<Texture> for WindowBorder {
    fn as_ref(&self) -> &Texture {
        self.0.as_ref()
    }
}

impl Geometry for WindowBorder {
    fn width(&self) -> f32 {
        self.0.width()
    }
    fn height(&self) -> f32 {
        self.0.height()
    }
}

impl Drawable for WindowBorder {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        ctx.draw(|ctx, pb| {
            let radius = self
                .inner
                .radius
                .map(|r| r.as_abs(self.min_side()) + self.inner.border_width);
            let path = Rectangle::path(
                pb,
                self.inner.width + self.inner.border_width,
                self.inner.height + self.inner.border_width,
                radius,
            )
            .unwrap();
            super::stroke_path(
                &path,
                &self.inner.texture,
                self.inner.border_width,
                ctx.deref_mut(),
                self,
                transform.pre_translate(self.inner.border_width / 2., self.inner.border_width / 2.),
            );
            path
        })
    }
}

/// A traditional window with an header.
pub struct Window<T, H, W>
where
    H: Widget<T>,
    W: Widget<T>,
{
    activated: bool,
    positioned: bool,
    /// Top window decoration
    header: Header<T, H>,
    /// The window's content
    window: Positioner<Proxy<T, WidgetStyle<T, W>>>,
    border: WindowBorder,
}

impl<T, H, W> Window<T, H, W>
where
    H: Widget<T> + Style,
    W: Widget<T>,
{
    pub fn new(header: H, widget: W) -> Self {
        Window {
            header: Header::new(header),
            activated: false,
            positioned: false,
            border: WindowBorder::default(),
            window: Positioner::new(Proxy::new(WidgetStyle::new(widget))),
        }
    }
}

impl<T, H, W> Themeable for Window<T, H, W>
where
    H: Widget<T> + Style,
    W: Widget<T>,
{
    const NAME: &'static str = "window";

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(radius) = env.try_get(id, "radius") {
            self.set_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "top-left-radius") {
            self.set_top_left_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "top-right-radius") {
            self.set_top_right_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "bottom-right-radius") {
            self.set_bottom_right_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "bottom-left-radius") {
            self.set_bottom_left_radius(radius)
        }
        if let Some(texture) = env.try_get::<Texture>(id, "background") {
            self.set_texture(texture)
        }
        if let Some(texture) = env.try_get::<Texture>(id, "border-texture") {
            self.border.set_texture(texture)
        }
        if let Some(width) = env.try_get(id, "border-width") {
            self.border.set_border_width(width)
        }
        if let Some(texture) = env.try_get::<Texture>(id.push(Id::from("header")), "background") {
            self.header.set_texture(texture)
        }
    }
}

impl<T, H, W> Widget<T> for Window<T, H, W>
where
    H: Widget<T> + Style,
    W: Widget<T>,
{
    fn draw_scene(&mut self, mut scene: Scene, env: &Env) {
        let env = env.push(Id::new("window", self.activated.then_some("activated")));
        if self.border.reset() {
            let position = scene.position();
            scene = scene.damage(position.to_region(&self.border));
        }
        let mut border = scene.apply_background(&self.border);
        self.window.draw_scene(border.borrow(), &env);
        self.header.draw_scene(border.borrow(), &env);
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        let env = env.push(Id::new("window", self.activated.then_some("activated")));
        let header = self.header.update(ctx, &env);
        let window = self.window.update(ctx, &env);
        header.max(window)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let mut env = env.push(Id::from("window"));
        let d = match event {
            Event::Pointer(MouseEvent {
                ref pointer,
                ref position,
            }) => {
                if let Some(serial) = pointer.left_button_click() {
                    let x = if position.x < 0. {
                        START
                    } else if position.x < self.border.width() {
                        CENTER
                    } else {
                        END
                    };
                    let y = if position.y < 0. {
                        START
                    } else if position.y < self.border.height() {
                        CENTER
                    } else {
                        END
                    };
                    if x != CENTER || y != CENTER {
                        ctx.window().resize(serial, (x, y));
                    }
                }
                let header = self.header.event(ctx, event, &env);
                let window = self.window.event(ctx, event, &env);
                header.max(window)
            }
            Event::Message(STYLESHEET_MESSAGE) => {
                env.map(self);
                let header = self.header.event(ctx, event, &env);
                let window = self.window.event(ctx, event, &env);
                header.max(window).max(self.border.damage())
            }
            Event::Configure => {
                let window = ctx.window();
                let state = window.get_state();
                let mut activated = false;
                let mut positioned = false;
                for state in state.iter().rev() {
                    match state {
                        WindowState::Activated | WindowState::Resizing => {
                            activated = true;
                        }
                        WindowState::TiledLeft
                        | WindowState::TiledRight
                        | WindowState::TiledBottom
                        | WindowState::TiledTop
                        | WindowState::Maximized
                        | WindowState::Fullscreen => {
                            positioned = true;
                            self.set_radius(RelativeUnit::Unit(0.));
                        }
                    }
                }
                if activated {
                    env = env.set_class("activated");
                }
                if activated != self.activated {
                    if let Some(texture) = env.get::<Texture>("background") {
                        self.set_texture(texture)
                    }
                    if let Some(texture) = env.get::<Texture>("border-texture") {
                        self.border.set_texture(texture)
                    }
                    if let Some(width) = env.get("border-width") {
                        self.border.set_border_width(width)
                    }
                    if let Some(texture) = env.push(Id::from("header")).get::<Texture>("background")
                    {
                        self.header.set_texture(texture)
                    }
                    // if !activated {
                    //     event = Event::Focus(Focus::local_unfocus())
                    // }
                }
                if ((!self.activated != !activated) || (!positioned && self.positioned))
                    && !positioned
                {
                    if let Some(radius) = env.get("radius") {
                        self.set_radius(radius)
                    }
                    if let Some(radius) = env.get("top-left-radius") {
                        self.set_top_left_radius(radius)
                    }
                    if let Some(radius) = env.get("top-right-radius") {
                        self.set_top_right_radius(radius)
                    }
                    if let Some(radius) = env.get("bottom-right-radius") {
                        self.set_bottom_right_radius(radius)
                    }
                    if let Some(radius) = env.get("bottom-left-radius") {
                        self.set_bottom_left_radius(radius)
                    }
                }
                self.activated = activated;
                self.positioned = positioned;
                self.header
                    .event(ctx, event, &env)
                    .max(self.window.event(ctx, event, &env))
            }
            _ => {
                let header = self.header.event(ctx, event, &env);
                let window = self.window.event(ctx, event, &env);
                header.max(window)
            }
        };
        d
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let env = env.push(Id::new("window", self.activated.then_some("activated")));
        let border_width = self.border.inner.border_width;
        let header_height = self.header.inner.inner.height();
        let (w_width, w_height) = self
            .window
            .layout(
                ctx,
                &bc.crop(border_width * 2., header_height + border_width * 2.),
                &env,
            )
            .into();
        let (h_width, h_height) = self
            .header
            .layout(
                ctx,
                &bc.with_min(w_width, header_height)
                    .with_max(w_width, header_height),
                &env,
            )
            .into();
        self.header.inner.set_position(border_width, border_width);
        self.window
            .set_position(border_width, h_height + border_width);
        self.border.set_size(
            w_width.max(h_width) + 2. * border_width,
            h_height + w_height + 2. * border_width,
        );
        self.border.size()
    }
}

impl<T, H, W> Style for Window<T, H, W>
where
    H: Widget<T> + Style,
    W: Widget<T>,
{
    fn set_texture<B: Into<Texture>>(&mut self, texture: B) {
        self.window.set_texture(texture);
    }
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.border.set_top_left_radius(radius);
        self.header.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.border.set_top_right_radius(radius);
        self.header.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.border.set_bottom_right_radius(radius);
        self.window.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.border.set_bottom_left_radius(radius);
        self.window.set_bottom_left_radius(radius);
    }
}

impl<T, H, W> Deref for Window<T, H, W>
where
    H: Widget<T>,
    W: Widget<T>,
{
    type Target = W;
    fn deref(&self) -> &Self::Target {
        self.window.inner.deref()
    }
}

impl<T, H, W> DerefMut for Window<T, H, W>
where
    H: Widget<T>,
    W: Widget<T>,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.window.inner.deref_mut()
    }
}

fn wm_button<T>() -> impl Widget<T>
where
    T: 'static,
{
    Column::new()
        .with_child(Minimize::new())
        .with_spacing(10.)
        .with_child(Maximize::new())
        .with_spacing(10.)
        .with_child(Close::new())
}

fn headerbar<T: 'static, H: Widget<T> + 'static>(widget: H) -> impl Widget<T> {
    Column!(
        widget.flex(Orientation::Vertical),
        wm_button().clamp().anchor(END, CENTER),
    )
}

pub fn default_window<T, H, W>(header: H, widget: W) -> Window<T, impl Widget<T> + Style, W>
where
    T: 'static,
    H: Widget<T> + 'static,
    W: Widget<T>,
{
    Window::new(
        headerbar(header)
            .with_min_height(25.)
            .with_max_height(40.)
            .background(color::TRANSPARENT)
            .padding(10.),
        widget,
    )
}
