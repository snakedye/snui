use crate::*;

use widgets::*;

/// A type can be scrolled.
pub trait Scrollable {
    fn forward(&mut self, step: Option<f32>);
    fn backward(&mut self, step: Option<f32>);
    fn orientation(&self) -> Orientation;
    fn inner_width(&self) -> f32;
    fn inner_height(&self) -> f32;
    fn position(&self) -> f32;
}

/// A region where a user can scroll over a widget.
pub struct Scrollbox<T, W: Widget<T>> {
    orientation: Orientation,
    inner: Clipbox<T, W>,
}

impl<T, W: Widget<T>> Scrollbox<T, W> {
    pub fn new(inner: W) -> Self {
        Self {
            inner: Clipbox::new(inner),
            orientation: Orientation::Vertical,
        }
    }
    pub fn set_orientation(&mut self, orientation: Orientation) {
        self.orientation = orientation;
    }
    pub fn orientation(mut self, orientation: Orientation) -> Self {
        self.set_orientation(orientation);
        self
    }
}

impl<T, W: Widget<T>> Scrollable for Scrollbox<T, W> {
    fn forward(&mut self, step: Option<f32>) {
        let mut coords = self.inner.position();
        match self.orientation {
            Orientation::Horizontal => {
                coords.x = (coords.x + step.unwrap_or(10.)).min(0.);
            }
            Orientation::Vertical => {
                coords.y = (coords.y + step.unwrap_or(10.)).min(0.);
            }
        }
        self.inner.set_position(coords.x, coords.y);
    }
    fn backward(&mut self, step: Option<f32>) {
        let mut coords = self.inner.position();
        let Size { width, height } = self.inner.inner_size();
        match self.orientation {
            Orientation::Horizontal => {
                coords.x =
                    (coords.x - step.unwrap_or(10.)).max((self.inner.width() - width).min(0.));
            }
            Orientation::Vertical => {
                coords.y =
                    (coords.y - step.unwrap_or(10.)).max((self.inner.height() - height).min(0.))
            }
        }
        self.inner.set_position(coords.x, coords.y);
    }
    fn position(&self) -> f32 {
        match self.orientation {
            Orientation::Horizontal => self.inner.position().x,
            Orientation::Vertical => self.inner.position().y,
        }
    }
    fn inner_width(&self) -> f32 {
        self.inner.width()
    }
    fn inner_height(&self) -> f32 {
        self.inner.height()
    }
    fn orientation(&self) -> Orientation {
        self.orientation
    }
}

impl<T, W> Widget<T> for Scrollbox<T, W>
where
    W: Widget<T>,
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner.draw_scene(scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer: Pointer::Scroll { orientation, step },
                ..
            }) => {
                if orientation == self.orientation {
                    match step {
                        Step::Increment(i) => {
                            if i.is_positive() {
                                (0..i).for_each(|_| self.forward(None));
                            } else {
                                (i..0).for_each(|_| self.backward(None));
                            }
                        }
                        Step::Value(value) => {
                            if value.is_sign_positive() {
                                self.forward(Some(value));
                            } else {
                                self.backward(Some(value.abs()));
                            }
                        }
                    }
                }
                self.inner.event(ctx, event, env)
            }
            _ => self.inner.event(ctx, event, env),
        }
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.inner.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env);
        let Size { width, height } = self.inner.inner_size();
        match self.orientation {
            Orientation::Horizontal => {
                self.inner.set_size(bc.maximum_width(), height);
            }
            Orientation::Vertical => {
                self.inner.set_size(width, bc.maximum_height());
            }
        }
        self.inner.size()
    }
}
