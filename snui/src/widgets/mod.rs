//!	All purpose widgets.

pub(crate) mod active;
pub(crate) mod button;
pub(crate) mod canvas;
pub(crate) mod clipbox;
mod debug;
pub(crate) mod expander;
pub(crate) mod filter;
pub(crate) mod proxy;
pub(crate) mod revealer;
pub(crate) mod tab;
pub(crate) mod toggle;

#[cfg(feature = "img")]
pub(crate) mod image;
pub(crate) mod input;
pub(crate) mod label;
pub(crate) mod layout;
pub(crate) mod padding;
pub(crate) mod scroll;
pub(crate) mod shapes;
pub(crate) mod slider;
#[cfg(feature = "svg")]
pub(crate) mod svg;
pub(crate) mod window;

pub use crate::environment::Stylesheet;
#[cfg(feature = "img")]
pub use crate::widgets::image::*;
#[cfg(feature = "svg")]
pub use crate::widgets::svg::*;
use crate::*;
pub use active::Activate;
pub use button::Button;
pub use canvas::*;
pub use clipbox::Clipbox;
pub use expander::*;
pub use filter::*;
pub use input::*;
pub use label::{DynLabel, Label, LabelRef};
pub use layout::*;
pub use padding::Padding;
pub use revealer::*;
pub use scroll::{Scrollable, Scrollbox};
pub use shapes::*;
pub use slider::{Progress, Slider};
pub use tab::TabView;
pub use toggle::*;
pub use window::{default_window, Window};

use std::ops::{Deref, DerefMut};

pub const START: Alignment = Alignment::Start;
pub const CENTER: Alignment = Alignment::Center;
pub const END: Alignment = Alignment::End;

pub mod msg {
    pub use super::tab::msg::*;
}

/// Alignment on an axis.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default)]
pub enum Alignment {
    Start,
    #[default]
    Center,
    End,
}

/// Layout size constraints.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Constraint {
    /// The size remains the same regardless.
    Fixed,
    /// The inner size is the maximum size the widget can have.
    ///
    /// The widget will try to occupy as little space as possible by default.
    Upward,
    /// The inner size is the minimum size the widget can have.
    ///
    /// The widget will try to occupy as much space as possible by default.
    Downward,
}

impl<T> Widget<T> for () {
    fn draw_scene(&mut self, _: Scene, _: &Env) {}
    fn event(&mut self, _ctx: &mut UpdateContext<T>, _event: Event, _: &Env) -> Damage {
        Damage::None
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _: &Env) -> Damage {
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, _bc: &BoxConstraints, _: &Env) -> Size {
        Size::default()
    }
}

/// An empty widget with that adds space between widgets.
#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct Spacer(Size);

impl<T> Widget<T> for Spacer {
    fn draw_scene(&mut self, _: Scene, _: &Env) {}
    fn event(&mut self, _ctx: &mut UpdateContext<T>, _event: Event, _: &Env) -> Damage {
        Damage::None
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _: &Env) -> Damage {
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, _bc: &BoxConstraints, _: &Env) -> Size {
        self.0
    }
}

impl Spacer {
    pub fn horizontal(width: f32) -> Self {
        Self(Size::new(width, 0.))
    }
    pub fn vertical(height: f32) -> Self {
        Self(Size::new(0., height))
    }
}

/// A widget with dimension constraints.
pub struct WidgetBox<T, W: Widget<T>> {
    widget: Positioner<Proxy<T, W>>,
    width: Guard<Option<RelativeUnit>>,
    height: Guard<Option<RelativeUnit>>,
    constraint: Constraint,
    anchor: (Alignment, Alignment),
}

impl<T, W: Widget<T>> WidgetBox<T, W> {
    pub fn new(widget: W) -> Self {
        Self {
            widget: Positioner::new(Proxy::new(widget)),
            width: Guard::from(None),
            height: Guard::from(None),
            anchor: (Alignment::Center, Alignment::Center),
            constraint: Constraint::Downward,
        }
    }
    pub fn constraint(mut self, constraint: Constraint) -> Self {
        self.constraint = constraint;
        self
    }
    pub fn set_constraint(&mut self, constraint: Constraint) {
        self.constraint = constraint;
    }
    pub fn anchor(mut self, x: Alignment, y: Alignment) -> Self {
        self.anchor = (x, y);
        self
    }
    pub fn set_anchor(&mut self, x: Alignment, y: Alignment) {
        self.anchor = (x, y);
    }
    pub fn set_rel_width(&mut self, width: RelativeUnit) {
        self.width.set(Some(width));
    }
    pub fn set_rel_height(&mut self, height: RelativeUnit) {
        self.height.set(Some(height));
    }
    pub fn set_rel_size(&mut self, width: RelativeUnit, height: RelativeUnit) {
        self.width.set(Some(width));
        self.height.set(Some(height));
    }
    pub fn with_rel_width(mut self, width: RelativeUnit) -> Self {
        self.set_rel_width(width);
        self
    }
    /// Set the height of the item.
    pub fn with_rel_height(mut self, height: RelativeUnit) -> Self {
        self.set_rel_height(height);
        self
    }
    /// Set the size of the item.
    pub fn with_rel_size(mut self, width: RelativeUnit, height: RelativeUnit) -> Self {
        self.set_rel_size(width, height);
        self
    }
}

impl<T, W: Widget<T>> GeometryExt for WidgetBox<T, W> {
    fn set_width(&mut self, width: f32) {
        self.width.set(Some(RelativeUnit::Unit(width)));
    }
    fn set_height(&mut self, height: f32) {
        self.height.set(Some(RelativeUnit::Unit(height)));
    }
}

impl<T, W: Widget<T>> Widget<T> for WidgetBox<T, W> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.restore();
        self.widget.draw_scene(scene, env);
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.damage().max(self.widget.event(ctx, event, env))
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.damage().max(self.widget.update(ctx, env))
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let mut width = match self.width.get() {
            Some(l_width) => match self.constraint {
                Constraint::Fixed => l_width.as_abs(bc.maximum_width()),
                Constraint::Upward => l_width.as_abs(bc.maximum_width()).min(bc.maximum_width()),
                Constraint::Downward => l_width.as_abs(bc.maximum_width()).max(bc.maximum_width()),
            },
            None => bc.maximum_width(),
        };
        let mut height = match self.height.get() {
            Some(l_height) => match self.constraint {
                Constraint::Fixed => l_height.as_abs(bc.maximum_height()),
                Constraint::Upward => l_height
                    .as_abs(bc.maximum_height())
                    .min(bc.maximum_height()),
                Constraint::Downward => l_height
                    .as_abs(bc.maximum_height())
                    .max(bc.maximum_height()),
            },
            None => bc.maximum_height(),
        };
        let (inner_width, inner_height) = self
            .widget
            .layout(
                ctx,
                &match self.constraint {
                    Constraint::Fixed => BoxConstraints::new((width, height), (width, height)),
                    Constraint::Downward => bc.with_min(width, height),
                    Constraint::Upward => bc.with_max(width, height),
                },
                env,
            )
            .into();
        width = width.max(inner_width);
        height = height.max(inner_height);
        let (horizontal, vertical) = &self.anchor;
        let dx = match horizontal {
            Alignment::Start => 0.,
            Alignment::Center => (width - inner_width) / 2.,
            Alignment::End => width - inner_width,
        };
        let dy = match vertical {
            Alignment::Start => 0.,
            Alignment::Center => (height - inner_height) / 2.,
            Alignment::End => height - inner_height,
        };
        self.widget.set_position(dx, dy);
        Size::new(width, height)
    }
}

impl<T, W: Widget<T>> Themeable for WidgetBox<T, W> {
    const NAME: &'static str = "box";
    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        let (width_prop, height_prop) = match self.constraint {
            Constraint::Downward => ("min-width", "min-height"),
            Constraint::Fixed => ("width", "height"),
            Constraint::Upward => ("max-width", "max-height"),
        };
        if let Some(width) = env.try_get(id, width_prop) {
            self.width.set(Some(width));
        }
        if let Some(height) = env.try_get(id, height_prop) {
            self.height.set(Some(height));
        }
    }
}

impl<T, W: Widget<T>> Guarded for WidgetBox<T, W> {
    fn restore(&mut self) {
        self.width.restore();
        self.height.restore();
    }
    fn touched(&self) -> bool {
        self.width.touched() | self.height.touched()
    }
}

impl<T, W: Widget<T>> Deref for WidgetBox<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.widget
    }
}

impl<T, W: Widget<T>> DerefMut for WidgetBox<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.widget
    }
}
