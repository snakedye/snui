use crate::scene::Position;
use crate::widgets::utils::*;
use crate::widgets::Positionable;
use crate::*;
use msg_api::*;

use super::Clipbox;

/// The state of the [`Revealer`].
#[derive(Clone, Default, Debug, Copy, PartialEq, Eq)]
pub enum RevealerState {
    #[default]
    Hidden,
    Visible,
}

impl std::ops::Not for RevealerState {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            RevealerState::Hidden => RevealerState::Visible,
            RevealerState::Visible => RevealerState::Hidden,
        }
    }
}

#[derive(Clone, Debug, Copy, PartialEq, Eq)]
enum Direction {
    Normal = 1,
    Inverted = -1,
}

/// The direction of the animation transition.
#[derive(Clone, Debug, Copy, PartialEq, Eq)]
pub enum Transition {
    SlideRight,
    SlideLeft,
    SlideTop,
    SlideBottom,
}

/// A inner that reveals or hides its child with an animated transition.
pub struct Revealer<T, M, E, W>
where
    E: Easer,
    W: Widget<T>,
{
    inner: Clipbox<T, W>,
    message: M,
    easer: Option<E>,
    duration: u32,
    direction: Direction,
    state: Option<RevealerState>,
    orientation: Orientation,
    visible: bool,
}

impl<T, M, E, W> Revealer<T, M, E, W>
where
    E: Easer,
    W: Widget<T>,
{
    pub fn new(message: M, inner: W) -> Self {
        Self {
            message,
            direction: Direction::Inverted,
            inner: Clipbox::new(inner),
            duration: 500,
            visible: false,
            easer: None,
            orientation: Orientation::Vertical,
            state: None,
        }
    }
    pub fn set_transition(&mut self, transition: Transition) {
        match transition {
            Transition::SlideRight => {
                self.direction = Direction::Normal;
                self.orientation = Orientation::Horizontal;
            }
            Transition::SlideLeft => {
                self.direction = Direction::Inverted;
                self.orientation = Orientation::Horizontal;
            }
            Transition::SlideBottom => {
                self.direction = Direction::Normal;
                self.orientation = Orientation::Vertical;
            }
            Transition::SlideTop => {
                self.direction = Direction::Inverted;
                self.orientation = Orientation::Vertical;
            }
        }
    }
    pub fn transition(mut self, transition: Transition) -> Self {
        self.set_transition(transition);
        self
    }
    fn run(&mut self) {
        if let Some(state) = self.state {
            match state {
                RevealerState::Hidden => self.hide(),
                RevealerState::Visible => self.reveal(),
            }
        }
    }
    fn reveal(&mut self) {
        let direction = match self.direction {
            Direction::Normal => 1.,
            Direction::Inverted => -1.,
        };
        let Size { width, height } = self.inner.inner_size();
        self.easer = Some(match self.orientation {
            Orientation::Horizontal => {
                self.inner.set_position(direction * width, 0.);
                Easer::new(0.5, 1.)
            }
            Orientation::Vertical => {
                self.inner.set_position(0., direction * height);
                Easer::new(0.5, 1.)
            }
        });
    }
    fn hide(&mut self) {
        self.easer = Some(match self.orientation {
            Orientation::Horizontal => Easer::new(0., 0.5),
            Orientation::Vertical => Easer::new(0., 0.5),
        });
    }
    /// The duration of the animation in ms.
    pub fn duration(mut self, duration: u32) -> Self {
        self.duration = duration;
        self
    }
    pub fn state(&self) -> RevealerState {
        self.state.unwrap()
    }
}

impl<T, M, E, W> Geometry for Revealer<T, M, E, W>
where
    E: Easer,
    W: Widget<T>,
{
    fn width(&self) -> f32 {
        match self.direction {
            Direction::Normal => self.inner.inner_size().width() - self.inner.position().x,
            Direction::Inverted => self.inner.inner_size().width() + self.inner.position().x,
        }
    }
    fn height(&self) -> f32 {
        match self.direction {
            Direction::Normal => self.inner.inner_size().height() - self.inner.position().y,
            Direction::Inverted => self.inner.inner_size().height() + self.inner.position().y,
        }
    }
}

impl<M, E, W, T> Widget<T> for Revealer<T, M, E, W>
where
    E: Easer,
    W: Widget<T>,
    M: MessageBuilder,
    for<'m> M::Message<'m>: Release<bool>,
    T: Request<M>,
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        if self.visible {
            self.inner.draw_scene(scene, env)
        }
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        ctx.prepare();
        if self.visible != ctx.get(&self.message).release() {
            let state = self
                .visible
                .then_some(RevealerState::Visible)
                .unwrap_or_default();
            self.state = Some(!state);
            self.inner.update(ctx, env).max(Damage::Frame(true))
        } else {
            self.inner.update(ctx, env)
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        match event {
            Event::Callback(frame_time) => {
                if self.state.is_some() {
                    if let Some(easer) = self.easer.as_mut() {
                        self.visible = true;
                        let steps = easer.partial_steps(frame_time, self.duration);
                        let position = easer.step_by(steps);
                        let direction = self.direction as i32 as f32;
                        let Size { width, height } = self.inner.inner_size();
                        match self.orientation {
                            Orientation::Vertical => self.inner.set_position(
                                0.,
                                direction
                                    * height
                                    * position.unwrap_or_else(|| Easer::position(easer)),
                            ),
                            Orientation::Horizontal => self.inner.set_position(
                                direction
                                    * width
                                    * position.unwrap_or_else(|| Easer::position(easer)),
                                0.,
                            ),
                        }
                        if position.is_none() {
                            self.visible = matches!(self.state, Some(RevealerState::Visible));
                            self.state = None;
                            self.easer = None;
                        }
                        return self
                            .inner
                            .event(ctx, event, env)
                            .max(Damage::Frame(self.inner.touched()));
                    } else {
                        // Create an easer
                        self.run();
                        // Call event on self again
                        return self
                            .inner
                            .event(ctx, event, env)
                            .max(Damage::Frame(self.inner.touched()));
                    }
                } else if self.visible {
                    return self.inner.event(ctx, event, env);
                }
            }
            Event::Keyboard(_) | Event::Pointer(_) => {
                if self.visible && !matches!(self.state, Some(RevealerState::Hidden)) {
                    return self.inner.event(ctx, event, env);
                }
            }
            _ => return self.inner.event(ctx, event, env),
        }
        Damage::None
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env);
        let Size { width, height } = self.inner.inner_size();
        if self.visible {
            let direction = self.direction as i32 as f32;
            let Position { x, y } = self.inner.position();
            self.inner
                .set_size(width - direction * x, height - direction * y);
        } else {
            match self.orientation {
                Orientation::Horizontal => self.inner.set_size(0., height),
                Orientation::Vertical => self.inner.set_size(width, 0.),
            }
        }
        self.size()
    }
}
