use crate::cache::font::{FontProperty, FontStyle};
use crate::*;
use fontdue::layout::{CoordinateSystem, Layout, LayoutSettings};
use msg_api::{MessageBuilder, Request};
use std::ops::{Deref, DerefMut};
use tiny_skia::*;

const DEFAULT_LAYOUT_SETTINGS: LayoutSettings = LayoutSettings {
    x: 0.,
    y: 0.,
    max_width: None,
    max_height: None,
    horizontal_align: fontdue::layout::HorizontalAlign::Left,
    vertical_align: fontdue::layout::VerticalAlign::Top,
    wrap_style: fontdue::layout::WrapStyle::Word,
    wrap_hard_breaks: true,
};

const DEFAULT_FONT_SIZE: f32 = 15.;

static DEFAULT_FONT: FontProperty = FontProperty {
    family: String::new(),
    style: FontStyle::Regular,
};

#[derive(Clone, Copy, Debug, PartialEq, Default)]
enum LayoutSize {
    #[default]
    Unset,
    Pending,
    Set(Size),
}

impl From<LayoutSize> for Size {
    fn from(s: LayoutSize) -> Self {
        match s {
            LayoutSize::Set(size) => size,
            _ => Size::default(),
        }
    }
}

/// A static text widget.
pub struct Label {
    pub(crate) layout: Layout<Color>,
    pub(crate) text: String,
    pub(crate) font_size: f32,
    pub(crate) color: Color,
    settings: LayoutSettings,
    font: FontProperty,
    size: LayoutSize,
}

/// A reference to a [`Label`].
///
/// It can also be used to to layout text from borrowed data.
#[derive(Copy, Clone, PartialEq)]
pub struct LabelRef<'s> {
    pub text: &'s str,
    pub font_size: f32,
    pub color: Color,
    pub settings: &'s LayoutSettings,
    pub font: &'s FontProperty,
}

impl<'s> From<&'s str> for LabelRef<'s> {
    fn from(text: &'s str) -> Self {
        LabelRef {
            text,
            color: to_color(color::FG0),
            font_size: DEFAULT_FONT_SIZE,
            font: &DEFAULT_FONT,
            settings: &DEFAULT_LAYOUT_SETTINGS,
        }
    }
}

impl Guarded for Label {
    fn restore(&mut self) {}
    fn touched(&self) -> bool {
        matches!(self.size, LayoutSize::Pending | LayoutSize::Unset)
    }
}

impl<'s> LabelRef<'s> {
    pub fn font(mut self, font: &'s FontProperty) -> Self {
        self.font = font;
        self
    }
    pub fn color(mut self, color: impl Into<Color>) -> Self {
        self.color = color.into();
        self
    }
    pub fn font_size(mut self, font_size: f32) -> Self {
        self.font_size = font_size;
        self
    }
}

impl Deref for Label {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.text
    }
}

impl DerefMut for Label {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.size = LayoutSize::Pending;
        &mut self.text
    }
}

impl Themeable for Label {
    const NAME: &'static str = "label";

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(color) = env.try_get(id, "foreground") {
            self.color = color;
            self.size = LayoutSize::Pending;
        }
        if let Some(font) = env.try_get(id, "font-family") {
            self.font.family = font;
            self.size = LayoutSize::Pending;
        }
        if let Some(style) = env.try_get::<&str>(id, "font-style") {
            self.font.style = FontStyle::from(style);
            self.size = LayoutSize::Pending;
        }
        if let Some(size) = env.try_get(id, "font-size") {
            self.font_size = size;
            self.size = LayoutSize::Pending;
        }
    }
}

impl Label {
    fn new<T: Into<String>>(text: T) -> Label {
        Label {
            text: text.into(),
            layout: Layout::new(CoordinateSystem::PositiveYDown),
            font_size: DEFAULT_FONT_SIZE,
            font: FontProperty::default(),
            settings: DEFAULT_LAYOUT_SETTINGS,
            color: to_color(color::FG0),
            size: LayoutSize::Unset,
        }
    }
    pub fn as_ref(&self) -> LabelRef {
        LabelRef {
            color: self.color,
            text: self.text.as_str(),
            font_size: self.font_size,
            settings: &self.settings,
            font: &self.font,
        }
    }
    pub fn text_layout(&self) -> &Layout<Color> {
        &self.layout
    }
    pub fn set_color(&mut self, color: impl Into<Color>) {
        self.color = color.into();
    }
    pub fn edit(&mut self, s: &str) {
        if s.ne(self.text.as_str()) {
            self.text.replace_range(0.., s);
            self.size = LayoutSize::Pending;
        }
    }
    pub fn font<F: Into<FontProperty>>(mut self, font: F) -> Self {
        self.font = font.into();
        self
    }
    pub fn color(mut self, color: u32) -> Self {
        self.color = to_color(color);
        self
    }
    pub fn font_size(mut self, font_size: f32) -> Self {
        self.font_size = font_size;
        self
    }
    pub fn settings(mut self, settings: LayoutSettings) -> Self {
        self.settings = settings;
        self
    }
}

impl PartialEq for Label {
    fn eq(&self, other: &Self) -> bool {
        self.font_size == other.font_size
            && self.text == other.text
            && self.color == other.color
            && self.font.eq(&other.font)
    }
}

impl Eq for Label {}

impl<T> From<T> for Label
where
    T: ToString,
{
    fn from(label: T) -> Self {
        Label::new(label.to_string())
    }
}

impl Geometry for Label {
    fn width(&self) -> f32 {
        Size::from(self.size).width
    }
    fn height(&self) -> f32 {
        Size::from(self.size).height
    }
}

impl Drawable for Label {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        let mut text = ctx.text();
        text.draw_glyphs(self.layout.glyphs(), transform);
    }
}

impl<T> Widget<T> for Label {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        scene.draw(self)
    }
    fn event(&mut self, _ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        if event == STYLESHEET_SIGNAL {
            env.map(self)
        }
        self.damage()
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        self.damage()
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        if !matches!(self.size, LayoutSize::Unset) {
            self.settings.max_width = bc
                .maximum_width()
                .ne(&std::f32::INFINITY)
                .then_some(bc.maximum_width() * ctx.scale());
            self.settings.max_height = bc
                .maximum_height()
                .ne(&std::f32::INFINITY)
                .then_some(bc.maximum_height() * ctx.scale());
        } else {
            env.map(self);
        }
        let label = LabelRef {
            text: self.text.as_str(),
            font_size: self.font_size * ctx.scale(),
            color: self.color,
            settings: &self.settings,
            font: &self.font,
        };
        let fc = ctx.cache.font_cache();
        let Size { width, height } = fc.layout_into(&mut self.layout, &label);
        self.size = LayoutSize::Set(Size::new(width / ctx.scale(), height / ctx.scale()).ceil());
        self.size.into()
    }
}

/// A label with a dynamic text.
///
/// The text is provided by the application.
pub struct DynLabel<M> {
    message: M,
    label: Label,
}

impl<T> Guarded for DynLabel<T> {
    fn restore(&mut self) {
        self.label.restore()
    }
    fn touched(&self) -> bool {
        self.label.touched()
    }
}

impl<M> Themeable for DynLabel<M> {
    const NAME: &'static str = Label::NAME;

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        self.label.theme(id, env);
    }
}

impl<M> DynLabel<M> {
    pub fn new(message: M) -> Self {
        Self {
            message,
            label: Label::from(""),
        }
    }
    /// Assigns a message to the label.
    pub fn map<T>(self, message: T) -> DynLabel<T> {
        DynLabel {
            message,
            label: self.label,
        }
    }
}

impl<M, T> Widget<T> for DynLabel<M>
where
    M: MessageBuilder,
    for<'m> M::Message<'m>: AsRef<str>,
    T: Request<M>,
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        Widget::<T>::draw_scene(&mut self.label, scene, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        ctx.prepare();
        self.label.edit(ctx.get(&self.message).as_ref());
        self.label.update(ctx, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.label.event(ctx, event, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        Widget::<T>::layout(&mut self.label, ctx, bc, env)
    }
}

impl<M> Deref for DynLabel<M> {
    type Target = Label;
    fn deref(&self) -> &Self::Target {
        &self.label
    }
}

impl<M> DerefMut for DynLabel<M> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.label
    }
}
