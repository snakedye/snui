use crate::widgets::Scrollable;
use crate::*;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

/// A widget that add padding around its child.
#[derive(Clone, Debug, PartialEq)]
pub struct Padding<T, W: Widget<T>> {
    pub(crate) widget: W,
    pub(crate) padding: Guard<[f32; 4]>,
    _data: PhantomData<T>,
}

impl<T, W: Widget<T>> Padding<T, W> {
    pub fn new(widget: W) -> Self {
        Self {
            widget,
            _data: PhantomData,
            padding: Default::default(),
        }
    }
    pub fn set_padding_top(&mut self, padding: f32) {
        self.padding.make_mut()[0] = padding;
    }
    pub fn set_padding_right(&mut self, padding: f32) {
        self.padding.make_mut()[1] = padding;
    }
    pub fn set_padding_bottom(&mut self, padding: f32) {
        self.padding.make_mut()[2] = padding;
    }
    pub fn set_padding_left(&mut self, padding: f32) {
        self.padding.make_mut()[3] = padding;
    }
    pub fn padding_top(mut self, padding: f32) -> Self {
        self.set_padding_top(padding);
        self
    }
    pub fn padding_right(mut self, padding: f32) -> Self {
        self.set_padding_right(padding);
        self
    }
    pub fn padding_bottom(mut self, padding: f32) -> Self {
        self.set_padding_bottom(padding);
        self
    }
    pub fn padding_left(mut self, padding: f32) -> Self {
        self.set_padding_left(padding);
        self
    }
    pub fn set_padding(&mut self, padding: f32) {
    	self.padding.set([padding; 4]);
    }
    pub fn padding(mut self, padding: f32) -> Self {
        self.set_padding(padding);
        self
    }
}

impl<T, W: Widget<T> + Geometry> Geometry for Padding<T, W> {
    fn width(&self) -> f32 {
        let [_, right, _, left] = self.padding.get();
        self.widget.width() + left + right
    }
    fn height(&self) -> f32 {
        let [top, _, bottom, _] = self.padding.get();
        self.widget.height() + top + bottom
    }
}

impl<T, W: Widget<T>> Themeable for Padding<T, W> {
    const NAME: &'static str = "box";
    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(padding) = env.try_get(id, "padding") {
            self.set_padding(padding)
        }
        if let Some(padding) = env.try_get(id, "padding-top") {
            self.set_padding_top(padding)
        }
        if let Some(padding) = env.try_get(id, "padding-right") {
            self.set_padding_right(padding)
        };
        if let Some(padding) = env.try_get(id, "padding-bottom") {
            self.set_padding_bottom(padding)
        };
        if let Some(padding) = env.try_get(id, "padding-left") {
            self.set_padding_left(padding)
        };
    }
}

impl<T, W: Widget<T>> Guarded for Padding<T, W> {
    fn restore(&mut self) {
        self.padding.restore()
    }
    fn touched(&self) -> bool {
        self.padding.touched()
    }
}

impl<T, W: Widget<T> + Drawable> Drawable for Padding<T, W> {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        let [top, _, _, left] = self.padding.get();
        self.widget.draw(ctx, transform.pre_translate(left, top));
    }
}

impl<T, W: Widget<T>> Widget<T> for Padding<T, W> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.restore();
        let [top, _, _, left] = self.padding.get();
        self.widget.draw_scene(scene.translate(left, top), env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        if let Event::Pointer(MouseEvent { pointer, position }) = event {
            let [top, _, _, left] = self.padding.get();
            self.widget.event(
                ctx,
                MouseEvent::new(position.translate(-left, -top), pointer),
                env,
            )
        } else {
            self.widget.event(ctx, event, env)
        }
        .max(self.damage())
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.damage().max(self.widget.update(ctx, env))
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let [top, right, bottom, left] = self.padding.get();
        let (width, height) = self
            .widget
            .layout(ctx, &bc.crop(left + right, top + bottom), env)
            .into();
        Size::new(width + left + right, height + top + bottom)
    }
}

impl<T, W: Widget<T> + Scrollable> Scrollable for Padding<T, W> {
    fn forward(&mut self, step: Option<f32>) {
        self.widget.forward(step)
    }
    fn backward(&mut self, step: Option<f32>) {
        self.widget.backward(step)
    }
    fn inner_height(&self) -> f32 {
        self.widget.inner_height()
    }
    fn inner_width(&self) -> f32 {
        self.widget.inner_width()
    }
    fn orientation(&self) -> Orientation {
        self.widget.orientation()
    }
    fn position(&self) -> f32 {
        self.widget.position()
    }
}

impl<T, W: Widget<T>> Deref for Padding<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.widget
    }
}

impl<T, W: Widget<T>> DerefMut for Padding<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.widget
    }
}
