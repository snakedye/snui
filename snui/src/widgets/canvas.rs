use crate::*;

/// A widget that paints a [`Drawable`].
pub struct Canvas<P: Drawable + GeometryExt> {
    painted: P,
}

impl<P: Drawable + GeometryExt> From<P> for Canvas<P> {
    fn from(painted: P) -> Self {
        Canvas { painted }
    }
}

impl<P: Drawable + GeometryExt> Canvas<P> {
    pub fn new(painted: P) -> Self {
        Canvas { painted }
    }
}

impl<P: Drawable + GeometryExt> Geometry for Canvas<P> {
    fn width(&self) -> f32 {
        self.painted.width()
    }
    fn height(&self) -> f32 {
        self.painted.height()
    }
}

impl<P: Drawable + GeometryExt> Drawable for Canvas<P> {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        self.painted.draw(ctx, transform)
    }
}

impl<P: Drawable + GeometryExt + Guarded> Guarded for Canvas<P> {
    fn restore(&mut self) {
        self.painted.restore()
    }
    fn touched(&self) -> bool {
        self.painted.touched()
    }
}

impl<P: Drawable + GeometryExt + Themeable> Themeable for Canvas<P> {
    const NAME: &'static str = P::NAME;
    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        self.painted.theme(id, env)
    }
}

impl<T, P: Drawable + GeometryExt + Guarded> Widget<T> for Canvas<P> {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
    	self.restore();
        scene.draw(self)
    }
    fn event(&mut self, _ctx: &mut UpdateContext<T>, _event: Event, _env: &Env) -> Damage {
        self.damage()
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        self.damage()
    }
    /// The painted object size is determined by the maximum size of the layout.
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, bc: &BoxConstraints, _env: &Env) -> Size {
        self.painted
            .set_size(bc.maximum_width(), bc.maximum_height());
        self.painted.size()
    }
}
