//! Simple shapes for building widgets

pub(crate) mod circle;
pub(crate) mod rectangle;
pub(crate) mod style;

use crate::{
    scene::{Position, Texture},
    Backend, Geometry, Orientation, RelativeUnit,
};
use tiny_skia::*;

pub use circle::{BorderedCircle, Circle};
pub use rectangle::{BorderedRectangle, Rectangle};
pub use style::WidgetStyle;

/// A trait used to theme widgets.
pub trait Style: Sized {
    fn set_texture<B: Into<Texture>>(&mut self, texture: B);
    fn set_top_left_radius(&mut self, radius: RelativeUnit);
    fn set_top_right_radius(&mut self, radius: RelativeUnit);
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit);
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit);
    fn set_radius(&mut self, radius: RelativeUnit) {
        self.set_top_left_radius(radius);
        self.set_top_right_radius(radius);
        self.set_bottom_right_radius(radius);
        self.set_bottom_left_radius(radius);
    }
    fn radius(mut self, radius: RelativeUnit) -> Self {
        self.set_radius(radius);
        self
    }
    fn top_left_radius(mut self, radius: RelativeUnit) -> Self {
        self.set_top_left_radius(radius);
        self
    }
    fn top_right_radius(mut self, radius: RelativeUnit) -> Self {
        self.set_top_right_radius(radius);
        self
    }
    fn bottom_right_radius(mut self, radius: RelativeUnit) -> Self {
        self.set_bottom_right_radius(radius);
        self
    }
    fn bottom_left_radius(mut self, radius: RelativeUnit) -> Self {
        self.set_bottom_left_radius(radius);
        self
    }
    fn texture<B: Into<Texture>>(mut self, texture: B) -> Self {
        self.set_texture(texture);
        self
    }
}

pub fn stroke_path(
    path: &Path,
    texture: &Texture,
    border_width: f32,
    backend: &mut Backend,
    geometry: &impl Geometry,
    transform: Transform,
) {
    if let Backend::Pixmap { pixmap, clipmask } = backend {
        let clipmask = clipmask
            .as_ref()
            .and_then(|clipmask| (!clipmask.is_empty()).then_some(&**clipmask));
        let stroke = Stroke {
            width: border_width,
            line_cap: LineCap::Butt,
            line_join: LineJoin::Miter,
            miter_limit: 4.,
            dash: None,
        };
        match texture {
            Texture::Color(color) => {
                pixmap.stroke_path(
                    path,
                    &Paint {
                        shader: Shader::SolidColor(*color),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    &stroke,
                    transform,
                    clipmask,
                );
            }
            #[cfg(feature = "img")]
            Texture::Image(image) => {
                let sx = geometry.width() / image.width();
                let sy = geometry.height() / image.height();
                pixmap.stroke_path(
                    &path,
                    &Paint {
                        shader: Pattern::new(
                            image.pixmap(),
                            SpreadMode::Repeat,
                            FilterQuality::Bilinear,
                            1.0,
                            Transform::from_scale(sx, sy),
                        ),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    &stroke,
                    transform,
                    clipmask,
                );
            }
            Texture::RadialGradient(gradient) => {
                let region = Position::default().to_region(geometry);
                let diameter = region.width.max(region.height);
                let center = region.center();
                pixmap.stroke_path(
                    path,
                    &Paint {
                        shader: RadialGradient::new(
                            Position::default().into(),
                            Position::default().into(),
                            diameter / 2.,
                            gradient.stops.to_vec(),
                            gradient.mode,
                            Transform::identity()
                                .pre_scale(region.width / diameter, region.height / diameter)
                                .post_translate(
                                    center.x - border_width / 2.,
                                    center.y - border_width / 2.,
                                ),
                        )
                        .expect("Failed to build RadialGradient shader"),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    &stroke,
                    transform,
                    clipmask,
                );
            }
            Texture::LinearGradient(gradient) => {
                let region = Position::default().to_region(geometry);
                let start = match gradient.orientation {
                    Orientation::Horizontal => region.start(),
                    Orientation::Vertical => region.top_anchor(),
                };
                let end = match gradient.orientation {
                    Orientation::Horizontal => region.end(),
                    Orientation::Vertical => region.bottom_anchor(),
                };
                pixmap.stroke_path(
                    path,
                    &Paint {
                        shader: LinearGradient::new(
                            start.into(),
                            end.into(),
                            gradient.stops.to_vec(),
                            gradient.mode,
                            Transform::from_scale(transform.sx, transform.sy),
                        )
                        .expect("Failed to build LinearGradient shader"),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    &stroke,
                    transform,
                    clipmask,
                );
            }
            _ => {}
        }
    }
}

pub fn fill_path(
    path: &Path,
    texture: &Texture,
    backend: &mut Backend,
    geometry: &impl Geometry,
    transform: Transform,
) {
    if let Backend::Pixmap { pixmap, clipmask } = backend {
        let clipmask = clipmask
            .as_ref()
            .and_then(|clipmask| (!clipmask.is_empty()).then_some(&**clipmask));
        match &texture {
            Texture::Color(color) => {
                pixmap.fill_path(
                    path,
                    &Paint {
                        shader: Shader::SolidColor(*color),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    FillRule::Winding,
                    transform,
                    clipmask,
                );
            }
            #[cfg(feature = "img")]
            Texture::Image(image) => {
                let sx = geometry.width() / image.width();
                let sy = geometry.height() / image.height();
                pixmap.fill_path(
                    &path,
                    &Paint {
                        shader: Pattern::new(
                            image.pixmap(),
                            SpreadMode::Repeat,
                            FilterQuality::Bilinear,
                            1.0,
                            Transform::from_scale(sx, sy),
                        ),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    FillRule::Winding,
                    transform,
                    clipmask,
                );
            }
            Texture::RadialGradient(gradient) => {
                let region = Position::default().to_region(geometry);
                let diameter = region.width.max(region.height);
                let center = region.center();
                pixmap.fill_path(
                    path,
                    &Paint {
                        shader: RadialGradient::new(
                            Position::default().into(),
                            Position::default().into(),
                            diameter / 2.,
                            gradient.stops.to_vec(),
                            gradient.mode,
                            Transform::identity()
                                .pre_scale(region.width / diameter, region.height / diameter)
                                .post_translate(center.x, center.y),
                        )
                        .expect("Failed to build RadialGradient shader"),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    FillRule::Winding,
                    transform,
                    clipmask,
                );
            }
            Texture::LinearGradient(gradient) => {
                let region = Position::default().to_region(geometry);
                let start = match gradient.orientation {
                    Orientation::Horizontal => region.start(),
                    Orientation::Vertical => region.top_anchor(),
                };
                let end = match gradient.orientation {
                    Orientation::Horizontal => region.end(),
                    Orientation::Vertical => region.bottom_anchor(),
                };
                pixmap.fill_path(
                    path,
                    &Paint {
                        shader: LinearGradient::new(
                            start.into(),
                            end.into(),
                            gradient.stops.to_vec(),
                            gradient.mode,
                            Transform::from_scale(transform.sx, transform.sy),
                        )
                        .expect("Failed to build LinearGradient shader"),
                        blend_mode: BlendMode::SourceOver,
                        anti_alias: true,
                        force_hq_pipeline: false,
                    },
                    FillRule::Winding,
                    transform,
                    clipmask,
                );
            }
            _ => {}
        }
    }
}
