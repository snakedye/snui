use super::{BorderedRectangle, Rectangle, Style};
use crate::scene::Texture;
use crate::*;
use snui_derive::Guarded;

/// A drawable circle.
#[derive(Clone, Debug, PartialEq, Guarded)]
pub struct Circle {
    inner: Rectangle,
}

impl AsRef<Texture> for Circle {
    fn as_ref(&self) -> &Texture {
        self.inner.as_ref()
    }
}

impl Default for Circle {
    fn default() -> Self {
        Circle::new(0.)
    }
}

impl Circle {
    pub fn new(radius: f32) -> Circle {
        Circle {
            inner: Rectangle::new(2. * radius, 2. * radius).radius(RelativeUnit::Percent(50.)),
        }
    }
    pub fn set_radius(&mut self, radius: f32) {
        self.inner.set_size(2. * radius, 2. * radius)
    }
    pub fn set_texture(&mut self, texture: impl Into<Texture>) {
        self.inner.set_texture(texture)
    }
    pub fn texture(mut self, texture: impl Into<Texture>) -> Self {
        self.inner.set_texture(texture.into());
        self
    }
}

impl Geometry for Circle {
    fn width(&self) -> f32 {
        self.inner.width()
    }
    fn height(&self) -> f32 {
        self.inner.height()
    }
    fn contains(&self, position: &Position) -> bool {
        let radius = self.width() / 2.;
        let Position { x, y } = position.translate(-radius, -radius);
        x.powi(2) + y.powi(2) < radius.powi(2)
    }
}

impl Drawable for Circle {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        self.inner.draw(ctx, transform);
    }
}

impl Themeable for Circle {
    const NAME: &'static str = "circle";
    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(texture) = env
            .try_get::<Texture>(id, "texture")
            .or_else(|| env.try_get::<Texture>(id, "color"))
        {
            self.set_texture(texture);
        }
    }
}

impl<T> Widget<T> for Circle {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        Widget::<T>::draw_scene(&mut self.inner, scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        Widget::<T>::event(&mut self.inner, ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        Widget::<T>::update(&mut self.inner, ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        Widget::<T>::layout(&mut self.inner, ctx, bc, env)
    }
}

/// A drawable bordered circle.
#[derive(Clone, Debug, PartialEq, Guarded)]
pub struct BorderedCircle {
    inner: BorderedRectangle,
}

impl AsRef<Texture> for BorderedCircle {
    fn as_ref(&self) -> &Texture {
        &color::TRANSPARENT
    }
}

impl Default for BorderedCircle {
    fn default() -> Self {
        BorderedCircle::new(0.)
    }
}

impl BorderedCircle {
    pub fn new(radius: f32) -> BorderedCircle {
        BorderedCircle {
            inner: BorderedRectangle::new(2. * radius, 2. * radius)
                .radius(RelativeUnit::Percent(50.)),
        }
    }
    pub fn set_radius(&mut self, radius: f32) {
        self.inner.set_size(2. * radius, 2. * radius)
    }
    pub fn set_texture(&mut self, texture: impl Into<Texture>) {
        self.inner.set_texture(texture)
    }
    pub fn texture(mut self, texture: impl Into<Texture>) -> Self {
        self.inner.set_texture(texture.into());
        self
    }
    pub fn border_width(mut self, width: f32) -> Self {
        self.set_border_width(width);
        self
    }
    pub fn set_border_width(&mut self, width: f32) {
        self.inner.set_border_width(width);
    }
}

impl Geometry for BorderedCircle {
    fn width(&self) -> f32 {
        self.inner.width()
    }
    fn height(&self) -> f32 {
        self.inner.height()
    }
    fn contains(&self, position: &Position) -> bool {
        let radius = self.width() / 2.;
        let Position { x, y } = position.translate(-radius, -radius);
        x.powi(2) + y.powi(2) < radius.powi(2)
    }
}

impl Drawable for BorderedCircle {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        self.inner.draw(ctx, transform);
    }
}

impl Themeable for BorderedCircle {
    const NAME: &'static str = "circle";
    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(texture) = env.try_get::<Texture>(id, "border-texture") {
            self.set_texture(texture);
        }
        if let Some(width) = env.try_get(id, "border-width") {
            self.set_border_width(width);
        }
    }
}

impl<T> Widget<T> for BorderedCircle {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        Widget::<T>::draw_scene(&mut self.inner, scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        Widget::<T>::event(&mut self.inner, ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        Widget::<T>::update(&mut self.inner, ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        Widget::<T>::layout(&mut self.inner, ctx, bc, env)
    }
}
