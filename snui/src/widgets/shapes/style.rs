use crate::environment::*;
use crate::scene::*;
use crate::widgets::*;
use crate::*;
use std::ops::{Deref, DerefMut};

#[derive(Debug)]
/// A widget that adds styling to a widget.
pub struct WidgetStyle<T, W: Widget<T>> {
    background: Rectangle,
    border: BorderedRectangle,
    inner: Positioner<Padding<T, W>>,
}

impl<T, W: Widget<T>> WidgetStyle<T, W> {
    pub fn new(inner: W) -> Self {
        WidgetStyle {
            background: Rectangle::default(),
            border: BorderedRectangle::default(),
            inner: Positioner::new(Padding::new(inner)),
        }
    }
    /// Add padding at the top of this widget.
    pub fn padding_top(mut self, padding: f32) -> Self {
        self.inner.set_padding_top(padding);
        self
    }
    /// Add padding at the right of this widget.
    pub fn padding_right(mut self, padding: f32) -> Self {
        self.inner.set_padding_right(padding);
        self
    }
    /// Add padding beneath this widget.
    pub fn padding_bottom(mut self, padding: f32) -> Self {
        self.inner.set_padding_bottom(padding);
        self
    }
    /// Add padding at the left of this widget.
    pub fn padding_left(mut self, padding: f32) -> Self {
        self.inner.set_padding_left(padding);
        self
    }
    /// Add padding around this widget.
    pub fn set_padding(&mut self, padding: f32) {
        self.inner.set_padding_top(padding);
        self.inner.set_padding_right(padding);
        self.inner.set_padding_bottom(padding);
        self.inner.set_padding_left(padding);
    }
    /// Add padding around this widget.
    pub fn padding(mut self, padding: f32) -> Self {
        self.set_padding(padding);
        self
    }
    pub fn set_border_width(&mut self, width: f32) {
        self.inner.set_position(width, width);
        // To set the damage inside the Positioner to false.
        self.inner.set_position(width, width);
        self.border.set_border_width(width)
    }
    /// Add borders around this widget.
    pub fn set_border(&mut self, texture: impl Into<Texture>, width: f32) {
        self.set_border_texture(texture);
        self.set_border_width(width);
    }
    /// Set the width of the borders around this widget.
    pub fn border_width(mut self, width: f32) -> Self {
        self.set_border_width(width);
        self
    }
    /// Set the [`Texture`] of the borders around this widget.
    pub fn set_border_texture(&mut self, texture: impl Into<Texture>) {
        self.border.set_texture(texture);
    }
    /// Add borders around this widget.
    pub fn border(mut self, texture: impl Into<Texture>, width: f32) -> Self {
        self.set_border(texture, width);
        self
    }
}

impl<T, W: Widget<T>> Themeable for WidgetStyle<T, W> {
    const NAME: &'static str = "style";

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(radius) = env.try_get(id, "radius") {
            self.set_radius(radius);
        }

        if let Some(radius) = env.try_get(id, "top-left-radius") {
            self.set_top_left_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "top-right-radius") {
            self.set_top_right_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "bottom-left-radius") {
            self.set_bottom_left_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "bottom-right-radius") {
            self.set_bottom_right_radius(radius)
        }

        self.inner.theme(id, env);

        if let Some(texture) = env.try_get::<Texture>(id, "background") {
            self.set_texture(texture)
        }
        if let Some(texture) = env.try_get::<Texture>(id, "border-texture") {
            self.set_border_texture(texture)
        }
        if let Some(width) = env.try_get(id, "border-width") {
            self.set_border_width(width)
        }
    }
}

impl<T, W: Widget<T>> Guarded for WidgetStyle<T, W> {
    fn restore(&mut self) {
        self.border.inner.restore();
        self.background.inner.restore()
    }
    fn touched(&self) -> bool {
        self.border.inner.touched() || self.background.inner.touched()
    }
}

impl<T, W: Widget<T>> Geometry for WidgetStyle<T, W> {
    fn width(&self) -> f32 {
        self.background.width().max(self.border.width())
    }
    fn height(&self) -> f32 {
        self.background.height().max(self.border.height())
    }
}

impl<T, W: Widget<T>> Style for WidgetStyle<T, W> {
    fn set_radius(&mut self, radius: RelativeUnit) {
        self.background.set_radius(radius);
        self.border.set_radius(radius);
    }
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.background.set_top_left_radius(radius);
        self.border.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.background.set_top_right_radius(radius);
        self.border.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.background.set_bottom_right_radius(radius);
        self.border.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.background.set_bottom_left_radius(radius);
        self.border.set_bottom_left_radius(radius);
    }
    fn set_texture<B: Into<Texture>>(&mut self, texture: B) {
        self.background.set_texture(texture);
    }
}

impl<T, W: Widget<T>> Widget<T> for WidgetStyle<T, W> {
    fn draw_scene(&mut self, mut scene: Scene, env: &Env) {
        if self.reset() {
            let position = scene.position();
            scene = scene.damage(position.to_region(self));
        }
        self.inner.draw_scene(
            scene
                .apply_background(&self.border)
                .translate(
                    self.border.inner.border_width,
                    self.border.inner.border_width,
                )
                .apply_background(&self.background)
                .translate(
                    -self.border.inner.border_width,
                    -self.border.inner.border_width,
                ),
            env,
        );
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.inner.update(ctx, env).max(self.damage())
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.inner.event(ctx, event, env).max(self.damage())
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let border_width = self.border.inner.border_width;
        let size = self
            .inner
            .layout(ctx, &bc.crop(border_width * 2., border_width * 2.), env)
            .round();
        self.background.set_size(size.width, size.height);
        self.border.set_size(
            size.width + border_width * 2.,
            size.height + border_width * 2.,
        );
        self.size()
    }
}

impl<T, W: Drawable + Widget<T>> Drawable for WidgetStyle<T, W> {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        let Position { x, y } = self.inner.position();
        self.border.draw(ctx, transform);
        self.background.draw(ctx, transform.pre_translate(x, y));
        self.inner.draw(ctx, transform.pre_translate(x, y));
    }
}

impl<T, W: Widget<T>> Deref for WidgetStyle<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        self.inner.deref()
    }
}

impl<T, W: Widget<T>> DerefMut for WidgetStyle<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.inner.deref_mut()
    }
}
