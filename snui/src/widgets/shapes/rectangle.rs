use crate::scene::Texture;
use crate::widgets::shapes::Style;
use crate::*;
use snui_derive::Guarded;
use tiny_skia::*;

/// A rectangle.
#[derive(Clone, Debug, PartialEq, Default, Guarded)]
pub struct Rectangle {
    pub(crate) inner: Guard<InnerRectangle>,
}

#[derive(Clone, Debug, PartialEq, Default)]
pub(crate) struct InnerRectangle {
    pub(crate) width: f32,
    pub(crate) height: f32,
    pub(crate) texture: Texture,
    pub(crate) radius: [RelativeUnit; 4],
}

impl AsRef<Texture> for Rectangle {
    fn as_ref(&self) -> &Texture {
        &self.inner.texture
    }
}

impl Rectangle {
    pub fn new(width: f32, height: f32) -> Self {
        Self {
            inner: MutGuard::from(InnerRectangle::default()),
        }
        .with_size(width, height)
    }
    pub fn as_path(&self, pb: PathBuilder) -> Option<Path> {
        let radius = self.inner.radius.map(|r| r.as_abs(self.min_side()));
        Self::path(pb, self.inner.width, self.inner.height, radius)
    }
    pub fn path(mut pb: PathBuilder, width: f32, height: f32, radius: [f32; 4]) -> Option<Path> {
        let (x, y) = (0., 0.);
        let mut cursor = Position::new(x, y);

        let [tl, tr, br, bl] = radius;

        // Positioning the cursor
        cursor.y += tl;
        pb.move_to(cursor.x, cursor.y);

        // Drawing the outline
        pb.cubic_to(
            cursor.x,
            cursor.y,
            cursor.x,
            cursor.y - tl,
            {
                cursor.x += tl;
                cursor.x
            },
            {
                cursor.y -= tl;
                cursor.y
            },
        );
        pb.line_to(
            {
                cursor.x = x + width - tr;
                cursor.x
            },
            cursor.y,
        );
        pb.cubic_to(
            cursor.x,
            cursor.y,
            cursor.x + tr,
            cursor.y,
            {
                cursor.x += tr;
                cursor.x
            },
            {
                cursor.y += tr;
                cursor.y
            },
        );
        pb.line_to(cursor.x, {
            cursor.y = y + height - br;
            cursor.y
        });
        pb.cubic_to(
            cursor.x,
            cursor.y,
            cursor.x,
            cursor.y + br,
            {
                cursor.x -= br;
                cursor.x
            },
            {
                cursor.y += br;
                cursor.y
            },
        );
        pb.line_to(
            {
                cursor.x = x + bl;
                cursor.x
            },
            cursor.y,
        );
        pb.cubic_to(
            cursor.x,
            cursor.y,
            cursor.x - bl,
            cursor.y,
            {
                cursor.x -= bl;
                cursor.x
            },
            {
                cursor.y -= bl;
                cursor.y
            },
        );

        // Closing path
        pb.close();

        pb.finish()
    }
    fn minimum_height(&self) -> f32 {
        let min = self.min_side();
        self.inner.radius[0]
            .as_abs(min)
            .max(self.inner.radius[2].as_abs(min))
    }
    fn minimum_width(&self) -> f32 {
        let min = self.min_side();
        self.inner.radius[1]
            .as_abs(min)
            .max(self.inner.radius[3].as_abs(min))
    }
    pub(crate) fn min_side(&self) -> f32 {
        self.inner.width.min(self.inner.height)
    }
}

impl Geometry for Rectangle {
    fn width(&self) -> f32 {
        self.inner.width
    }
    fn height(&self) -> f32 {
        self.inner.height
    }
}

impl GeometryExt for Rectangle {
    fn set_width(&mut self, width: f32) {
        self.inner.make_mut().width = width.round().max(self.minimum_width());
    }
    fn set_height(&mut self, height: f32) {
        self.inner.make_mut().height = height.round().max(self.minimum_height());
    }
}

impl Drawable for Rectangle {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        ctx.draw(|ctx, pb| {
            let radius = self.inner.radius.map(|r| r.as_abs(self.min_side()));
            let path = Rectangle::path(pb, self.inner.width, self.inner.height, radius).unwrap();
            super::fill_path(&path, &self.inner.texture, ctx.deref_mut(), self, transform);
            path
        })
    }
}

impl Style for InnerRectangle {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.radius[0] = radius;
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.radius[1] = radius;
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.radius[2] = radius;
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.radius[3] = radius;
    }
    fn set_texture<B: Into<Texture>>(&mut self, background: B) {
        self.texture = background.into();
    }
}

impl Style for Rectangle {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[0] = radius;
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[1] = radius;
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[2] = radius;
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[3] = radius;
    }
    fn set_texture<B: Into<Texture>>(&mut self, background: B) {
        set!(self.inner, texture = background.into());
    }
}

impl Themeable for Rectangle {
    const NAME: &'static str = "rectangle";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        let mut rect = self.inner.make_mut();
        if let Some(radius) = env.try_get(list, "radius") {
            rect.radius = [radius; 4];
        }
        if let Some(radius) = env.try_get(list, "top-left-radius") {
            rect.set_top_left_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "top-right-radius") {
            rect.set_top_right_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "bottom-right-radius") {
            rect.set_bottom_right_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "bottom-left-radius") {
            rect.set_bottom_left_radius(radius);
        }
        if let Some(texture) = env
            .try_get::<Texture>(list, "texture")
            .or_else(|| env.try_get::<Texture>(list, "color"))
        {
            rect.set_texture(texture)
        }
    }
}

impl<T> Widget<T> for Rectangle {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        self.inner.restore();
        scene.draw(self)
    }
    fn event(&mut self, _ctx: &mut UpdateContext<T>, _event: Event, _env: &Env) -> Damage {
        self.inner.damage()
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        self.inner.damage()
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, bc: &BoxConstraints, _env: &Env) -> Size {
        set!(self.inner, width = bc.maximum_width());
        set!(self.inner, height = bc.maximum_height());
        bc.maximum()
    }
}

/// A rectangle.
#[derive(Clone, Debug, PartialEq, Default, Guarded)]
pub struct BorderedRectangle {
    pub(crate) inner: Guard<InnerBorderedRectangle>,
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct InnerBorderedRectangle {
    pub(crate) width: f32,
    pub(crate) height: f32,
    pub(crate) texture: Texture,
    pub(crate) border_width: f32,
    pub(crate) radius: [RelativeUnit; 4],
}

impl AsRef<Texture> for BorderedRectangle {
    fn as_ref(&self) -> &Texture {
        &Texture::Transparent
    }
}

impl BorderedRectangle {
    pub fn new(width: f32, height: f32) -> Self {
        Self {
            inner: MutGuard::from(InnerBorderedRectangle::default()),
        }
        .with_size(width, height)
    }
    pub fn set_border_width(&mut self, border_width: f32) {
        set!(self.inner, border_width = border_width);
    }
    pub fn border_width(mut self, border_width: f32) -> Self {
        self.set_border_width(border_width);
        self
    }
    fn minimum_height(&self) -> f32 {
        let min = self.min_side();
        self.inner.radius[0]
            .as_abs(min)
            .max(self.inner.radius[2].as_abs(min))
    }
    fn minimum_width(&self) -> f32 {
        let min = self.min_side();
        self.inner.radius[1]
            .as_abs(min)
            .max(self.inner.radius[3].as_abs(min))
    }
    pub(crate) fn min_side(&self) -> f32 {
        self.inner.width.min(self.inner.height) + self.inner.border_width
    }
}

impl Geometry for BorderedRectangle {
    fn width(&self) -> f32 {
        self.inner.width + 2. * self.inner.border_width
    }
    fn height(&self) -> f32 {
        self.inner.height + 2. * self.inner.border_width
    }
}

impl GeometryExt for BorderedRectangle {
    fn set_width(&mut self, width: f32) {
        self.inner.make_mut().width =
            (width.round() - 2. * self.inner.border_width).max(self.minimum_width());
    }
    fn set_height(&mut self, height: f32) {
        self.inner.make_mut().height =
            (height.round() - 2. * self.inner.border_width).max(self.minimum_height());
    }
}

impl Drawable for BorderedRectangle {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        ctx.draw(|ctx, pb| {
            let radius = self.inner.radius.map(|r| r.as_abs(self.min_side()));
            let path = Rectangle::path(
                pb,
                self.inner.width + self.inner.border_width,
                self.inner.height + self.inner.border_width,
                radius,
            )
            .unwrap();
            super::stroke_path(
                &path,
                &self.inner.texture,
                self.inner.border_width,
                ctx.deref_mut(),
                self,
                transform.pre_translate(self.inner.border_width / 2., self.inner.border_width / 2.),
            );
            path
        })
    }
}

impl Style for InnerBorderedRectangle {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.radius[0] = radius;
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.radius[1] = radius;
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.radius[2] = radius;
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.radius[3] = radius;
    }
    fn set_texture<B: Into<Texture>>(&mut self, background: B) {
        self.texture = background.into();
    }
}

impl Style for BorderedRectangle {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[0] = radius;
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[1] = radius;
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[2] = radius;
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.make_mut().radius[3] = radius;
    }
    fn set_texture<B: Into<Texture>>(&mut self, background: B) {
        set!(self.inner, texture = background.into());
    }
}

impl Themeable for BorderedRectangle {
    const NAME: &'static str = "rectangle";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        let mut rect = self.inner.make_mut();
        if let Some(radius) = env.try_get(list, "radius") {
            rect.radius = [radius; 4];
        }
        if let Some(radius) = env.try_get(list, "top-left-radius") {
            rect.set_top_left_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "top-right-radius") {
            rect.set_top_right_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "bottom-right-radius") {
            rect.set_bottom_right_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "bottom-left-radius") {
            rect.set_bottom_left_radius(radius);
        }
        if let Some(texture) = env
            .try_get::<Texture>(list, "texture")
            .or_else(|| env.try_get::<Texture>(list, "color"))
        {
            rect.set_texture(texture)
        }
        if let Some(width) = env.try_get(list, "border-width") {
            rect.border_width = width;
        }
    }
}

impl<T> Widget<T> for BorderedRectangle {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        self.inner.restore();
        scene.draw(self)
    }
    fn event(&mut self, _ctx: &mut UpdateContext<T>, _event: Event, _env: &Env) -> Damage {
        self.inner.damage()
    }
    fn update(&mut self, _ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        self.inner.damage()
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, bc: &BoxConstraints, _env: &Env) -> Size {
        set!(self.inner, width = bc.maximum_width());
        set!(self.inner, height = bc.maximum_height());
        bc.maximum()
    }
}
