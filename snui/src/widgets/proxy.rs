use crate::scene::Region;
use crate::*;
use std::marker::PhantomData;

#[derive(Debug, Clone, Copy)]
enum PointerFocus {
    None,
    Leave,
    Enter,
}

/// A proxy for a widget.
///
/// A widget proxy contains additional state needed to appropriately layout, damage the scene and propagate events.
///
/// It filters events, updates, layouts and draws to avoid unecessary computation.
#[derive(Debug)]
pub struct Proxy<T, W: Widget<T>> {
    size: Guard<Size>,
    serial: u32,
    damage: Damage,
    update: bool,
    pointer_focus: PointerFocus,
    keyboard_focus: bool,
    region: Option<Region>,
    bc: BoxConstraints,
    inner: W,
    _data: PhantomData<T>,
}

impl<T, W: Widget<T>> Proxy<T, W> {
    /// Create a new proxy.
    pub fn new(inner: W) -> Self {
        Proxy {
            inner,
            serial: 0,
            update: true,
            bc: BoxConstraints::default(),
            pointer_focus: PointerFocus::None,
            keyboard_focus: false,
            size: Guard::from(Size::default()),
            region: None,
            damage: Damage::Partial,
            _data: PhantomData,
        }
    }
    /// Increment the damage.
    pub fn upgrade(&mut self) {
        self.damage = self.damage.max(Damage::Partial);
    }
    /// The size of the proxied widget since the last layout.
    pub fn size(&self) -> Size {
        self.size.get()
    }
    /// The [`Region`] previously occupied by the widget.
    pub fn region(&self) -> Option<Region> {
        self.region
    }
}

impl<T, W: Widget<T>> Deref for Proxy<T, W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W: Widget<T>> DerefMut for Proxy<T, W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.upgrade();
        &mut self.inner
    }
}

impl<T, W: Widget<T>> Geometry for Proxy<T, W> {
    fn width(&self) -> f32 {
        self.size.width
    }
    fn height(&self) -> f32 {
        self.size.height
    }
}

impl<T, W: Themeable + Widget<T>> Themeable for Proxy<T, W> {
    const NAME: &'static str = W::NAME;
    fn theme(&mut self, id: utils::List<Id>, theme: &dyn Environment) {
        self.inner.theme(id, theme);
    }
}

impl<T, W: Widget<T>> Guarded for Proxy<T, W> {
    fn restore(&mut self) {
        self.size.restore();
        self.damage = Damage::None;
    }
    fn touched(&self) -> bool {
        self.damage.is_partial() | self.size.touched()
    }
}

impl<T, W: Widget<T>> Widget<T> for Proxy<T, W> {
    fn draw_scene(&mut self, mut scene: Scene, env: &Env) {
        let region = scene.position().to_region(&self.size.get());
        if self.reset() {
            if self.region == Some(region) {
                self.inner.draw_scene(scene.clamp(&region), env)
            } else {
                self.inner.draw_scene(
                    scene
                        .clamp(self.region.as_ref().unwrap_or(&region))
                        .damage(region),
                    env,
                );
            }
        } else if scene.damaged() {
            self.inner
                .draw_scene(scene.clamp(self.region.as_ref().unwrap_or(&region)), env)
        } else if self.region != Some(region) {
            self.inner.draw_scene(
                scene.clamp(self.region.as_ref().unwrap()).damage(region),
                env,
            );
        }
        self.region = Some(region);
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        if self.update {
            self.damage = self.damage.max(self.inner.update(&mut ctx.borrow(), env));
            self.update = ctx.ready();
        }
        self.damage
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let mut ctx = ctx.borrow();
        self.damage = self.damage.max(match event {
            Event::Pointer(MouseEvent {
                ref position,
                ref pointer,
            }) => match pointer {
                Pointer::Leave => {
                    if !matches!(self.pointer_focus, PointerFocus::None) {
                        self.pointer_focus = PointerFocus::None;
                        self.inner.event(&mut ctx, event, env)
                    } else {
                        Damage::None
                    }
                }
                _ => {
                    if self.size.contains(position) {
                        if let Some(serial) = pointer.serial() {
                            self.serial = serial;
                        }
                        if let PointerFocus::Enter = self.pointer_focus {
                            self.inner.event(&mut ctx, event, env)
                        } else {
                            self.pointer_focus = PointerFocus::Enter;
                            self.inner.event(
                                &mut ctx,
                                MouseEvent::new(*position, Pointer::Enter),
                                env,
                            )
                        }
                    } else if let PointerFocus::Enter = self.pointer_focus {
                        if let Some(serial) = pointer.serial() {
                            self.serial = serial;
                        }
                        let res = self.inner.event(&mut ctx, event, env);
                        if res.is_none() {
                            self.pointer_focus = PointerFocus::Leave;
                        }
                        res
                    } else if let PointerFocus::Leave = self.pointer_focus {
                        if let Some(serial) = pointer.serial() {
                            self.serial = serial;
                        }
                        self.pointer_focus = PointerFocus::None;
                        self.inner
                            .event(&mut ctx, MouseEvent::new(*position, Pointer::Leave), env)
                    } else if let Some(serial) = pointer.serial() {
                        (serial == self.serial + 1)
                            .then(|| {
                                self.event(&mut ctx, Event::Focus(Focus::pointer_unfocus()), env)
                            })
                            .unwrap_or_default()
                    } else {
                        Damage::None
                    }
                }
            },
            Event::Focus(focus) => {
                if focus.is_pointer_unfocus() {
                    self.pointer_focus = PointerFocus::None;
                }
                if let Some(serial) = focus.serial {
                    (serial <= self.serial + 2)
                        .then(|| {
                            self.serial = serial;
                            self.inner.event(&mut ctx, event, env)
                        })
                        .unwrap_or_default()
                } else {
                    self.inner.event(&mut ctx, event, env)
                }
            }
            Event::Action(serial, _) => (self.serial == serial)
                .then(|| self.inner.event(&mut ctx, event, env))
                .unwrap_or_default(),
            Event::Keyboard(ref key) => {
                self.keyboard_focus = key.serial <= self.serial + 2;
                self.keyboard_focus
                    .then(|| {
                        self.serial = key.serial;
                        self.inner.event(&mut ctx, event, env)
                    })
                    .unwrap_or_default()
            }
            _ => self.inner.event(&mut ctx, event, env),
        });
        self.update |= ctx.ready();
        self.damage
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        if self.damage.is_partial() || self.bc.ne(bc) || ctx.force {
            self.bc = *bc;
            self.size.set(self.inner.layout(ctx, bc, env).ceil());
        }
        self.size.get()
    }
}
