use crate::{widgets::*, *};

/// A widget that layout horizontally a trio of widget.
///
/// The widget at the center will occupy as much space as possible.
pub struct Center<T, S, M, E>
where
    S: Widget<T>,
    M: Widget<T>,
    E: Widget<T>,
{
    a: Positioner<Proxy<T, S>>,
    b: Positioner<Proxy<T, M>>,
    c: Positioner<Proxy<T, E>>,
    orientation: Orientation,
}

impl<T, S, M, E> Center<T, S, M, E>
where
    S: Widget<T>,
    M: Widget<T>,
    E: Widget<T>,
{
    pub fn row(a: S, b: M, c: E) -> Self {
        Self {
            a: child(a),
            b: child(b),
            c: child(c),
            orientation: Orientation::Vertical,
        }
    }
    pub fn column(a: S, b: M, c: E) -> Self {
        Self {
            a: child(a),
            b: child(b),
            c: child(c),
            orientation: Orientation::Horizontal,
        }
    }
}

impl<T, S, M, E> Widget<T> for Center<T, S, M, E>
where
    S: Widget<T>,
    M: Widget<T>,
    E: Widget<T>,
{
    fn draw_scene(&mut self, mut scene: scene::Scene, env: &Env) {
        self.a.draw_scene(scene.borrow(), env);
        self.b.draw_scene(scene.borrow(), env);
        self.c.draw_scene(scene.borrow(), env);
    }

    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.a
            .event(ctx, event, env)
            .max(self.b.event(ctx, event, env))
            .max(self.c.event(ctx, event, env))
    }

    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.a
            .update(ctx, env)
            .max(self.b.update(ctx, env))
            .max(self.c.update(ctx, env))
    }

    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let sizeb = self.b.layout(ctx, bc, env);
        match self.orientation {
            Orientation::Horizontal => {
                let sizea = self.a.layout(
                    ctx,
                    &bc.with_max((bc.maximum_width() - sizeb.width) / 2., bc.maximum_height()),
                    env,
                );
                let sizec = self.c.layout(
                    ctx,
                    &bc.with_max(
                        bc.maximum_width() - sizea.width - sizeb.width,
                        bc.maximum_height(),
                    ),
                    env,
                );

                self.b.set_position(sizea.width, 0.);
                self.c.set_position(
                    (bc.maximum_width() - sizec.width).max(sizea.width + sizeb.width),
                    0.,
                );

                Size::new(
                    (sizea.width + sizeb.width + sizec.width).max(bc.maximum_width()),
                    sizea.height.max(sizeb.height).max(sizec.height),
                )
            }
            Orientation::Vertical => {
                let sizea = self.a.layout(
                    ctx,
                    &bc.with_max(
                        bc.maximum_width(),
                        (bc.maximum_height() - sizeb.height) / 2.,
                    ),
                    env,
                );
                let sizec = self.c.layout(
                    ctx,
                    &bc.with_max(
                        bc.maximum_width(),
                        bc.maximum_height() - sizea.height - sizeb.height,
                    ),
                    env,
                );

                self.b.set_position(0., sizea.height);
                self.c.set_position(
                    0.,
                    (bc.maximum_height() - sizec.height).max(sizea.height + sizeb.height),
                );

                Size::new(
                    sizea.width.max(sizeb.width).max(sizec.width),
                    (sizea.height + sizeb.height + sizec.height).max(bc.maximum_height()),
                )
            }
        }
    }
}

impl<T, S, M, E> Themeable for Center<T, S, M, E>
where
    S: Widget<T> + Themeable,
    M: Widget<T>,
    E: Widget<T> + Themeable,
{
    const NAME: &'static str = "boxed-list";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        let (first, last) = match self.orientation {
            Orientation::Horizontal => ("left", "right"),
            Orientation::Vertical => ("top", "bottom"),
        };
        self.a.theme(list.push(Id::new(S::NAME, Some(first))), env);
        self.c.theme(list.push(Id::new(E::NAME, Some(last))), env);
    }
}
