//! Layout widgets that supports interactive resize.

use super::*;
use crate::widgets::Spacer;
use crate::*;
use scene::Scene;

/// An horizontal layout.
pub struct Row<T, W: Widget<T> = Box<dyn Widget<T>>> {
    children: Vec<Positioner<Proxy<T, W>>>,
}

impl<T, W: Widget<T>> Row<T, W> {
    /// Creates a widget container.
    ///
    /// Widgets are layed out in a column.
    pub fn new() -> Self {
        Self {
            children: Vec::new(),
        }
    }
    /// A builder style variant of `add`.
    pub fn with(mut self, widget: W) -> Self {
        self.add(widget);
        self
    }
    /// Add a widget to the layout.
    pub fn add(&mut self, widget: W) {
        self.children.push(child(widget))
    }
}

impl<T, W: Widget<T>> Widget<T> for Row<T, W> {
    fn draw_scene(&mut self, mut scene: Scene, env: &Env) {
        for widget in &mut self.children {
            widget.draw_scene(scene.borrow(), env)
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.children
            .iter_mut()
            .map(|widget| widget.event(ctx, event, env))
            .max()
            .unwrap_or_default()
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.children
            .iter_mut()
            .map(|widget| widget.update(ctx, env))
            .max()
            .unwrap_or_default()
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let len = self.children.len();
        RowIter::new(self.children.iter_mut(), ctx, bc, env)
            .with_len(len)
            .reduce(|acc, region| acc.merge(&region))
            .unwrap_or_default()
            .size()
    }
}

impl<T, W: Widget<T> + Themeable> Themeable for Row<T, W> {
    const NAME: &'static str = "boxed-list";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(item) = self.children.first_mut() {
            let id = item.id(Some("top"));
            item.theme(list.push(id), env);
        }
        if let Some(item) = self.children.last_mut() {
            let id = item.id(Some("bottom"));
            item.theme(list.push(id), env);
        }
    }
}

impl<T, W: Widget<T>> FromIterator<W> for Row<T, W> {
    fn from_iter<I: IntoIterator<Item = W>>(iter: I) -> Self {
        let mut this = Row::new();
        for widget in iter {
            this.children.push(child(widget));
        }
        this
    }
}

impl<T: 'static> Row<T, Box<dyn Widget<T>>> {
    /// Add any widget to the layout.
    pub fn add_child<W: Widget<T> + 'static>(&mut self, widget: W) {
        self.children.push(child(Box::new(widget)));
    }
    /// A builder style variant of `add_child`.
    pub fn with_child<W: Widget<T> + 'static>(mut self, widget: W) -> Self {
        self.children.push(child(Box::new(widget)));
        self
    }
    /// Insert a spacer into the [`struct@Row`].
    pub fn with_spacing(self, size: f32) -> Self {
        self.with_child(Spacer::vertical(size))
    }
}

/// A vertical layout.
pub struct Column<T, W: Widget<T> = Box<dyn Widget<T>>> {
    children: Vec<Positioner<Proxy<T, W>>>,
}

impl<T, W: Widget<T>> Column<T, W> {
    /// Creates a widget container.
    ///
    /// Widgets are layed out in a column.
    pub fn new() -> Self {
        Self {
            children: Vec::new(),
        }
    }
    /// A builder style variant of `add`.
    pub fn with(mut self, widget: W) -> Self {
        self.add(widget);
        self
    }
    /// Add a widget to the layout.
    pub fn add(&mut self, widget: W) {
        self.children.push(child(widget))
    }
}

impl<T, W: Widget<T>> Widget<T> for Column<T, W> {
    fn draw_scene(&mut self, mut scene: Scene, env: &Env) {
        for widget in &mut self.children {
            widget.draw_scene(scene.borrow(), env)
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.children
            .iter_mut()
            .map(|widget| widget.event(ctx, event, env))
            .max()
            .unwrap_or_default()
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.children
            .iter_mut()
            .map(|widget| widget.update(ctx, env))
            .max()
            .unwrap_or_default()
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let len = self.children.len();
        ColumnIter::new(self.children.iter_mut(), ctx, bc, env)
            .with_len(len)
            .reduce(|acc, region| acc.merge(&region))
            .unwrap_or_default()
            .size()
    }
}

impl<T: 'static> Column<T, Box<dyn Widget<T>>> {
    /// Add any widget to the layout.
    pub fn add_child<W: Widget<T> + 'static>(&mut self, widget: W) {
        self.children.push(child(Box::new(widget)));
    }
    /// A builder style variant of `add_child`.
    pub fn with_child<W: Widget<T> + 'static>(mut self, widget: W) -> Self {
        self.children.push(child(Box::new(widget)));
        self
    }
    /// Insert a spacer into the [`struct@Column`].
    pub fn with_spacing(self, size: f32) -> Self {
        self.with_child(Spacer::horizontal(size))
    }
}

impl<T, W: Widget<T> + Themeable> Themeable for Column<T, W> {
    const NAME: &'static str = "boxed-list";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(item) = self.children.first_mut() {
            let id = item.id(Some("left"));
            item.theme(list.push(id), env);
        }
        if let Some(item) = self.children.last_mut() {
            let id = item.id(Some("right"));
            item.theme(list.push(id), env);
        }
    }
}

impl<T, W: Widget<T>> FromIterator<W> for Column<T, W> {
    fn from_iter<I: IntoIterator<Item = W>>(iter: I) -> Self {
        let mut this = Column::new();
        for widget in iter {
            this.children.push(child(widget));
        }
        this
    }
}

#[macro_export]
/// A horizontal layout of widgets.
macro_rules! Row {
    ($($child:expr),* $(,)?) => {
        {
            let mut f = $crate::widgets::Row::new();

            $(
                f.add_child($child);
            )*

            f
        }
    };
}

#[macro_export]
/// A vertical layout of widgets.
macro_rules! Column {
    ($($child:expr),* $(,)?) => {
        {
            let mut f = $crate::widgets::Column::new();

            $(
                f.add_child($child);
            )*

            f
        }
    };
}
