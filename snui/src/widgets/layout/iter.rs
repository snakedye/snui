use std::marker::PhantomData;

use super::*;
use crate::scene::Region;
use crate::*;

/// Iterator that builds a vertical layout of widget.
pub struct RowIter<'a, I, W, T>
where
    I: Iterator<Item = W>,
{
    iter: I,
    pos: Position,
    len: usize,
    index: usize,
    bc: &'a BoxConstraints,
    context: LayoutContext<'a, 'a, T>,
    env: &'a Env<'a, 'a>,
    _data: std::marker::PhantomData<T>,
}

impl<'a, I, W, T> RowIter<'a, I, W, T>
where
    I: Iterator<Item = W>,
{
    /// Create a new layout iterator.
    ///
    /// # Arguments
    ///
    /// * `iter` - An iterator of widgets.
    /// * `context` - The [`LayoutContext`].
    /// * `bc` - The layout constraints.
    /// * `env` - The current state environment.
    pub fn new(
        iter: I,
        context: &'a mut LayoutContext<T>,
        bc: &'a BoxConstraints,
        env: &'a Env,
    ) -> Self {
        Self {
            iter,
            len: usize::MAX,
            context: context.borrow(),
            bc,
            env,
            index: 0,
            pos: Position::default(),
            _data: PhantomData,
        }
    }
    pub fn with_len(mut self, len: usize) -> Self {
        self.len = len;
        self
    }
}

impl<'a, I, W, T> Iterator for RowIter<'a, I, W, T>
where
    I: Iterator<Item = W>,
    W: Positionable<Value = f32> + Widget<T>,
{
    type Item = Region;

    fn next(&mut self) -> Option<Self::Item> {
        let pos = self.pos;
        self.iter
            .next()
            .map(|mut widget| {
                widget.set_position(0., self.pos.y);
                let height =
                    (self.bc.maximum_height() - self.pos.y) / (self.len - self.index) as f32;
                let size = widget.layout(
                    &mut self.context,
                    &self.bc.with_max(self.bc.maximum_width(), height),
                    self.env,
                );
                self.index += 1;
                self.pos.y += size.height;
                size
            })
            .map(|size| Region::new(pos.x, pos.y, size.width, size.height))
    }
}

/// Iterator that builds an horizontal layout of widget.
pub struct ColumnIter<'a, I, W, T>
where
    I: Iterator<Item = W>,
{
    iter: I,
    pos: Position,
    len: usize,
    index: usize,
    env: &'a Env<'a, 'a>,
    bc: &'a BoxConstraints,
    context: LayoutContext<'a, 'a, T>,
    _data: std::marker::PhantomData<T>,
}

impl<'a, I, W, T> ColumnIter<'a, I, W, T>
where
    I: Iterator<Item = W>,
{
    /// Create a new layout iterator.
    ///
    /// # Arguments
    ///
    /// * `iter` - An iterator of widgets.
    /// * `context` - The [`LayoutContext`].
    /// * `bc` - The layout constraints.
    /// * `env` - The current state environment.
    pub fn new(
        iter: I,
        context: &'a mut LayoutContext<T>,
        bc: &'a BoxConstraints,
        env: &'a Env<'a, 'a>,
    ) -> Self {
        Self {
            iter,
            len: usize::MAX,
            context: context.borrow(),
            bc,
            env,
            index: 0,
            pos: Position::default(),
            _data: PhantomData,
        }
    }
    pub fn with_len(mut self, len: usize) -> Self {
        self.len = len;
        self
    }
}

impl<'a, I, W, T> Iterator for ColumnIter<'a, I, W, T>
where
    I: Iterator<Item = W>,
    W: Positionable<Value = f32> + Widget<T>,
{
    type Item = Region;

    fn next(&mut self) -> Option<Self::Item> {
        let pos = self.pos;
        self.iter
            .next()
            .map(|mut widget| {
                widget.set_position(self.pos.x, 0.);
                let width = (self.bc.maximum_width() - self.pos.x) / (self.len - self.index) as f32;
                let size = widget.layout(
                    &mut self.context,
                    &self.bc.with_max(width, self.bc.maximum_height()),
                    self.env,
                );
                self.index += 1;
                self.pos.x += size.width;
                size
            })
            .map(|size| Region::new(pos.x, pos.y, size.width, size.height))
    }
}
