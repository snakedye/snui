//! Layout widgets

mod center;
mod iter;
mod list;
use crate::*;
pub use center::Center;
pub use iter::*;
pub use list::{Column, Row};
use scene::Position;
use std::ops::{Deref, DerefMut};
use widgets::scroll::Scrollable;
use widgets::Style;

/// A type that can be positionned.
pub trait Positionable {
    type Value;
    fn set_position(&mut self, x: Self::Value, y: Self::Value);
}

impl<'a, T> Positionable for &'a mut T
where
    T: Positionable,
{
    type Value = T::Value;
    fn set_position(&mut self, x: Self::Value, y: Self::Value) {
        T::set_position(self, x, y);
    }
}

/// A widget with relative coordinates.
#[derive(Clone, Debug, PartialEq)]
pub struct Positioner<W> {
    position: Guard<Position>,
    pub(crate) inner: W,
}

impl<W> Positioner<W> {
    pub fn new(inner: W) -> Self {
        Positioner {
            inner,
            position: Default::default(),
        }
    }
    pub fn at(mut self, position: Position) -> Self {
        self.position.set(position);
        self
    }
    pub fn position(&self) -> Position {
        self.position.get()
    }
}

impl<W> Guarded for Positioner<W> {
    fn restore(&mut self) {
        self.position.restore()
    }
    fn touched(&self) -> bool {
        self.position.touched()
    }
}

impl<W> Positionable for Positioner<W> {
    type Value = f32;
    fn set_position(&mut self, x: Self::Value, y: Self::Value) {
        let position = Position::new(x, y);
        self.position.set(position);
    }
}

impl<W: Geometry> Geometry for Positioner<W> {
    fn width(&self) -> f32 {
        self.inner.width()
    }
    fn height(&self) -> f32 {
        self.inner.height()
    }
    fn contains(&self, position: &Position) -> bool {
        self.inner.contains(&(*position - self.position.get()))
    }
}

impl<T, W: Widget<T>> Widget<T> for Positioner<Proxy<T, W>> {
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.restore();
        self.inner
            .draw_scene(scene.translate(self.position.x, self.position.y), env);
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.damage().max(match event {
            Event::Pointer(MouseEvent { position, pointer }) => self.inner.event(
                ctx,
                MouseEvent::new(position - self.position.get(), pointer),
                env,
            ),
            _ => self.inner.event(ctx, event, env),
        })
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.damage().max(self.inner.update(ctx, env))
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env)
    }
}

impl<W: Style> Style for Positioner<W> {
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.inner.set_texture(texture)
    }
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_left_radius(radius);
    }
}

impl<W: Scrollable> Scrollable for Positioner<W> {
    fn forward(&mut self, step: Option<f32>) {
        self.inner.forward(step)
    }
    fn backward(&mut self, step: Option<f32>) {
        self.inner.backward(step)
    }
    fn inner_height(&self) -> f32 {
        self.inner.inner_height()
    }
    fn inner_width(&self) -> f32 {
        self.inner.inner_width()
    }
    fn orientation(&self) -> Orientation {
        self.inner.orientation()
    }
    fn position(&self) -> f32 {
        self.inner.position()
    }
}

impl<W: Drawable> Drawable for Positioner<W> {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        self.inner.draw(
            ctx,
            transform.pre_translate(self.position.x, self.position.y),
        )
    }
}

impl<W> Deref for Positioner<W> {
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<W> DerefMut for Positioner<W> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

pub fn child<T, W: Widget<T>>(inner: W) -> Positioner<Proxy<T, W>> {
    Positioner::new(Proxy::new(inner))
}
