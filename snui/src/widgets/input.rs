use crate::widgets::{Label, Positionable, Positioner, Rectangle, Style};
use crate::*;
use msg_api::*;
#[cfg(feature = "linux")]
use xkbcommon::xkb::keysyms::*;

/// The look of the cursor inside the [`Input`] widget.
pub enum CursorType {
    Char,
    Line,
}

/// An input field widget.
pub struct Input<M> {
    message: M,
    label: Label,
    focused: bool,
    active: bool,
    offset: Position,
    size: Size,
    cursor_index: usize,
    cursor_type: CursorType,
    cursor: Positioner<Rectangle>,
}

impl<T> Guarded for Input<T> {
    fn restore(&mut self) {
        self.label.restore();
        self.cursor.restore();
    }
    fn touched(&self) -> bool {
        self.label.touched() || self.cursor.touched()
    }
}

impl<M> Themeable for Input<M> {
    const NAME: &'static str = "input";

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        self.cursor.theme(
            id.push(Id::new("cursor", self.focused.then_some("focused"))),
            env,
        );
        self.label.theme(id.push(self.label.id(None)), env);
    }
}

impl<M> Input<M> {
    pub fn new(message: M) -> Self {
        Self {
            label: Label::from(""),
            message,
            focused: false,
            active: false,
            offset: Position::default(),
            size: Size::default(),
            cursor_type: CursorType::Line,
            cursor: Positioner::new(Rectangle::default().texture(color::BEIGE)),
            cursor_index: 0,
        }
    }
    pub fn cursor(mut self, cursor: CursorType) -> Self {
        self.cursor_type = cursor;
        self
    }
    /// Assign a message to the input field.
    pub fn map<T>(self, message: T) -> Input<T> {
        Input {
            message,
            label: self.label,
            focused: self.focused,
            active: self.active,
            size: Size::default(),
            offset: Position::default(),
            cursor_index: self.cursor_index,
            cursor_type: self.cursor_type,
            cursor: self.cursor,
        }
    }
}

impl<M> Geometry for Input<M> {
    fn width(&self) -> f32 {
        self.label.width() + self.cursor.width()
    }
    fn height(&self) -> f32 {
        self.label.height().max(self.cursor.height())
    }
}

impl<M> Drawable for Input<M> {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        if self.active {
            self.cursor.draw(
                ctx,
                transform.pre_scale(1. / transform.sx, 1. / transform.sy),
            );
        }
        self.label.draw(ctx, transform);
    }
}

impl<M, T> Widget<T> for Input<M>
where
    for<'a> M::Message<'a>: PartialEq + FieldParameter + AsRef<str>,
    for<'a> M: MessageBuilder<Data<'a> = &'a str>,
    for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
{
    fn draw_scene(&mut self, mut scene: Scene, _env: &Env) {
        let scale = scene.context().scale();
        scene
            .apply_clip(self.size)
            .translate(self.offset.x / scale, self.offset.y / scale)
            .draw(self)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer,
                mut position,
            }) => match pointer {
                Pointer::Enter => {
                    self.focused = true;
                    ctx.window().set_cursor(Cursor::Text);
                }
                Pointer::Leave => {
                    self.focused = false;
                    ctx.window().set_cursor(Cursor::Arrow);
                }
                _ => {
                    if pointer.left_button_click().is_some() {
                        position = position - self.offset;
                        let scale = (self.label.layout.height() / self.size.height).ceil();
                        let cursor_index = self.cursor_index;
                        self.cursor_index = self
                            .label
                            .layout
                            .glyphs()
                            .iter()
                            .enumerate()
                            .find(|(_, gp)| gp.x >= position.x * scale)
                            .map(|(i, gp)| {
                                if gp.width == 0 {
                                    i
                                } else {
                                    (i as f32 + ((position.x * scale) - gp.x) / gp.width as f32)
                                        .round() as usize
                                }
                            })
                            .unwrap_or_else(|| self.label.layout.glyphs().len());
                        return (!self.active || cursor_index != self.cursor_index)
                            .then(|| {
                                self.active = true;
                                Damage::Partial
                            })
                            .unwrap_or_default();
                    }
                }
            },
            Event::Configure => {
                if !ctx.window().get_state().contains(&WindowState::Activated) && self.active {
                    self.active = false;
                    return Damage::Partial;
                }
            }
            Event::Focus(Focus {
                state, focus_type, ..
            }) => {
                if matches!(focus_type, FocusType::Local | FocusType::Global) {
                    self.active |= state;
                    return (!state).then_some(Damage::Partial).unwrap_or_default();
                } else {
                    self.active = state;
                    self.focused = state;
                    return Damage::Partial;
                }
            }
            Event::Keyboard(key) => {
                if key.pressed {
                    #[cfg(feature = "linux")]
                    if key.value.contains(&KEY_Escape) {
                        self.clear();
                        self.active = false;
                        self.cursor_index = 0;
                        ctx.window().set_cursor(Cursor::Arrow);
                        ctx.post(&self.message, "");
                        return Damage::Partial;
                    } else if key.value.contains(&KEY_Return) {
                        let s = ctx.get(&self.message);
                        self.label.edit(s.as_ref());
                        self.cursor_index = s.as_ref().len();
                        std::mem::drop(s);
                        ctx.window().set_cursor(Cursor::Arrow);
                    } else if key.value.contains(&KEY_BackSpace) {
                        if key.modifiers.ctrl {
                            if let Some(word) = self.text[0..self.cursor_index]
                                .rsplit(&[' ', '/', '-', '.'])
                                .next()
                            {
                                let len = word.len();
                                let total_len = self.len();
                                let mut i =
                                    self.text[0..self.cursor_index].rfind(word).unwrap().max(1);
                                if len == 0 && i > 0 {
                                    i -= 1;
                                } else if len == total_len {
                                    i = 0;
                                }
                                self.replace_range(i..(i + len + 1).min(total_len), "");
                                self.cursor_index = i;
                            } else {
                            }
                        } else if self.cursor_index > 0 {
                            let len = self.text.chars().rev().next().unwrap().len_utf8();
                            self.cursor_index -= len;
                            self.label.remove(self.cursor_index);
                        }
                        ctx.post(&self.message, self.label.as_str());
                    } else if key.value.contains(&KEY_Left) && self.cursor_index > 0 {
                        if key.modifiers.ctrl {
                            if let Some(word) = self.text[0..self.cursor_index]
                                .rsplit(&[' ', '/', '-', '.'])
                                .find(|w| !w.is_empty())
                            {
                                self.cursor_index =
                                    self.text[0..self.cursor_index].rfind(word).unwrap();
                            } else {
                                self.cursor_index = self.cursor_index.max(1) - 1;
                            }
                        } else {
                            let len = self.text.chars().rev().next().unwrap().len_utf8();
                            self.cursor_index = self.cursor_index.max(len) - len;
                        }
                        return Damage::Partial;
                    } else if key.value.contains(&KEY_Right)
                        && self.cursor_index < self.label.text.len()
                    {
                        if key.modifiers.ctrl {
                            if let Some(word) = self.text[self.cursor_index..]
                                .split(&[' ', '/', '-', '.'])
                                .find(|w| !w.is_empty())
                            {
                                self.cursor_index = self.cursor_index
                                    + self.text[self.cursor_index..].find(word).unwrap()
                                    + word.len();
                            }
                        } else {
                            self.cursor_index += 1;
                        }
                        return Damage::Partial;
                    } else if let Some(utf8) = key.utf8 {
                        if !utf8.is_empty() {
                            if self.label.text.is_empty() {
                                self.label.push_str(utf8);
                            } else {
                                self.label.insert_str(self.cursor_index, utf8);
                            }
                            if ctx.get(&self.message).as_ref().ne(self.label.as_str()) {
                                ctx.update();
                                ctx.post(&self.message, self.label.as_str());
                            }
                            self.cursor_index += utf8.len();
                            if self.focused {
                                ctx.window().set_cursor(Cursor::None);
                            }
                        }
                    }
                }
            }
            Event::Message("theme") => env
                .push(Id::new("cursor", self.focused.then_some("focused")))
                .map(self.cursor.deref_mut()),
            _ => {}
        }
        self.label.event(ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
    	ctx.prepare();
        self.label.edit(ctx.get(&self.message).as_ref());
        self.label.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let scale = ctx.scale();
        Widget::<T>::layout(
            &mut self.label,
            ctx,
            &bc.with_max(std::f32::INFINITY, bc.maximum_height()),
            env,
        );
        let height;
        if self.cursor_index > 0 && self.cursor_index <= self.label.text.len() {
            let layout = &mut self.label.layout;
            let mut len = 0;
            let index = self
                .label
                .text
                .chars()
                .map(|c| c.len_utf8())
                .filter(|len_utf8| {
                    len += len_utf8;
                    len < self.cursor_index
                })
                .count();

            height = layout.height();
            let lines = layout.lines();
            let layout = layout.glyphs();
            let gp = layout[index];
            if let Some(line) = lines {
                let y = line
                    .iter()
                    .find(|line| index >= line.glyph_start && index < line.glyph_end)
                    .map(|position| position.baseline_y - position.max_ascent)
                    .unwrap_or(0.);
                self.cursor.set_height(height);
                match self.cursor_type {
                    CursorType::Char => {
                        self.cursor.set_width(gp.width as f32);
                        self.cursor.set_position(gp.x, y);
                        self.offset.x = (self.size.width - gp.x - 1.).min(0.);
                    }
                    CursorType::Line => {
                        self.cursor.set_width(1.);
                        if gp.parent.is_whitespace() && index + 1 < layout.len() {
                            let gp = layout[index + 1];
                            self.cursor.set_position(gp.x, y);
                            self.offset.x = (self.size.width * scale - gp.x - 1. * scale).min(0.);
                        } else {
                            self.cursor.set_position(gp.x + gp.width as f32, y);
                            self.offset.x =
                                (self.size.width * scale - gp.x - gp.width as f32 - 1. * scale)
                                    .min(0.);
                        }
                    }
                }
            }
        } else {
            self.cursor.set_position(0., 0.);
            height = self.label.text_layout().height().max(self.cursor.height());
        }
        self.size = Size::new(bc.maximum_width(), self.label.font_size.max(height / scale));
        self.size
    }
}

impl<M> Deref for Input<M> {
    type Target = Label;

    fn deref(&self) -> &Self::Target {
        &self.label
    }
}

impl<M> DerefMut for Input<M> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.label
    }
}
