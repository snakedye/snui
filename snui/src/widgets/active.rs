use crate::*;
use environment::Id;
use msg_api::*;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

use super::Style;

/// A widget that's activated when the state associated to its message changes.
pub struct Activate<T, W, M, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    cb: F,
    message: M,
    active: bool,
    inner: W,
    _data: PhantomData<T>,
}

impl<T, W, M, F> Guarded for Activate<T, W, M, F>
where
    W: Widget<T> + Guarded,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    fn restore(&mut self) {
        self.inner.restore()
    }
    fn touched(&self) -> bool {
        self.inner.touched()
    }
}

impl<T, W, M, F> Activate<T, W, M, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    /// Create an [`Activate`] widget.
    ///
    /// # Arguments
    ///
    /// * `inner` - The widget manipulated.
    /// * `message` - The message that will trigger an activation or deactivation.
    /// * `cb` - The callback where the widget will be manipulated.
    pub fn new(inner: W, message: M, cb: F) -> Self {
        Self {
            cb,
            message,
            active: false,
            inner,
            _data: PhantomData,
        }
    }
    /// The state of the widget.
    pub fn active(&self) -> bool {
        self.active
    }
}

impl<T, W, M, F> Widget<T> for Activate<T, W, M, F>
where
    W: Widget<T>,
    M: MessageBuilder,
    for<'m> M::Message<'m>: Release<bool>,
    T: Request<M>,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner.draw_scene(
            scene,
            &env.push(Id::new("button", self.active.then_some("hover"))),
        );
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let mut list;
        ctx.prepare();
        if let Event::Focus(Focus {
            state, focus_type, ..
        }) = event
        {
            if state
                && matches!(focus_type, FocusType::Global | FocusType::Local)
                && self.active != ctx.get(&self.message).release()
            {
                self.active = !self.active;
                list = env.push(Id::new("active", self.active.then_some("activated")));
                (self.cb)(&mut self.inner, self.active, ctx, &list);
            }
        }
        list = env.push(Id::new("active", self.active.then_some("activated")));
        self.inner.event(ctx, event, &list)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        let list;
        ctx.prepare();
        if self.active != ctx.get(&self.message).release() {
            self.active = !self.active;
            list = env.push(Id::new("active", self.active.then_some("activated")));
            (self.cb)(&mut self.inner, self.active, ctx, &list);
        } else {
            list = env.push(Id::new("active", self.active.then_some("activated")));
        }
        self.inner.update(ctx, &list)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(
            ctx,
            bc,
            &env.push(Id::new("active", self.active.then_some("activated"))),
        )
    }
}

impl<T, W, M, F> Themeable for Activate<T, W, M, F>
where
    W: Widget<T> + Themeable,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    const NAME: &'static str = W::NAME;

    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        self.inner.theme(list, env);
    }
}

impl<T, W, M, F> Style for Activate<T, W, M, F>
where
    W: Widget<T> + Style,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.inner.set_texture(texture);
    }
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_left_radius(radius);
    }
}

impl<T, W, M, F> Deref for Activate<T, W, M, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W, M, F> DerefMut for Activate<T, W, M, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, bool, &'a mut UpdateContext<T>, &'a Env),
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
