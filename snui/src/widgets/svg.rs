use resvg::render;
use usvg::{FitTo, Tree};

use super::image::*;
use super::Rectangle;
use super::Style;
use crate::cache::source::Source;
use crate::scene::Region;
use crate::*;
use std::path::{Path, PathBuf};
use std::sync::Arc;

/// A SVG image widget.
#[derive(Clone)]
pub struct Svg {
    path: Option<PathBuf>,
    inner: Option<InnerSvg>,
}

#[derive(Clone)]
pub struct InnerSvg {
    size: Size,
    tree: Arc<Tree>,
    mask: Option<Region>,
}

impl From<Size> for FitTo {
    fn from(size: Size) -> Self {
        FitTo::Size(size.width as u32, size.height as u32)
    }
}

impl Geometry for InnerSvg {
    fn width(&self) -> f32 {
        self.size.width
    }
    fn height(&self) -> f32 {
        self.size.height
    }
}

impl Drawable for InnerSvg {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        if let Backend::Pixmap { pixmap, clipmask } = ctx.deref_mut() {
            let width = pixmap.width();
            let height = pixmap.height();
            if let Some(mask) = self.mask {
                let mut layer =
                    tiny_skia::Pixmap::new(mask.width as u32, mask.height as u32).unwrap();
                render(&self.tree, self.size.into(), transform, layer.as_mut());
                pixmap
                    .draw_pixmap(
                        0,
                        0,
                        layer.as_ref(),
                        &context::TEXT,
                        transform,
                        clipmask.as_ref().map(|mask| &**mask),
                    )
                    .unwrap();
            } else {
                let pixmap =
                    tiny_skia::PixmapMut::from_bytes(pixmap.data_mut(), width, height).unwrap();
                render(&self.tree, self.size.into(), transform, pixmap);
            }
        }
    }
}

impl<T> Widget<T> for Svg {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        if let Some(inner) = self.inner.as_mut() {
            inner.mask = scene.clip().copied();
            scene.draw(inner);
            inner.mask.take();
        }
    }
    fn event(&mut self, _: &mut UpdateContext<T>, _: Event, _env: &Env) -> Damage {
        Damage::None
    }
    fn update(&mut self, _: &mut UpdateContext<T>, _env: &Env) -> Damage {
        Damage::None
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, _env: &Env) -> Size {
        match self.inner.as_mut() {
            Some(inner) => {
                inner.size.width = inner
                    .size
                    .width
                    .clamp(bc.minimum_width(), bc.maximum_width())
                    .round();
                inner.size.height = inner
                    .size
                    .height
                    .clamp(bc.minimum_height(), bc.maximum_height())
                    .round();
                return inner.size;
            }
            None => {
                if let Ok(Source::Vector(tree)) = ctx
                    .cache
                    .source_cache
                    .get(self.path.take().unwrap().as_path())
                {
                    eprintln!("warn: usvg: SVGs cannot be clipped!");
                    let inner = InnerSvg {
                        size: bc.maximum(),
                        mask: None,
                        tree,
                    };
                    self.inner = Some(inner);
                    return bc.maximum();
                }
            }
        }
        Size::default()
    }
}

impl<P: AsRef<Path>> From<P> for Svg {
    fn from(path: P) -> Self {
        Self {
            path: Some(path.as_ref().to_path_buf()),
            inner: None,
        }
    }
}

#[cfg(any(feature = "img", feature = "svg"))]
#[derive(Clone)]
/// A widget that can be an [`Image`] or an [`Svg`].
pub enum GenericIcon {
    Svg(InnerSvg),
    Image(InnerImage),
}

impl GenericIcon {
    pub fn prefered_size(&self) -> Option<Size> {
        if let Self::Image(inner) = self {
            if let scene::Texture::Image(image) = inner.image.as_ref() {
                return Some(image.size());
            }
        }
        None
    }
    pub fn as_ref(&self) -> &dyn Drawable {
        match self {
            Self::Svg(inner) => inner,
            Self::Image(inner) => inner,
        }
    }
}

impl From<Source> for GenericIcon {
    fn from(source: Source) -> Self {
        match source {
            Source::Vector(tree) => {
                eprintln!("warn: usvg: SVGs cannot be clipped!");
                Self::Svg(InnerSvg {
                    size: Size::default(),
                    mask: None,
                    tree,
                })
            }
            Source::Pixmap(pixmap) => Self::Image(InnerImage {
                scale: Scale::KeepAspect,
                image: Rectangle::default().texture(pixmap),
            }),
        }
    }
}

impl Geometry for GenericIcon {
    fn width(&self) -> f32 {
        self.as_ref().width()
    }
    fn height(&self) -> f32 {
        self.as_ref().height()
    }
}

impl GeometryExt for GenericIcon {
    fn set_width(&mut self, _width: f32) {}
    fn set_height(&mut self, _height: f32) {}
    fn set_size(&mut self, width: f32, height: f32) {
        match self {
            Self::Svg(inner) => {
                inner.size = Size::new(width, height);
            }
            Self::Image(inner) => match inner.scale {
                Scale::KeepAspect => {
                    let raw = match inner.image.as_ref() {
                        scene::Texture::Image(ref raw) => raw,
                        _ => unreachable!(),
                    };
                    let (width, height) = if width < height {
                        (width, width * raw.height() / raw.width())
                    } else {
                        (height * raw.width() / raw.height(), height)
                    };
                    inner.image.set_size(width, height);
                }
                Scale::Fill => inner.image.set_size(width, height),
            },
        }
    }
}

impl<T> Widget<T> for GenericIcon {
    fn draw_scene(&mut self, scene: Scene, _: &Env) {
        match self {
            Self::Svg(inner) => scene.draw(inner),
            Self::Image(inner) => scene.draw(&inner.image),
        }
    }
    fn event(&mut self, _: &mut UpdateContext<T>, _: Event, _: &Env) -> Damage {
        Damage::None
    }
    fn update(&mut self, _: &mut UpdateContext<T>, _: &Env) -> Damage {
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, bc: &BoxConstraints, _env: &Env) -> Size {
        self.set_size(bc.maximum_width(), bc.maximum_height());
        self.size()
    }
}
