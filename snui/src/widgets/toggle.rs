use std::ops::Not;

use super::BorderedRectangle;
use crate::widgets::utils::*;
use crate::widgets::{Positionable, Rectangle, Style};
use crate::*;
use msg_api::{FieldParameter, MessageBuilder, MessageHandler, Release, Request};
use tiny_skia::{Color, GradientStop};

/// The state of the [`Toggle`].
#[derive(Clone, Debug, Copy, PartialEq, Eq)]
pub enum ToggleState {
    Activated,
    Deactivated,
}

impl Not for ToggleState {
    type Output = Self;
    fn not(self) -> Self::Output {
        match self {
            Self::Activated => Self::Deactivated,
            Self::Deactivated => Self::Activated,
        }
    }
}

/// Two stage toggle.
pub struct Toggle<M, E: Easer> {
    running: bool,
    toggle: Positioner<Rectangle>,
    state: ToggleState,
    easer: E,
    duration: u32,
    message: M,
    gradient: BorderedRectangle,
}

impl<M> Toggle<M, Quadratic> {
    pub fn default(message: M) -> Self {
        Self::new(message)
    }
}

impl<M, E: Easer> Themeable for Toggle<M, E> {
    const NAME: &'static str = "toggle";

    fn theme(&mut self, id: List<Id>, env: &dyn Environment) {
        self.toggle.theme(id, env);
        set!(self.gradient.inner, radius = self.toggle.inner.inner.radius);
    }
}

impl<M, E: Easer> Guarded for Toggle<M, E> {
    fn restore(&mut self) {
        self.toggle.restore()
    }
    fn touched(&self) -> bool {
        self.toggle.touched()
    }
}

impl<M, E: Easer> Toggle<M, E> {
    pub fn new(message: M) -> Self {
        Self {
            running: false,
            toggle: Positioner::new(Rectangle::new(20., 20.).texture(color::BG2)),
            easer: Easer::new(0., 0.5),
            message,
            duration: 500,
            state: ToggleState::Deactivated,
            gradient: BorderedRectangle::default().border_width(4.).texture(
                scene::RadialGradient {
                    stops: vec![
                        GradientStop::new(0., Color::BLACK),
                        GradientStop::new(1., Color::TRANSPARENT),
                    ]
                    .into(),
                    mode: SpreadMode::Pad,
                },
            ),
        }
    }
    fn set_state(&mut self, state: ToggleState) -> ToggleState {
        let state = match state {
            ToggleState::Activated => {
                self.easer = Easer::new(0.5, 1.);
                ToggleState::Deactivated
            }
            ToggleState::Deactivated => {
                self.easer = Easer::new(0., 0.5);
                ToggleState::Activated
            }
        };
        self.state = state;
        state
    }
    /// The duration of the animation in ms.
    pub fn duration(mut self, duration: u32) -> Self {
        self.duration = duration;
        self
    }
    pub fn state(&self) -> ToggleState {
        self.state
    }
}

impl<M, E: Easer> Geometry for Toggle<M, E> {
    fn width(&self) -> f32 {
        self.toggle.width() * 2. + 4.
    }
    fn height(&self) -> f32 {
        self.toggle.height() + 4.
    }
}

impl<M, E: Easer> GeometryExt for Toggle<M, E> {
    fn set_width(&mut self, width: f32) {
        self.toggle.set_width(width / 2.);
    }
    fn set_height(&mut self, height: f32) {
        self.toggle.set_height(height);
    }
}

impl<M, E: Easer> Drawable for Toggle<M, E> {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        let Position { x, y } = self.toggle.position();
        self.gradient.draw(ctx, transform.pre_translate(x, y));
        self.toggle.draw(ctx, transform.pre_translate(2., 2.));
    }
}

impl<M, E, T> Widget<T> for Toggle<M, E>
where
    E: Easer,
    for<'a> M::Message<'a>: PartialEq + FieldParameter + Release<bool>,
    for<'a> M: MessageBuilder<Data<'a> = bool>,
    for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
{
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        self.restore();
        scene.draw(self)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        ctx.prepare();
        let state = if ctx.get(&self.message).release() {
            ToggleState::Deactivated
        } else {
            ToggleState::Activated
        };
        if state == self.state {
            self.running = true;
            self.set_state(state);
            Damage::Frame(self.touched())
        } else {
            Damage::None
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer,
                ref position,
            }) => {
                if self.contains(position) && pointer.left_button_click().is_some() {
                    self.running = true;
                    self.set_state(self.state);
                    match self.state {
                        ToggleState::Activated => {
                            ctx.post(&self.message, true);
                        }
                        ToggleState::Deactivated => {
                            ctx.post(&self.message, false);
                        }
                    };
                    return Damage::Frame(self.touched());
                }
            }
            Event::Callback(frame_time) => {
                if self.running {
                    let steps = self.easer.partial_steps(frame_time, self.duration);
                    let position = self.easer.step_by(steps);
                    self.toggle.set_position(
                        self.toggle.width() * position.unwrap_or_else(|| self.easer.position()),
                        0.,
                    );
                    self.running = position.is_some();
                    return Damage::Frame(self.touched());
                }
            }
            _ => {
                if event == STYLESHEET_SIGNAL {
                    env.map(self);
                }
            }
        }
        self.damage()
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, _bc: &BoxConstraints, _env: &Env) -> Size {
        self.gradient
            .set_size(self.toggle.width() + 4., self.toggle.height() + 4.);
        self.size()
    }
}

impl<M, E: Easer> Style for Toggle<M, E> {
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.toggle.set_texture(texture);
    }
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.toggle.set_top_left_radius(radius);
        self.gradient.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.toggle.set_top_right_radius(radius);
        self.gradient.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.toggle.set_bottom_right_radius(radius);
        self.gradient.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.toggle.set_bottom_left_radius(radius);
        self.gradient.set_bottom_left_radius(radius);
    }
}
