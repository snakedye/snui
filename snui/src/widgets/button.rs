use crate::*;
use environment::Id;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

use super::Style;

/// A container widget that reponds to pointer events.
pub struct Button<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    cb: F,
    entered: bool,
    inner: W,
    _data: PhantomData<T>,
}

impl<T, W, F> Button<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    /// Creates a new button.
    pub fn new(child: W, cb: F) -> Proxy<T, Self> {
        Proxy::new(Self {
            cb,
            entered: false,
            inner: child,
            _data: PhantomData,
        })
    }
}

impl<T, W, F> Guarded for Button<T, W, F>
where
    W: Widget<T> + Guarded,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    fn restore(&mut self) {
        self.inner.restore()
    }
    fn touched(&self) -> bool {
        self.inner.touched()
    }
}

impl<T, W, F> Widget<T> for Button<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    fn draw_scene(&mut self, scene: Scene, env: &Env) {
        self.inner.draw_scene(
            scene,
            &env.push(Id::new("button", self.entered.then_some("hover"))),
        );
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let list;
        ctx.prepare();
        match event {
            Event::Pointer(MouseEvent { pointer, .. }) => {
                self.entered = pointer != Pointer::Leave;
                list = env.push(Id::new("button", self.entered.then_some("hover")));
                (self.cb)(&mut self.inner, ctx, &list, pointer);
            }
            Event::Focus(focus) => match focus.focus_type {
                FocusType::Global | FocusType::Pointer => {
                    self.entered = focus.state;
                    let pointer = if focus.state {
                        Pointer::Enter
                    } else {
                        Pointer::Leave
                    };
                    list = env.push(Id::new("button", self.entered.then_some("hover")));
                    (self.cb)(&mut self.inner, ctx, &list, pointer);
                }
                _ => {
                    list = env.push(Id::new("button", self.entered.then_some("hover")));
                }
            },
            _ => {
                list = env.push(Id::new("button", self.entered.then_some("hover")));
            }
        }
        self.inner.event(ctx, event, &list)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.inner.update(
            ctx,
            &env.push(Id::new("button", self.entered.then_some("hover"))),
        )
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(
            ctx,
            bc,
            &env.push(Id::new("button", self.entered.then_some("hover"))),
        )
    }
}

impl<T, W, F> Themeable for Button<T, W, F>
where
    W: Widget<T> + Themeable,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    const NAME: &'static str = W::NAME;

    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        self.inner.theme(list, env);
    }
}

impl<T, W, F> Style for Button<T, W, F>
where
    W: Widget<T> + Style,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.inner.set_bottom_left_radius(radius);
    }
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.inner.set_texture(texture);
    }
}

impl<T, W, F> Deref for Button<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    type Target = W;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, W, F> DerefMut for Button<T, W, F>
where
    W: Widget<T>,
    F: for<'a> FnMut(&'a mut W, &'a mut UpdateContext<T>, &Env, Pointer),
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
