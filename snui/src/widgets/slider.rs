use super::layout::Positionable;
use super::BorderedCircle;
use super::Circle;
use crate::*;
use msg_api::*;
use scene::RadialGradient;
use scene::Texture;
use widgets::shapes::Rectangle;
use widgets::shapes::Style;

/// A widget that displays values in a range.
///
/// A value between 0. and 1.0 is sent to the application.
pub struct Progress<M> {
    message: M,
    flip: bool,
    size: Size,
    ratio: Guard<f32>,
    pressed: bool,
    bar: Rectangle,
    orientation: Orientation,
}

impl<M> Progress<M> {
    pub fn new(message: M) -> Self {
        Progress {
            message,
            ratio: Guard::from(1.),
            flip: false,
            pressed: false,
            size: Size::default(),
            bar: Rectangle::default(),
            orientation: Orientation::Horizontal,
        }
    }
}

impl<M> Themeable for Progress<M> {
    const NAME: &'static str = "progress";

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        self.bar.theme(id, env);
    }
}

impl<M> Guarded for Progress<M> {
    fn restore(&mut self) {
        self.ratio.restore();
        self.bar.inner.restore()
    }
    fn touched(&self) -> bool {
        self.ratio.touched() || self.bar.inner.touched()
    }
}

impl<M> Progress<M> {
    /// Invert the direction of the slider.
    pub fn flip(mut self) -> Self {
        self.flip = true;
        self
    }
    /// Set the orientation.
    pub fn orientation(mut self, orientation: Orientation) -> Self {
        self.orientation = orientation;
        self
    }
    fn inner_offset(&self) -> Position {
        self.flip
            .then_some(match self.orientation {
                Orientation::Horizontal => Position::new(self.width() - self.bar.width(), 0.),
                Orientation::Vertical => Position::new(0., self.height() - self.bar.height()),
            })
            .unwrap_or_default()
    }
}

impl<M> Geometry for Progress<M> {
    fn width(&self) -> f32 {
        if let Orientation::Horizontal = &self.orientation {
            self.size.width()
        } else {
            self.bar.width()
        }
    }
    fn height(&self) -> f32 {
        if let Orientation::Vertical = &self.orientation {
            self.size.height()
        } else {
            self.bar.height()
        }
    }
}

impl<M> Drawable for Progress<M> {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        let Position { x, y } = self.inner_offset();
        self.bar.draw(ctx, transform.pre_translate(x, y));
    }
}

impl<M, T> Widget<T> for Progress<M>
where
    for<'a> M::Message<'a>: PartialEq + FieldParameter + Release<f32>,
    for<'a> M: MessageBuilder<Data<'a> = f32>,
    for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
{
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        self.restore();
        scene.draw(self);
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, _env: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer,
                ref position,
            }) => match pointer {
                Pointer::MouseClick {
                    serial: _,
                    button,
                    pressed,
                } => {
                    if self.pressed || self.contains(position) {
                        let mut ratio;
                        self.pressed = pressed;
                        match &self.orientation {
                            Orientation::Horizontal => {
                                ratio = position.x / self.size.width;
                            }
                            Orientation::Vertical => {
                                ratio = position.y / self.size.height;
                            }
                        }
                        ratio = if self.flip { 1. - ratio } else { ratio };
                        if pressed && button.is_left() {
                            self.ratio.set(ratio);
                            ctx.window().set_cursor(Cursor::Hand);
                            ctx.post(&self.message, ratio);
                            return Damage::Partial;
                        } else {
                            ctx.window().set_cursor(Cursor::Arrow);
                        }
                    }
                }
                Pointer::Scroll {
                    orientation: _,
                    step,
                } => {
                    let ratio = match &self.orientation {
                        Orientation::Horizontal => {
                            let width = (self.bar.width()
                                - match step {
                                    Step::Value(v) => v,
                                    Step::Increment(s) => {
                                        self.bar.width() + (s as f32 * self.size.width) / 100.
                                    }
                                })
                            .clamp(0., self.size.width);
                            width / self.size.width
                        }
                        Orientation::Vertical => {
                            let height = (self.bar.height()
                                - match step {
                                    Step::Value(v) => v,
                                    Step::Increment(s) => {
                                        self.bar.height() + (s as f32 * self.size.height) / 100.
                                    }
                                })
                            .clamp(0., self.size.height);
                            height / self.size.height
                        }
                    };
                    self.ratio.set(ratio);
                    ctx.post(&self.message, ratio);
                    return self.damage();
                }
                Pointer::Leave => {
                    self.pressed = false;
                }
                Pointer::Hover => {
                    if self.pressed {
                        match &self.orientation {
                            Orientation::Horizontal => {
                                let width = position.x.clamp(0., self.size.width);
                                let ratio = width / self.size.width;
                                let ratio = if self.flip { 1. - ratio } else { ratio };
                                self.ratio.set(ratio);
                                ctx.post(&self.message, ratio);
                                return Damage::Partial;
                            }
                            Orientation::Vertical => {
                                let height = position.x.clamp(0., self.size.height);
                                let ratio = height / self.size.height;
                                let ratio = if self.flip { 1. - ratio } else { ratio };
                                self.ratio.set(ratio);
                                ctx.post(&self.message, ratio);
                                return self.damage();
                            }
                        }
                    }
                }
                _ => {}
            },
            _ => {}
        }
        Damage::None
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, _env: &Env) -> Damage {
        ctx.prepare();
        self.ratio.set(ctx.get(&self.message).release());
        self.ratio.damage()
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<T>, bc: &BoxConstraints, _env: &Env) -> Size {
        self.size = bc.maximum();
        match self.orientation {
            Orientation::Vertical => {
                self.bar
                    .set_size(bc.maximum_width(), bc.maximum_height() * self.ratio.get());
                Size::new(self.size.width(), self.bar.height())
            }
            Orientation::Horizontal => {
                self.bar
                    .set_size(bc.maximum_width() * self.ratio.get(), bc.maximum_height());
                Size::new(self.bar.width(), self.size.height())
            }
        }
    }
}

impl<M> Style for Progress<M> {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.bar.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.bar.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.bar.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.bar.set_bottom_left_radius(radius);
    }
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.bar.set_texture(texture);
    }
}

/// A widget that displays values in a range.
///
/// This widget is essentially [`Progress`] with more styling capabilities.
pub struct Slider<M> {
    ratio: f32,
    handle: Positioner<Circle>,
    progress: Progress<M>,
    background: Rectangle,
    gradient: BorderedCircle,
}

impl<M> Slider<M> {
    /// The background of the slider.
    pub fn background(mut self, texture: impl Into<Texture>) -> Self {
        self.background.set_texture(texture);
        self
    }
    /// Set a texture on the handle.
    pub fn handle_texture(mut self, texture: impl Into<Texture>) -> Self {
        self.handle.set_texture(texture);
        self
    }
    /// Set a ratio corresponding to the size of the handle compared to the inner [`Progress`].
    pub fn handle_size(mut self, ratio: f32) -> Self {
        self.ratio = ratio;
        self
    }
}

impl<M> Slider<M> {
    pub fn new(message: M) -> Self {
        Slider {
            progress: Progress::new(message),
            ratio: 2.,
            handle: Positioner::new(Circle::new(0.)),
            background: Rectangle::default(),
            gradient: BorderedCircle::default()
                .border_width(4.)
                .texture(RadialGradient {
                    stops: vec![
                        GradientStop::new(0., Color::BLACK),
                        GradientStop::new(1., Color::TRANSPARENT),
                    ]
                    .into(),
                    mode: SpreadMode::Pad,
                }),
        }
    }
}

impl<M> Themeable for Slider<M> {
    const NAME: &'static str = "slider";

    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        if let Some(radius) = env.try_get(id, "radius") {
            self.set_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "top-left-radius") {
            self.set_top_left_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "top-right-radius") {
            self.set_top_right_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "bottom-right-radius") {
            self.set_bottom_right_radius(radius)
        }
        if let Some(radius) = env.try_get(id, "bottom-left-radius") {
            self.set_bottom_left_radius(radius)
        }
        if let Some(texture) = env.try_get::<Texture>(id, "background") {
            self.background.set_texture(texture)
        }
        if let Some(texture) = env.try_get::<Texture>(id, "highlight") {
            self.progress.set_texture(texture);
        }
        self.handle.theme(id.push(Id::from("handle")), env);
        self.ratio = env
            .try_get(id.push(Id::from("handle")), "ratio")
            .unwrap_or(self.ratio);
    }
}

impl<M> Guarded for Slider<M> {
    fn restore(&mut self) {
        self.progress.restore();
        self.background.inner.restore();
        self.handle.restore()
    }
    fn touched(&self) -> bool {
        self.progress.touched() || self.background.inner.touched() || self.handle.touched()
    }
}

impl<M> Geometry for Slider<M> {
    fn width(&self) -> f32 {
        4. + match self.progress.orientation {
            Orientation::Horizontal => self.progress.width() + self.handle.width(),
            Orientation::Vertical => self.progress.width().max(self.handle.width()),
        }
    }
    fn height(&self) -> f32 {
        4. + match self.progress.orientation {
            Orientation::Vertical => self.progress.height() + self.handle.height(),
            Orientation::Horizontal => self.progress.height().max(self.handle.height()),
        }
    }
}

impl<M> Drawable for Slider<M> {
    fn draw(&self, ctx: &mut DrawContext, mut transform: tiny_skia::Transform) {
        let x_offset = (self.width() - self.progress.width()) / 2.;
        let y_offset = (self.height() - self.progress.height()) / 2.;

        self.background
            .draw(ctx, transform.pre_translate(x_offset, y_offset));
        self.progress
            .draw(ctx, transform.pre_translate(x_offset, y_offset));
        transform = transform.pre_translate(2., 2.);
        let Position { x, y } = self.handle.position();
        self.gradient
            .draw(ctx, transform.pre_translate(x - 2., y - 2.));
        self.handle.draw(ctx, transform);
    }
}

impl<M, T> Widget<T> for Slider<M>
where
    for<'a> M::Message<'a>: PartialEq + FieldParameter + Release<f32>,
    for<'a> M: MessageBuilder<Data<'a> = f32>,
    for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
{
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
        self.restore();
        if self.progress.flip {
            let Position { x, y } = self.progress.inner_offset();
            self.handle.set_position(x, y);
        } else {
            match self.progress.orientation {
                Orientation::Horizontal => {
                    self.handle
                        .set_position(self.progress.ratio.get() * self.background.width(), 0.);
                }
                Orientation::Vertical => {
                    self.handle
                        .set_position(0., self.progress.ratio.get() * self.background.width());
                }
            }
        }
        scene.draw(self)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        match event {
            Event::Pointer(MouseEvent {
                pointer,
                mut position,
            }) => {
                if !self.progress.pressed
                    && self.handle.contains(&position)
                    && pointer.left_button_click().is_some()
                {
                    self.progress.pressed = true;
                    position = self.handle.position().translate(2., 2.);
                } else {
                    let x = (self.width() - self.progress.width()) / 2.;
                    let y = (self.height() - self.progress.height()) / 2.;
                    position = position.translate(-x, -y);
                }
                position.x -= 2.;
                position.y -= 2.;
                Widget::<T>::event(
                    &mut self.progress,
                    ctx,
                    MouseEvent::new(position, pointer),
                    env,
                )
            }
            _ => Widget::<T>::event(&mut self.progress, ctx, event, env),
        }
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        Widget::<T>::update(&mut self.progress, ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        Widget::<T>::layout(&mut self.progress, ctx, bc, env);
        self.handle
            .set_radius(bc.maximum_width().min(bc.maximum_height()) * self.ratio / 4.);
        self.gradient.set_radius((self.handle.height() / 2.) + 2.);
        let bc = match self.progress.orientation {
            Orientation::Vertical => bc.crop(0., self.handle.height()),
            Orientation::Horizontal => bc.crop(self.handle.width(), 0.),
        }
        .crop(4., 4.);
        Widget::<T>::layout(&mut self.progress, ctx, &bc, env);
        Widget::<T>::layout(&mut self.background, ctx, &bc, env);
        self.size()
    }
}

impl<M> Style for Slider<M> {
    fn set_top_left_radius(&mut self, radius: RelativeUnit) {
        self.progress.set_top_left_radius(radius);
        self.background.set_top_left_radius(radius);
    }
    fn set_top_right_radius(&mut self, radius: RelativeUnit) {
        self.progress.set_top_right_radius(radius);
        self.background.set_top_right_radius(radius);
    }
    fn set_bottom_right_radius(&mut self, radius: RelativeUnit) {
        self.progress.set_bottom_right_radius(radius);
        self.background.set_bottom_right_radius(radius);
    }
    fn set_bottom_left_radius(&mut self, radius: RelativeUnit) {
        self.progress.set_bottom_left_radius(radius);
        self.background.set_bottom_left_radius(radius);
    }
    fn set_texture<B: Into<scene::Texture>>(&mut self, texture: B) {
        self.progress.set_texture(texture);
    }
}
