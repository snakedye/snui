use crate::*;

/// A widget that debugs [`Event`].
pub struct Debug<W>(pub W);

impl<T, W: Widget<T>> Widget<T> for Debug<W> {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        self.0.draw_scene(scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        println!("{:?}", event);
        self.0.event(ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.0.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.0.layout(ctx, bc, env)
    }
}
