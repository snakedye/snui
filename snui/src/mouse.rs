//! Mouse related types.

use crate::scene::Position;
use crate::{Event, Orientation};

/// Cursor mimetype
pub enum Cursor {
    Arrow,
    TopLeftCorner,
    TopRightCorner,
    BottomRightCorner,
    BottomLeftCorner,
    Exchange,
    TopSide,
    BottomSide,
    RightSide,
    LeftSide,
    Cross,
    Cursor,
    Mouse,
    PointCenter,
    PointLeft,
    Text,
    ColumnResize,
    RowResize,
    CrossHair,
    DragDropMove,
    DragDropNone,
    ArrowUp,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
    Draft,
    Help,
    Kill,
    Sizing,
    Blocked,
    Hand,
    OpenHand,
    Watch,
    Wait,
    // Hides the cursor
    None,
}

impl Cursor {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Arrow => "arrow",
            Self::Cursor => "cursor",
            Self::TopLeftCorner => "top_left_corner",
            Self::TopRightCorner => "top_right_corner",
            Self::BottomRightCorner => "bottom_right_corner",
            Self::BottomLeftCorner => "bottom_left_corner",
            Self::Exchange => "exchange",
            Self::TopSide => "top_side",
            Self::BottomSide => "bottom_side",
            Self::RightSide => "right_side",
            Self::LeftSide => "left_side",
            Self::Cross => "cross",
            Self::Mouse => "mouse",
            Self::PointCenter => "center_ptr",
            Self::PointLeft => "left_ptr",
            Self::Text => "text",
            Self::ColumnResize => "",
            Self::RowResize => "",
            Self::CrossHair => "crosshair",
            Self::DragDropMove => "",
            Self::DragDropNone => "",
            Self::ArrowUp => "up_arrow",
            Self::ArrowDown => "down_arror",
            Self::ArrowLeft => "left_arrow",
            Self::ArrowRight => "right_arrow",
            Self::Draft => "draft_large",
            Self::Help => "help",
            Self::Kill => "kill",
            Self::Sizing => "sizing",
            Self::Blocked => "block",
            Self::Hand => "hand1",
            Self::OpenHand => "hand2",
            Self::Wait => "wait",
            Self::Watch => "watch",
            Self::None => "",
        }
    }
}

/// Scroll steps.
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Step {
    /// Step in logical pixels.
    Value(f32),
    /// Step in arbitrary amount of jumps.
    Increment(i32),
}

/// Pointer event.
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct MouseEvent {
    pub pointer: Pointer,
    pub position: Position,
}

impl MouseEvent {
    pub fn new(position: Position, pointer: Pointer) -> Event<'static> {
        Event::Pointer(Self { pointer, position })
    }
}

impl Default for MouseEvent {
    fn default() -> Self {
        Self {
            pointer: Pointer::Hover,
            position: Default::default(),
        }
    }
}

/// Type of pointer event.
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Pointer {
    MouseClick {
        serial: u32,
        button: MouseButton,
        pressed: bool,
    },
    Scroll {
        orientation: Orientation,
        step: Step,
    },
    Hover,
    Enter,
    Leave,
}

impl Pointer {
    pub fn serial(&self) -> Option<u32> {
        match self {
            Self::MouseClick { serial, .. } => Some(*serial),
            _ => None,
        }
    }
    pub fn click(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial, pressed, ..
            } => pressed.then_some(serial),
            _ => None,
        }
    }
    pub fn release(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial, pressed, ..
            } => (!pressed).then_some(serial),
            _ => None,
        }
    }
    pub fn left_button_click(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial,
                button,
                pressed,
            } => {
                if button.is_left() && pressed {
                    Some(serial)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
    pub fn left_button_release(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial,
                button,
                pressed,
            } => {
                if button.is_left() && !pressed {
                    Some(serial)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
    pub fn middle_button_click(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial,
                button,
                pressed,
            } => {
                if button.is_middle() && pressed {
                    Some(serial)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
    pub fn middle_button_release(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial,
                button,
                pressed,
            } => {
                if button.is_middle() && !pressed {
                    Some(serial)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
    pub fn right_button_click(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial,
                button,
                pressed,
            } => {
                if button.is_right() && pressed {
                    Some(serial)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
    pub fn right_button_release(self) -> Option<u32> {
        match self {
            Self::MouseClick {
                serial,
                button,
                pressed,
            } => {
                if button.is_right() && !pressed {
                    Some(serial)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}

/// A mouse button.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum MouseButton {
    Left,
    Middle,
    Right,
    Extra(u32),
}

impl MouseButton {
    #[cfg(feature = "linux")]
    pub fn new(button: u32) -> MouseButton {
        let button = button % 272;
        match button {
            0 => MouseButton::Left,
            1 => MouseButton::Right,
            2 => MouseButton::Middle,
            _ => MouseButton::Extra(button),
        }
    }
    pub fn is_left(&self) -> bool {
        matches!(self, MouseButton::Left)
    }
    pub fn is_right(&self) -> bool {
        matches!(self, MouseButton::Right)
    }
    pub fn is_middle(&self) -> bool {
        matches!(self, MouseButton::Middle)
    }
    pub fn is_extra(self, button: u32) -> bool {
        match self {
            MouseButton::Extra(uint) => uint == button,
            _ => false,
        }
    }
}
