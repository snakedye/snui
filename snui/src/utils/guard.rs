use crate::*;
use std::ops::{BitAnd, BitOr};

pub type Guard<T> = MutGuard<'static, T>;

/// A guard that tracks changes.
#[derive(Clone, Debug, PartialEq, Default)]
pub struct MutGuard<'a, T> {
    inner: T,
    gbool: Gbool<'a>,
}

impl<T> From<T> for MutGuard<'static, T> {
    fn from(inner: T) -> Self {
        Guard::new(inner, Gbool::default())
    }
}

#[derive(Debug, PartialEq)]
/// A guarded boolean.
pub(crate) enum Gbool<'a> {
    Owned(bool),
    Handle(&'a mut bool),
}

impl Default for Gbool<'static> {
    fn default() -> Self {
        Self::Owned(false)
    }
}

impl<'a> Gbool<'a> {
    pub fn get(&self) -> bool {
        match self {
            Self::Owned(b) => *b,
            Self::Handle(b) => **b,
        }
    }
    pub fn set(&mut self, rhs: bool) {
        match self {
            Self::Owned(b) => *b = rhs,
            Self::Handle(b) => **b = rhs,
        }
    }
    pub fn borrow<'b>(&'b mut self) -> Gbool<'b>
    where
        'a: 'b,
    {
        match self {
            Self::Owned(b) => Gbool::Handle(b),
            Self::Handle(b) => Gbool::Handle(b),
        }
    }
    pub fn take(&mut self) -> bool {
        std::mem::take(match self {
            Self::Owned(b) => b,
            Self::Handle(b) => *b,
        })
    }
    pub fn upgrade(&mut self, other: bool) {
        self.set(self.get() | other);
    }
}

impl BitAnd for Gbool<'_> {
    type Output = Gbool<'static>;
    fn bitand(self, rhs: Self) -> Self::Output {
        Gbool::Owned(self.get() & rhs.get())
    }
}

impl BitOr for Gbool<'_> {
    type Output = Gbool<'static>;
    fn bitor(self, rhs: Self) -> Self::Output {
        Gbool::Owned(self.get() | rhs.get())
    }
}

impl Clone for Gbool<'_> {
    fn clone(&self) -> Self {
        Self::Owned(self.get())
    }
}

impl Deref for Gbool<'_> {
    type Target = bool;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::Owned(b) => b,
            Self::Handle(b) => b,
        }
    }
}

impl DerefMut for Gbool<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        match self {
            Self::Owned(b) => b,
            Self::Handle(b) => b,
        }
    }
}

pub(crate) struct GboolPair<'a>(Gbool<'a>, Gbool<'a>);

impl<'a> GboolPair<'a> {
    pub fn map<'b>(&'b mut self, gbool: Gbool<'b>) -> GboolPair<'b> {
        GboolPair(self.1.borrow(), gbool)
    }
}

impl<'a> Deref for GboolPair<'a> {
    type Target = Gbool<'a>;

    fn deref(&self) -> &Self::Target {
        &self.1
    }
}

impl<'a> DerefMut for GboolPair<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.1
    }
}

impl<'a> Default for GboolPair<'a> {
    fn default() -> Self {
        Self(Gbool::default(), Gbool::default())
    }
}

impl Drop for GboolPair<'_> {
    fn drop(&mut self) {
        self.0.upgrade(self.1.get());
    }
}

/// Map a field of a guard to another.
macro_rules! map {
    ($guard:expr, $($field:tt).*) => {
        $guard.map_unchecked(|t| &mut t$(.$field)*)
    };
}

pub(crate) use map;

/// Assigns a value to a [`MutGuard`].
macro_rules! set {
    ($guard:expr, $($field:tt).* = $data:expr) => {
        $guard.map_unchecked(|t| &mut t$(.$field)*).assign($data)
    };
}

pub(crate) use set;

/// A guarded type is one that keeps track of its mutation between resets.
pub trait Guarded {
    /// Returns true if the data has been tampered.
    fn touched(&self) -> bool;
    /// Puts the object in an untouched state.
    fn restore(&mut self);
    fn reset(&mut self) -> bool {
        let t = self.touched();
        self.restore();
        t
    }
    fn damage(&self) -> Damage {
        self.touched()
            .then_some(Damage::Partial)
            .unwrap_or_default()
    }
}

impl<'a, T> Guarded for &'a mut T
where
    T: Guarded,
{
    fn touched(&self) -> bool {
        T::touched(self)
    }
    fn restore(&mut self) {
        T::restore(self)
    }
}

impl<'a, T> MutGuard<'a, T> {
    const fn new(inner: T, gbool: Gbool<'a>) -> Self {
        Self { inner, gbool }
    }
    /// Copy the data.
    pub fn get(&self) -> T
    where
        T: Copy,
    {
        self.inner
    }
    /// Create a clone on write guard with the data.
    pub fn make_mut<'b>(&'b mut self) -> CowGuard<'b, T>
    where
        'a: 'b,
        T: PartialEq + Clone,
    {
        CowGuard::new(&mut self.inner, self.gbool.borrow())
    }
    pub fn as_mut<'b>(&'b mut self) -> DropGuard<'b, &'b mut T>
    where
        'a: 'b,
        T: Guarded,
    {
        DropGuard::new(&mut self.inner, self.gbool.borrow())
    }
    /// Replace the inner data by a new value.
    pub fn set(&mut self, data: T)
    where
        T: PartialEq,
    {
        self.gbool.upgrade(self.inner.ne(&data));
        self.inner = data;
    }
    /// Map a field from the data to another guard.
    ///
    /// This method assumes the user won't mutate the data with the closure.
    ///
    /// Please use [`map!`].
    pub(crate) fn map_unchecked<'b, V, F: FnOnce(&'b mut T) -> V>(&'b mut self, f: F) -> MutGuard<V>
    where
        V: 'b,
    {
        let v = f(&mut self.inner);
        MutGuard::new(v, self.gbool.borrow())
    }
    /// Release the guarded data.
    pub fn unwrap(mut self) -> T {
        self.gbool.upgrade(true);
        self.inner
    }
}

impl<'a, T> MutGuard<'a, &'a mut T>
where
    T: PartialEq,
{
    /// Replace the inner data by a new value.
    pub fn replace(&mut self, data: T) -> T {
        self.gbool.upgrade(data.ne(self.inner));
        std::mem::replace(self.inner, data)
    }
    /// Assign a new value to the inner data.
    pub fn assign(&mut self, data: T) {
        self.gbool.upgrade(data.ne(self.inner));
        *self.inner = data;
    }
}

impl<T: Guarded> MutGuard<'_, T> {
    #[inline]
    pub fn touched(&self) -> bool {
        self.inner.touched()
    }
    #[inline]
    pub fn restore(&mut self) {
        self.gbool.set(false);
        self.inner.restore();
    }
    pub fn then<V, F: FnOnce(&mut T) -> V>(&mut self, f: F) -> V {
        let v = f(&mut self.inner);
        self.gbool.upgrade(self.inner.touched());
        v
    }
}

impl<T> Guarded for MutGuard<'_, T> {
    #[inline]
    fn touched(&self) -> bool {
        self.gbool.get()
    }
    #[inline]
    fn restore(&mut self) {
        self.gbool.set(false);
    }
}

impl<T> Deref for MutGuard<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> AsMut<T> for MutGuard<'_, T> {
    fn as_mut(&mut self) -> &mut T {
        self.gbool.set(true);
        &mut self.inner
    }
}

/// A guard that updates it's touched state on Drop.
pub struct DropGuard<'a, T: Guarded> {
    inner: T,
    gbool: Gbool<'a>,
}

impl<'a, T: Guarded> Deref for DropGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<'a, T: Guarded> DerefMut for DropGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<'a, T: Guarded> DropGuard<'a, T> {
    fn new(inner: T, gbool: Gbool<'a>) -> Self {
        Self { inner, gbool }
    }
}

impl<'a, T: Guarded> Drop for DropGuard<'a, T> {
    fn drop(&mut self) {
        self.gbool.upgrade(self.inner.touched())
    }
}

/// A clone on write [`Guard`].
pub struct CowGuard<'a, T: PartialEq + Clone> {
    inner: &'a mut T,
    clone: Option<T>,
    gbool: Gbool<'a>,
}

impl<'a, T: PartialEq + Clone> CowGuard<'a, T> {
    fn new(inner: &'a mut T, gbool: Gbool<'a>) -> Self {
        Self {
            inner,
            gbool,
            clone: None,
        }
    }
}

impl<T: PartialEq + Clone> Deref for CowGuard<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.inner
    }
}

impl<T: PartialEq + Clone> DerefMut for CowGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        match self.clone {
            Some(ref mut inner) => inner,
            None => self.clone.insert(self.inner.clone()),
        }
    }
}

impl<T: PartialEq + Clone> Drop for CowGuard<'_, T> {
    fn drop(&mut self) {
        if let Some(inner) = self.clone.take() {
            self.gbool.upgrade(inner.ne(self.inner));
            *self.inner = inner;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Guard, Guarded, MutGuard};
    use snui_derive::guard;

    #[test]
    fn test_guard() {
        #[guard]
        struct Normal {
            f: f32,
        }

        let mut n = Normal { f: Guard::from(1.) };
        assert_eq!(n.touched(), false);
        n.f.set(0.);
        assert_eq!(n.touched(), true)
    }

    #[test]
    fn test_map() {
        struct Float {
            f: f32,
        }

        let mut n = MutGuard::from(Float { f: 1. });
        assert_eq!(n.touched(), false);
        let mut g = map!(n, f);
        g.replace(2.);
        assert_eq!(g.touched(), true);
        std::mem::drop(g);
        assert_eq!(n.touched(), true);
    }

    #[test]
    fn test_fmap() {
        struct Int {
            a: u32,
        }
        struct Float {
            f: (Int, Int),
        }

        let mut n = MutGuard::from(Float {
            f: (Int { a: 0 }, Int { a: 1 }),
        });
        assert_eq!(n.touched(), false);
        let mut g = map!(n, f.0.a);
        g.replace(2);
        assert_eq!(g.touched(), true);
        std::mem::drop(g);
        assert_eq!(n.touched(), true);
    }

    #[test]
    fn test_then() {
        #[guard]
        struct Float {
            f: f32,
        }

        let mut n = Guard::from(Float { f: Guard::from(1.) });
        assert_eq!(n.touched(), false);
        n.then(|n| n.f.set(2.));
        assert_eq!(n.touched(), true);
    }

    #[test]
    fn test_cow() {
        let mut g = Guard::from(1.);
        assert_eq!(g.touched(), false);
        *g.make_mut() = 2.;
        assert_eq!(g.touched(), true);
    }
}
