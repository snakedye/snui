use std::fmt::{Debug, Formatter};
use std::ops::Deref;

/// A stack based linked list.
#[derive(PartialEq, Clone, Copy)]
pub struct List<'a, T> {
    data: T,
    previous: Option<&'a List<'a, T>>,
}

impl<'a, T> List<'a, T> {
    /// Create a new [`List`]
    pub fn new(data: T) -> Self {
        Self {
            data,
            previous: None,
        }
    }
    /// Insert a new element at the top of the stack.
    pub fn push(&'a self, data: T) -> Self {
        Self {
            previous: Some(self),
            data,
        }
    }
    /// Returns a reference to the previous [`List`].
    pub fn previous(&self) -> Option<&'a List<'a, T>> {
        self.previous
    }
    /// Calls a closure on each element in the collection
    /// from the first item inserted to the last.
    pub fn rev_iter<F>(&self, f: &mut F)
    where
        F: FnMut(&List<T>),
    {
        if let Some(previous) = self.previous {
            previous.rev_iter(f);
        }
        f(self)
    }
    /// Siminal to `rev_iter` except the function exits on `Err`.
    pub fn try_rev_iter<F, E>(&self, f: &mut F) -> Result<(), E>
    where
        F: FnMut(&List<T>) -> Result<(), E>,
    {
        if let Some(previous) = self.previous {
            previous.try_rev_iter(f)?;
        }
        f(self)
    }
    pub fn iter(&self) -> ListIter<T> {
        ListIter { list: Some(self) }
    }
}

impl<'a, T: Debug> Debug for List<'a, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl<'a, T> Deref for List<'a, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

/// An iterator over a [`List`].
pub struct ListIter<'a, T> {
    list: Option<&'a List<'a, T>>,
}

impl<'a, T> Iterator for ListIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.list.map(|list| {
            let data = &list.data;
            self.list = list.previous;
            data
        })
    }
}

impl<'a, T> IntoIterator for &'a List<'a, T> {
    type Item = &'a T;

    type IntoIter = ListIter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        ListIter { list: Some(self) }
    }
}
