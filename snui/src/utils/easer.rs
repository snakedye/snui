use std::f32::consts::PI;

/// Easing curves for animations.
pub trait Easer {
    /// start..end is a range between zero and one.
    fn new(start: f32, end: f32) -> Self;
    fn position(&self) -> f32;
    fn partial_steps(&self, frame_time: u32, duration: u32) -> f32 {
        (frame_time as f32) / duration as f32
    }
    fn step_by(&mut self, step: f32) -> Option<f32>;
}

/// An easer implementing the sine function.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Sinus {
    start: f32,
    end: f32,
    position: f32,
}

impl Easer for Sinus {
    fn position(&self) -> f32 {
        (self.position * PI).sin().abs()
    }
    fn new(start: f32, end: f32) -> Self {
        assert!(start >= 0.);
        assert!(end <= 1.);
        Self {
            start,
            end,
            position: start,
        }
    }
    fn step_by(&mut self, step: f32) -> Option<f32> {
        assert!(step >= 0.);
        assert!(step <= 1.);
        self.position = (self.position + step / 2.).clamp(0., 1.);
        (self.position >= self.start && self.position < self.end).then_some(self.position())
    }
}

/// An easer with a quadratic curve.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Quadratic {
    end: f32,
    start: f32,
    position: f32,
}

impl Easer for Quadratic {
    fn new(mut start: f32, mut end: f32) -> Self {
        assert!(start >= 0.);
        assert!(end <= 1.);
        start *= 2.;
        end *= 2.;
        start -= 1.;
        end -= 1.;
        Self {
            start,
            end,
            position: start,
        }
    }
    fn position(&self) -> f32 {
        1. - self.position.powi(2)
    }
    fn step_by(&mut self, step: f32) -> Option<f32> {
        assert!(step >= 0.);
        assert!(step <= 1.);
        self.position = (self.position + step).clamp(-1., 1.);
        (self.position >= self.start && self.position < self.end).then_some(self.position())
    }
}
