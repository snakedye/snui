extern crate proc_macro;

use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{parse_macro_input, DeriveInput, Fields};

#[proc_macro_attribute]
pub fn guard(_args: TokenStream, input: TokenStream) -> TokenStream {
    let input: DeriveInput = parse_macro_input!(input);
    let ident = input.ident;
    let attrs = input.attrs.iter();
    let generics = &input.generics.lt_token;
    let where_clause = &input.generics.where_clause;

    match input.data {
        syn::Data::Struct(s) => match s.fields {
            Fields::Named(fields) => {
                let idents = fields.named.iter().map(|f| &f.ident);
                let idents_a = fields.named.iter().map(|f| &f.ident);
                let idents_b = fields.named.iter().map(|f| &f.ident);
                let vis = fields.named.iter().map(|f| &f.vis);
                let ftypes = fields.named.iter().map(|f| &f.ty);

                quote! {
                    #(#attrs)*
                    pub struct #ident #generics #where_clause{
                        #(#vis #idents: Guard<#ftypes>, )*
                    }

                    impl #generics Guarded for #ident #generics #where_clause {
                        fn touched(&self) -> bool {
                            #(self.#idents_a.touched() || )* false
                        }
                        fn restore(&mut self) {
                            #(self.#idents_b.restore(); )*
                        }
                    }
                }
            }
            Fields::Unnamed(fields) => {
                let ftypes = fields.unnamed.iter().map(|f| &f.ty);
                let ftypes_a = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let ftypes_b = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let ftypes_c = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let ftypes_d = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let vis = fields.unnamed.iter().map(|f| &f.vis);

                quote! {
                    #(#attrs)*
                    pub struct #ident #generics #where_clause (
                        #(#vis Guard<#ftypes>, )*
                    );

                    impl #generics Guarded for #ident #generics #where_clause {
                        fn touched(&self) -> bool {
                            let #ident(#( #ftypes_a, )*) = self;
                            #( #ftypes_b.touched() || )* false
                        }
                        fn restore(&mut self) {
                            let #ident(#( #ftypes_c, )*) = self;
                            #( #ftypes_d.restore(); )*
                        }
                    }
                }
            }
            Fields::Unit => {
                let attrs = input.attrs.iter();
                quote! {
                    #(#attrs)*
                    pub struct #ident;

                    impl Guarded for #ident {
                        fn touched(&self) -> bool {
                            false
                        }
                        fn restore(&mut self) {
                            false
                        }
                    }
                }
            }
        },
        _ => panic!("#[guarded] is not supported for {}", ident),
    }
    .into()
}

#[proc_macro_derive(Guarded)]
pub fn derive_guarded(item: TokenStream) -> TokenStream {
    let input: DeriveInput = parse_macro_input!(item);
    let ident = input.ident;
    let generics = &input.generics;
    let where_clause = &input.generics.where_clause;

    match input.data {
        syn::Data::Struct(s) => match s.fields {
            Fields::Named(fields) => {
                let idents_a = fields.named.iter().map(|f| &f.ident);
                let idents_b = fields.named.iter().map(|f| &f.ident);

                quote! {
                    impl #generics Guarded for #ident #generics #where_clause {
                        fn touched(&self) -> bool {
                            #(self.#idents_a.touched() || )* false
                        }
                        fn restore(&mut self) {
                            #(self.#idents_b.restore(); )*
                        }
                    }
                }
            }
            Fields::Unnamed(fields) => {
                let ftypes_a = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let ftypes_b = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let ftypes_c = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));
                let ftypes_d = fields
                    .unnamed
                    .iter()
                    .enumerate()
                    .map(|(i, _)| format_ident!("field{}", i));

                quote! {
                    impl #generics Guarded for #ident #generics #where_clause {
                        fn touched(&self) -> bool {
                            let #ident(#( #ftypes_a, )*) = self;
                            #( #ftypes_b.touched() || )* false
                        }
                        fn restore(&mut self) {
                            let #ident(#( #ftypes_c, )*) = self;
                            #( #ftypes_d.restore(); )*
                        }
                    }
                }
            }
            Fields::Unit => {
                quote! {
                    impl Guarded for #ident {
                        fn touched(&self) -> bool {
                            false
                        }
                        fn restore(&mut self) { }
                    }
                }
            }
        },
        _ => panic!("#[deriver(Guarded)] is not supported for {}", ident),
    }
    .into()
}
